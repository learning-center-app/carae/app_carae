#  LCA
Compatible React Native App, bootstrapé avec [Ignite](https://github.com/infinitered/ignite)


## Mac


System:
    OS: macOS 13.3.1
    CPU: (10) arm64 Apple M1 Pro
    Memory: 87.88 MB / 16.00 GB
    Shell: 5.9 - /bin/zsh
  Binaries:
    Node: 16.20.2 - ~/.nvm/versions/node/v16.20.2/bin/node
    Yarn: 1.22.19 - ~/.nvm/versions/node/v18.16.0/bin/yarn
    npm: 8.19.4 - ~/.nvm/versions/node/v16.20.2/bin/npm
  SDKs:
    iOS SDK:
      Platforms: DriverKit 22.4, iOS 16.4, macOS 13.3, tvOS 16.4, watchOS 9.4
  IDEs:
    Android Studio: 2022.2 AI-222.4459.24.2221.9971841
    Xcode: 14.3/14E222b - /usr/bin/xcodebuild
  npmPackages:
    react: ^16.8.6 => 16.14.0 
    react-native: ^0.60.6 => 0.60.6

### Deps 

- Nécessite JDK 11 : https://stackoverflow.com/a/66891978/1437016
- Besoin d'installer watchman via brew pour le dev

````
brew install watchman openjdk@11
```

For the system Java wrappers to find this JDK, symlink it with
  sudo ln -sfn /opt/homebrew/opt/openjdk@11/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk-11.jdk

openjdk@11 is keg-only, which means it was not symlinked into /opt/homebrew,
because this is an alternate version of another formula.

If you need to have openjdk@11 first in your PATH, run:
  echo 'export PATH="/opt/homebrew/opt/openjdk@11/bin:$PATH"' >> ~/.zshrc

For compilers to find openjdk@11 you may need to set:
  export CPPFLAGS="-I/opt/homebrew/opt/openjdk@11/include"

Et Java Home dans ~/.bash_rc ou .zshrc
  export JAVA_HOME="/opt/homebrew/opt/openjdk@11/libexec/openjdk.jdk/Contents/Home"

## Pour commencer

Cloner ce repository
Installer les dépendences via `yarn` or `npm i`


### Configuration des variables d'environnement 

Copier `.env.example` en `.env`, puis renseigner l'URL de l'API

```
API_URL=https://myapi.com
```

### Créer une propriété sur Firebase 
Pour les analytics et la détection de code barre : https://console.firebase.google.com/

Récupérer le fichier google-services.json, et le mettre dans le dossier `android/app`

### Run & build

Pour builder :

iOS (nécessite un mac) : 
```
$ cd ios && pod install
$ react-native run-ios
```

Android : 
```
$ react-native run-android
```
Pour débugger sur un device, il faut le .jks, voir section suivante



## Pour déployer

### Android
Google Play needs a signed APK, so you'll have to create a keystore and sign up your build:
1. Copy *android/app/gradle.properties.example* to *android/app/gradle.properties*
2. Generate a **.jks** file : https://stackoverflow.com/a/44103025/1437016
3. Copy your **.jks** in *android/app*
4. Update *android/app/gradle.properties* to point to your **.jks** file and its metadata
5. Run *npm run android:build* to generate a **.aab bundle** (recommended by google for size optimization)
6. Find your **.aab** under *android/app/build/generated/outputs/bundle/release/app.aab*

### iOS
Step to check before making an archive and upload it via XCode:

1. Don't forget to remove *NSAppTransportSecurity* from *Info.plist* if you use an http localhost server. You can just change *NSAppTransportSecurity* to *DISABLED_FOR_PROD_NSAppTransportSecurity*.