export default OrderResaByStartDate = (reservations, ascendant = true) => {
  if (!reservations || reservations.length == 0) {
    return reservations || [];
  }
  //1 pour descendent, -1 pour ascendant
  const operator = ascendant ? -1 : 1;
  var mutable_reservations = reservations.asMutable({deep: false});;
  mutable_reservations.sort((a, b)=>{
    if(a.start_time < b.start_time)
      return operator
    else if(a.start_time > b.start_time)
      return -operator
    else
      return 0
  })

  return mutable_reservations;
}
