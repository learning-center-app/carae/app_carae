import Salle from '../Classes/Salle'

export default SortSallesByCampus = (salles) => {
  var data = [];

  let obj = salles.reduce((result, salle) => {
    if(typeof result[salle.campus_id] == "undefined")
      result[salle.campus_id] = {id:salle.campus_id, campus_name:salle.campus_name, campus_id:salle.campus_id, type_materiel:salle.type_materiel, data: []}
    result[salle.campus_id].data.push(new Salle(salle, salle.reservations));
    return result;
  }, {})
  for (var campus in obj) {
    if (obj.hasOwnProperty(campus)) {
      data.push(obj[campus])
    }
  }
  __DEV__ && console.tron.log(data);
  return data;
}
