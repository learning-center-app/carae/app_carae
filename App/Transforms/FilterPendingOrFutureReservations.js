const numberOfSecondsFromNowToAdd = 3600 * 24 * 30 //30 jours

/**
 * Filtre les résa dans plus de 30 jours
 */
export default FilterPendingOrFutureReservations = (reservations) => {
  const in30Days = (Date.now() / 1000) + numberOfSecondsFromNowToAdd
  //Si la fin de la résa est postérieure à maintenant, c'est que la résa est soit en cours soit future
  return reservations.filter((reservation)=>{
    return reservation.end_time < in30Days
  })
}
