export default FilterReservationsByUser = (reservations, userId) => {
  //S'il n'y a pas d'utilisateur connecté,
  if (!userId || !reservations) {
    //On renvoie un tableau vide
    return []
  }
  //Sinon on filtre
  let _userId = userId.toLowerCase()
  return reservations.filter((reservation)=>{
    return reservation.beneficiaire.toLowerCase() == _userId
  })
}
