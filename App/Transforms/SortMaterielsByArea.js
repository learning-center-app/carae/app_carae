import Materiel from '../Classes/Materiel'

/**
 * Tri le matériel par area, et catégories triées via order_display (si présent)
 * @param {object} materiels - Materiels via redux
 * @returns {array} Catégories, contenant les matériels dans .data
 */
export default SortMaterielsByArea = (materiels) => {
  var data = [];

  let obj = materiels.reduce((result, materiel) => {
    if(typeof result[materiel.area_id] == "undefined")
      result[materiel.area_id] = { id: materiel.area_id, campus_name: materiel.campus_name, campus_id: materiel.campus_id, type_materiel: materiel.type_materiel, order_display: materiel.area_order_display, data: []}
    result[materiel.area_id].data.push(new Materiel(materiel, materiel.reservations));
    return result;
  }, {})
  for (var area in obj) {
    if (obj.hasOwnProperty(area)) {
      data.push(obj[area])
    }
  }
  //Tri par area.order_display, ordre défini dans GRR, et tri à nouveau par campus
  data
  .sort((area1, area2) => area1.order_display - area2.order_display)
  .sort((area1, area2) => area1.campus_id - area2.campus_id)
  
  // __DEV__ && console.tron.log(data);
  return data;
}
