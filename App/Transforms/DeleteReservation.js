export default DeleteReservation = (reservations, deleted_reservation) => {
  //On ne mute pas le tableau, on en créé un nouveau via filter
  return reservations.filter(_reservation => _reservation.id != deleted_reservation.id)
}
