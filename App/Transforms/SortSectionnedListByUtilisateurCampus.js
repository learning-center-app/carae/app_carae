export default SortSectionnedListByUtilisateurCampus = (array, campus_id) => {
  if (!campus_id)
    return array;
  //https://stackoverflow.com/a/62071369/1437016
  return [
    ...array.filter(campus => campus.campus_id == campus_id), 
    ...array.filter(campus => campus.campus_id != campus_id)
  ]
}
