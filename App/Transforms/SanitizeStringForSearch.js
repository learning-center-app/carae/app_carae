/**
 * Enleve les accents, et lowercase le string 
 * https://stackoverflow.com/a/37511463/1437016
 * @param string - string to transform
 */
export default SanitizeStringForSearch = (string) => {
  return string ? string.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase() : "";
}