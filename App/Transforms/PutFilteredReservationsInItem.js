export default PutFilteredReservationsInItem = (reservations, item, Item_class) => {
  // __DEV__ && console.tron.log(item)
  let _reservations = reservations.filter((reservation)=>{
    return reservation.room_id == item.room_id
  });

  return new Item_class(item, _reservations);
}
