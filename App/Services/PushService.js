//Notifications, en fin de fichier
import moment from "moment";
import 'moment/locale/fr'

import i18n from 'i18n-js'

import PushNotification, { Importance } from "react-native-push-notification";
import { translate } from "../I18n";
import { INITIAL_STATE as SCREEN_LIST } from "../Redux/ScreenRedux";
import FilterReservationsByUser from "../Transforms/FilterReservationsByUser";
import { CHANNEL_IDS } from "./PushConfiguration";
import FilterPendingOrFutureReservations from "../Transforms/FilterPendingOrFutureReservations";

export default class PushService {
  static scheduleNotification = (notification) => {
    __DEV__ && console.tron.log({schedulingNotification: notification})
    PushNotification.localNotificationSchedule(notification);
  }
  /**
   * Supprime la réservation prévue / l'enlève du notification center
   * @param {string|int} id_notification reservation.id
   */
  static cancelNotificationById = (id_notification) => {
    __DEV__ && console.tron.log({ "Annulation de notification : " : id_notification })
    PushNotification.cancelLocalNotification(id_notification);
    PushNotification.removeDeliveredNotifications([id_notification]);
  }
  /**
   * Reprogramme la réservation, en annulant la précédente notification et en créant une nouvelle
   * @param {object} reservation 
   */
  static rescheduleNotificationRetardMateriel = (reservation) => {
    PushService.cancelNotificationById(reservation.id.toString());
    __DEV__ && console.tron.log({ "Reprogrammation de notification " : reservation.id, "A la date " : reservation.end_time })
    PushService.scheduleNotificationRetardMateriel(reservation);
  }
  /**
   * Crée un rappel de retard le lendemain 9h00 de la date de rendu
   * @param {object} reservation 
   */
  static scheduleNotificationRetardMateriel = (reservation) => {
    const date_retour = moment.unix(reservation.end_time).locale(i18n.locale);
    PushService.scheduleNotification({
      id: reservation.id,
      title: translate('NOTIFICATIONS.RETARD_MATERIEL.TITLE'), // (optional)
      message: translate('NOTIFICATIONS.RETARD_MATERIEL.MESSAGE').replace("%s", reservation.salle).replace("%d", date_retour.format('LLLL')), // (required)
      // date: new Date(Date.now() + (60 * 1000)), //Dans 60 secondes (test)
      // date: date_retour.clone().toDate(), // A l'heure de fin de résa
      date: date_retour.clone().add(1, "day").set("hour", 9).set("minutes", 0).toDate(), // Le lendemain de la fin, à 9h00
      allowWhileIdle: true, // (optional) set notification to work while on doze, default: false
      importance: Importance.HIGH,
      userInfo: {
        id_reservation: reservation.id,
        targetScreen: SCREEN_LIST.list.find(item => item.screen == "MesReservationsMaterielScreen"),
        channelId: CHANNEL_IDS.RETARD_MATERIEL
      },
      /* Android Only Properties */
      channelId: CHANNEL_IDS.RETARD_MATERIEL,
      onlyAlertOnce: true,

      // repeatTime: 1, // (optional) Increment of configured repeatType. Check 'Repeating Notifications' section for more info.
    })
  }
  /**
   * Reprogramme la réservation, en annulant la précédente notification et en créant une nouvelle
   * @param {object} reservation 
   */
  static rescheduleNotificationRappelSalle = (reservation) => {
    PushService.cancelNotificationById(reservation.id.toString());
    __DEV__ && console.tron.log({ "Reprogrammation de notification " : reservation.id, "A la date " : reservation.end_time })
    PushService.scheduleNotificationRappelSalle(reservation);
  }
  /**
   * Crée un rappel de réservation de salle, si elle est dans plus que 48h
   * @param {object} reservation 
   */
  static scheduleNotificationRappelSalle = (reservation) => {
    const date_depart = moment.unix(reservation.start_time).locale(i18n.locale);;
    //Si avant 48h
    if (date_depart.isBefore(moment().add(2, "days"))) {
      //Pas de push
      // __DEV__ && console.tron.log({ "Réservation dans moins de deux jours : " : reservation })
      return;
    }
    __DEV__ && console.tron.log({ "Réservation de salle à notifier : " : reservation })
    PushService.scheduleNotification({
      id: reservation.id,
      title: translate('NOTIFICATIONS.RAPPEL_SALLE.TITLE'), // (optional)
      message: translate('NOTIFICATIONS.RAPPEL_SALLE.MESSAGE').replace("%s", reservation.salle).replace("%d", date_depart.format('LLLL')), // (required)
      // date: new Date(Date.now() + (60 * 1000)),
      date: date_depart.clone().subtract(2, "day").toDate(), // 48h exactement avant
      // date: date_depart.clone().toDate(), // A l'heure de début de résa
      allowWhileIdle: true, // (optional) set notification to work while on doze, default: false
      importance: Importance.HIGH,
      userInfo: {
        id_reservation: reservation.id,
        targetScreen: SCREEN_LIST.list.find(item => item.screen == "MesReservationsSalleScreen"),
        channelId: CHANNEL_IDS.RAPPEL_SALLE
      },
      /* Android Only Properties */
      channelId: CHANNEL_IDS.RAPPEL_SALLE,
      onlyAlertOnce: true,
    })
  }
  /**
   * Debug : suppression de toutes les notifications et réinitialisation au démarrage
   * @param {object} state - State Root Redux store.getState()
   */
  static cancelAndRescheduleAll = (state) => {
    __DEV__ && console.tron.log("cancellingAllNotifications")
    PushNotification.cancelAllLocalNotifications();

    const reservations_materiel = FilterReservationsByUser(state.materiel.reservations.raw, state.utilisateur.userId)
    __DEV__ && console.tron.log({reservations_materiel})

    reservations_materiel.forEach(reservation => 
      PushService.scheduleNotificationRetardMateriel(reservation)
    )

    const reservations_salles = FilterReservationsByUser(state.salles.reservations.raw, state.utilisateur.userId)
    __DEV__ && console.tron.log({ reservations_salles })

    reservations_salles.forEach(reservation =>
      PushService.scheduleNotificationRappelSalle(reservation)
    )
    PushNotification.getScheduledLocalNotifications(scheduledNotifications => {
      __DEV__ && console.tron.log({ scheduledNotifications })
    })
  }

  /**
   * Créé toutes les futures notifications pour les réservations récupérées depuis GRR
   * 
   * @param {array} reservations Raw reservation from store (ex: state.materiel.reservations.raw)
   * @param {object} utilisateur Raw user from store (ex: state.utilisateur)
   * @param {function} schedulerFn PushService.scheduleNotificationRetard[TYPE], ex: PushService.scheduleNotificationRetardMateriel
   * @param {string} channelId const PushConfiguration.CHANNEL_IDS - ex : RETARD_MATERIEL
   */
  static initScheduledNotificationsForReservationsUserAndType = (reservations, utilisateur, schedulerFn, channelId) => {
    const reservations_user = FilterPendingOrFutureReservations(FilterReservationsByUser(reservations, utilisateur.userId))
    __DEV__ && console.tron.log({ reservations_user, "channelId" : channelId })

    PushNotification.getScheduledLocalNotifications(scheduledNotifications => {
      __DEV__ && console.tron.log({ previouslyScheduledNotifications : scheduledNotifications })

      //Itération des notifications programmées, pour le channel concerné
      scheduledNotifications
      .filter(scheduledNotification => channelId == scheduledNotification.data.channelId)
      .forEach(scheduledNotification => {
        //Annulation de la notification si elle n'est plus dans les résas
        if (!reservations_user.find(reservation => reservation.id == scheduledNotification.data.id_reservation )){
          PushNotification.cancelLocalNotification(scheduledNotification.data.id_reservation)
          __DEV__ && console.tron.log({ cancellingNotification: scheduledNotification })
        }
      })
      //Itération des résas récupérées
      reservations_user.forEach(reservation => {
        //Création de la notification si elle n'est pas déjà prévue
        if (!scheduledNotifications.find(scheduledNotification => {
          // __DEV__ && console.tron.log({ "reservation.id": reservation.id, "scheduledNotification.data.id_reservation" : scheduledNotification.data.id_reservation, equality: reservation.id == scheduledNotification.data.id_reservation })
          return reservation.id == scheduledNotification.data.id_reservation
        }))
          schedulerFn(reservation)
      })
    })
  }
  
}