//Notifications, en fin de fichier
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification, { Importance } from "react-native-push-notification";
import { translate } from "../I18n";

import NavigationService from './NavigationService';

// Must be outside of any component LifeCycle (such as `componentDidMount`).
PushNotification.configure({
  // (optional) Called when Token is generated (iOS and Android)
  onRegister: function (token) {
    __DEV__ && console.tron.log("TOKEN:", token);
  },

  // (required) Called when a remote is received or opened, or local notification is opened
  onNotification: function (notification) {
    __DEV__ && console.tron.log("onNotification : NOTIFICATION:", notification);

    // userInfo est dans data
    // https://reactnavigation.org/docs/1.x/navigating-without-navigation-prop
    if (notification.data && notification.data.targetScreen){
      NavigationService.navigate(notification.data.targetScreen.screen, { ...notification.data.targetScreen, id_reservation_highlight: notification.data.id_reservation });
    }
    // (required) Called when a remote is received or opened, or local notification is opened
    notification.finish(PushNotificationIOS.FetchResult.NoData);
  },

  // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
  onAction: function (notification) {
    __DEV__ && console.tron.log("onACTION:", notification.action);
    __DEV__ && console.tron.log("NOTIFICATION:", notification);

    // process the action
  },

  // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
  onRegistrationError: function (err) {
    console.error(err.message, err);
  },

  // IOS ONLY (optional): default: all - Permissions to register.
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },

  // Should the initial notification be popped automatically
  // default: true
  popInitialNotification: true,

  /**
   * (optional) default: true
   * - Specified if permissions (ios) and token (android and ios) will requested or not,
   * - if not, you must call PushNotificationsHandler.requestPermissions() later
   * - if you are not using remote notification or do not have Firebase installed, use this:
   *     requestPermissions: Platform.OS === 'ios'
   */
  requestPermissions: true,
});

//Configuration des Channels pour Android 
// https://developer.android.com/training/notify-user/channels
export const CHANNEL_IDS = {
  RETARD_LIVRE: "channel-livres-retard",
  RETARD_MATERIEL: "channel-materiel-retard",
  RAPPEL_SALLE: "channel-rappel-salle",
}

const CHANNELS = [
  {
    channelId: CHANNEL_IDS.RETARD_LIVRE, // (required)
    channelName: translate('NOTIFICATIONS.RETARD_LIVRE.CHANNEL_NAME') , // (required)
    channelDescription: translate('NOTIFICATIONS.RETARD_LIVRE.CHANNEL_DESCRIPTION'), // (optional) default: undefined.
    playSound: false, // (optional) default: true
    soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
    importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
    vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
  },
  {
    channelId: CHANNEL_IDS.RETARD_MATERIEL, // (required)
    channelName: translate('NOTIFICATIONS.RETARD_MATERIEL.CHANNEL_NAME') , // (required)
    channelDescription: translate('NOTIFICATIONS.RETARD_MATERIEL.CHANNEL_DESCRIPTION'), // (optional) default: undefined.
    playSound: false, // (optional) default: true
    soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
    importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
    vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
  },
  {
    channelId: CHANNEL_IDS.RAPPEL_SALLE, // (required)
    channelName: translate('NOTIFICATIONS.RAPPEL_SALLE.CHANNEL_NAME') , // (required)
    channelDescription: translate('NOTIFICATIONS.RAPPEL_SALLE.CHANNEL_DESCRIPTION'), // (optional) default: undefined.
    playSound: false, // (optional) default: true
    soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
    importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
    vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
  },
]
PushNotification.getChannels(function (channel_ids) {
  //Création des channels inexistants
  CHANNELS.forEach(channel => {
    if(!channel_ids.includes(channel.channelId))
      PushNotification.createChannel(channel, (created) => __DEV__ && console.tron.log(`createChannel returned '${created}'`) )
  })
});
