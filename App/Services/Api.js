import { findBestAvailableLanguage } from "react-native-localize";
//Pour récupération numéro de version
import DeviceInfo from 'react-native-device-info'

//Internationalisation
import i18n from 'i18n-js'
// a library to wrap and simplify api calls
import apisauce from 'apisauce'
import Secrets from 'react-native-config'

const _baseURL = Secrets.API_URL || 'http://localhost:3000/'

const create = (baseURL = _baseURL) => {
  const fallback = { languageTag: "en", isRTL: false };

  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      'Cache-Control': 'no-cache',
      'X-LCA-app-version': DeviceInfo.getVersion(),
      //Si les traductions ont été chargées par App/I18/index.js#setI18nConfig, 
      //ça veut dire que l'initialisation a été faite depuis le reduxstore pour trouver la langue adéquate (forcée ou pas par l'utiliateur) 
      // => utilisation de i18n.locale qui a alors la bonne valeur
      // => Sinon, i18n.local est par défaut sur "en" au premier boot de l'application, on cherche donc la meilleure langue à afficher
      'Accept-Language': i18n.translations.hasOwnProperty(i18n.locale) ? i18n.locale : (findBestAvailableLanguage(["en", "fr"]) || fallback).languageTag
    },
    // 10 second timeout...
    timeout: 10000
  })
  __DEV__ && console.tron.log({ "i18n.translations": i18n.translations,  "hasIinitialized": i18n.translations.hasOwnProperty(i18n.locale), "bestLanguage": (findBestAvailableLanguage(["en", "fr"]) || fallback).languageTag, "i18n.locale": i18n.locale, api })

  const getHtml = (url) => api.get(url)

  const getSalles = () => api.get('api/grr/salles')
  const getSallesReservations = () => api.get('api/grr/salles/reservations')
  const getSallesReservationsUtilisateur = (id_utilisateur) => api.get('api/grr/salles/reservations/utilisateur', id_utilisateur)

  const getMateriel = () => api.get('api/grr/materiel')
  const getMaterielReservations = () => api.get('api/grr/materiel/reservations')
  const getMaterielReservationsUtilisateur = (id_utilisateur) => api.get('api/grr/materiel/reservations/utilisateur', id_utilisateur)

  const putReservation = (data) => api.put('api/grr/reservation', data)
  const postReservation = (data) => api.post('api/grr/reservation', data)
  const deleteReservation = (data) => api.delete('api/grr/reservation', data)
  
  const upsertUtilisateurGrr = (data) => api.post('api/grr/utilisateur', data)

  const getCGU = () => api.get('api/prismic/cgu')
  const getDescription = () => api.get('api/prismic/descriptions')
  const getActualites = (page) => {
    return api.get('api/prismic/actualites/' + page)
  }
  const getHoraires = () => api.get('api/ics/horaires')
  const getAgenda = () => api.get('api/ics/agenda')

  const postSuggestion = (data) => api.post('api/mail/suggestion', data)
  const postDemandeReference = (data) => api.post('api/mail/reference', data)
  const postRendezVous = (data) => api.post('api/mail/rdv', data)

  const getListeLivres = (id_koha) => api.get('api/koha/books', id_koha)
  const postEmpruntLivre = (data) => api.post('api/koha/book', data)

  const login = () => api.get('api/shibboleth/login')

  return {
    getHtml,

    getSalles,
    getSallesReservations,

    getMateriel,
    getMaterielReservations,

    putReservation,
    postReservation,
    deleteReservation,
    
    upsertUtilisateurGrr,

    getCGU,
    getDescription,
    getActualites,

    getHoraires,
    getAgenda,

    postSuggestion,
    postDemandeReference,
    postRendezVous,

    postEmpruntLivre,
    getListeLivres,

    login,
  }
}

// let's return back our create method as the default.
export default {
  create,
  baseURL : _baseURL
}
