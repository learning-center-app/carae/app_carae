// leave off @2x/@3x
const images = {
  background: require('../Images/background.jpg'),
  salle: require('../Images/salle.png'),
  carte: require('../Images/carte.jpg'),
  tutoriel: {
    fr: [
      require('../Images/tutoriel/fr/1.jpg'),
      require('../Images/tutoriel/fr/2.jpg'),
      require('../Images/tutoriel/fr/3.jpg'),
      require('../Images/tutoriel/fr/4.jpg'),
      require('../Images/tutoriel/fr/5.jpg'),
    ],
    en: [
      require('../Images/tutoriel/en/1.jpg'),
      require('../Images/tutoriel/en/2.jpg'),
      require('../Images/tutoriel/en/3.jpg'),
      require('../Images/tutoriel/en/4.jpg'),
      require('../Images/tutoriel/en/5.jpg'),
    ]
  },

  screen_icons : {
    dispo_salle : require('../Images/Screen/icon_reservation_rapide.png'),
    reservation_salle : require('../Images/Screen/icon_reservation_salle.png'),
    reservation_materiel : require('../Images/Screen/icon_reservation_materiel.png'),
    emprunt_livre : require('../Images/Screen/icon_emprunt_livre.png'),
    demande_reference : require('../Images/Screen/icon_demande_reference.png'),
    article : require('../Images/Screen/icon_article.png'),
    suggestion : require('../Images/Screen/icon_suggestion.png'),
    description : require('../Images/Screen/icon_description.png'),
    rendez_vous : require('../Images/Screen/icon_rendez_vous.png'),
    agenda : require('../Images/Screen/icon_agenda.png'),
    actualites : require('../Images/Screen/icon_actualites.png'),
    cgu: require('../Images/Screen/icon_description.png'),
    horaires : require('../Images/Screen/icon_horaires.png'),
  },
  actions_icons : {
    light :{
      add: require('../Images/Actions/picto_ajouter_blanc.png'),
      edit: require('../Images/Actions/picto_edit.png'),
      delete: require('../Images/Actions/picto_supprimer.png'),
      calendrier: require('../Images/Actions/picto_calendrier.png'),
      carte: require('../Images/Actions/picto_carte.png'),
      chevron: require('../Images/Actions/picto_chevron.png'),
      filtres: require('../Images/Actions/picto_filtres.png'),
      horaires: require('../Images/Actions/picto_horaires.png'),
      menu: require('../Images/Actions/picto_menu.png'),
      recherche: require('../Images/Actions/picto_recherche.png'),
    },
    dark : {
      add: require('../Images/Actions/picto_plus_navbar.png'),
      delete: require('../Images/Actions/picto_moins_navbar.png'),
      recherche: require('../Images/Actions/picto_recherche_dark.png'),
    }
  }
}

export default images
