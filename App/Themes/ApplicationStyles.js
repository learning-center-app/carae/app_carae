import Fonts from './Fonts'
import Metrics from './Metrics'
import Colors from './Colors'
import { getStatusBarHeight, isIPhoneWithMonobrow } from 'react-native-status-bar-height'

// This file is for a reusable grouping of Theme items.
// Similar to an XML fragment layout in Android

const ApplicationStyles = {
  screen: {
    backgroundImage: {
      position: 'absolute',
      top: 0,
      left: 0,
      bottom: 0,
      right: 0
    },
    container: {
      flex: 1,
      // paddingTop: Metrics.baseMargin,
      backgroundColor: Colors.transparent
    },
    row_separator: {
      backgroundColor:'rgba(255,255,255,0.2)',
      width:'80%',
      marginHorizontal:'10%',
      marginTop:-1,
      height:1,
    },
    section: {
      margin: Metrics.section,
      padding: Metrics.baseMargin
    },
    sectionHeader: {
      ...Fonts.style.normal,
      paddingVertical: Metrics.doubleBaseMargin,
      color: Colors.text,
      marginVertical: Metrics.smallMargin,
      textAlign: 'center'
    },
    sectionFixedHeader: {
      paddingVertical: Metrics.doubleBaseMargin,
      paddingHorizontal: Metrics.doubleBaseMargin,
      color: Colors.text,
      textAlign: 'left',
      fontFamily: Fonts.type.base,
      fontSize: Fonts.size.h4
    },
    sectionFixedHeaderWithSub: {
      paddingTop: Metrics.doubleBaseMargin,
      paddingBottom: Metrics.baseMargin,
      paddingHorizontal: Metrics.doubleBaseMargin,
      color: Colors.text,
      textAlign: 'left',
      fontFamily: Fonts.type.base,
      fontSize: Fonts.size.h4
    },
    sectionFixedSubHeader: {
      paddingBottom: Metrics.doubleBaseMargin,
      paddingHorizontal: Metrics.doubleBaseMargin,
      color: Colors.slightlyTransparentText,
      textAlign: 'left',
      fontFamily: Fonts.type.base,
      fontSize: Fonts.size.medium
    },
    sectionText: {
      ...Fonts.style.normal,
      paddingVertical: Metrics.doubleBaseMargin,
      color: Colors.text,
      marginVertical: Metrics.smallMargin,
      textAlign: 'center'
    },
    subtitle: {
      color: Colors.text,
      padding: Metrics.smallMargin,
      marginBottom: Metrics.smallMargin,
      marginHorizontal: Metrics.smallMargin
    },
    titleText: {
      ...Fonts.style.h2,
      fontSize: 14,
      color: Colors.text
    },
    bgFill:{
      backgroundColor: Colors.background
    },
    listContent: {
      backgroundColor: Colors.transparent
    },
    groupContainer: {
      margin: Metrics.smallMargin,
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center'
    },
    sub_nav : {
      backgroundColor:'white',
      shadowColor: '#222',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
      zIndex:10, //Pour que ça overlap les flatlist
    },
    sub_nav_text: {
      ...Fonts.style.normal,
      paddingVertical: (Metrics.baseMargin + Metrics.smallMargin),
      paddingHorizontal: Metrics.baseMargin,
      color: Colors.darkText
    },
    modalPaddingIphone: {
      paddingTop: getStatusBarHeight(true),
      marginTop: -getStatusBarHeight(true) //SafeAreaView padding au cas où y'a un monosourcil
    },
    modalMarginIphone: {
      marginTop: -getStatusBarHeight(true) //SafeAreaView padding au cas où y'a un monosourcil
    },
    modalMarginIphoneMonobrow: {
      marginTop: isIPhoneWithMonobrow() ? 20 : 0 //SafeAreaView padding au cas où y'a un monosourcil
    },

    navbar_button: {
      // paddingVertical: Metrics.baseMargin,
      paddingHorizontal: Metrics.baseMargin,
      // marginHorizontal: Metrics.baseMargin,
      backgroundColor: Colors.navbar_button,
      height:'100%',
      alignItems:'center',
      justifyContent:'center',
    },
    navbar_icon: {
      resizeMode: 'contain', //https://github.com/facebook/react-native/issues/26699#issuecomment-579893233
      width:25,
      height:25,
    },

    inline: {
      flexWrap: 'wrap',
      alignItems: 'center',
      alignSelf: 'center',
      flexDirection:'row',
      justifyContent:'space-between'
    },

    inline_no_wrap: {
      flexWrap: 'nowrap',
      alignItems: 'center',
      alignSelf: 'flex-start',
      flexDirection:'row',
      justifyContent:'space-between'
    },
    padded: {
      paddingVertical: Metrics.baseMargin,
      paddingHorizontal: Metrics.doubleBaseMargin,
    },
    list_empty : {
      ...Fonts.style.h5,
      flex:1,
      alignSelf:'center',
      textAlign:'center',
      padding: Metrics.doubleBaseMargin,
    }
  },
  forms: {
    label: {
      ...Fonts.style.normal,
      color: Colors.text
    },
    input: {
      paddingVertical: Metrics.smallMargin,
      backgroundColor:Colors.dark,
      color: Colors.text,
      fontFamily: Fonts.type.base,
      fontWeight: 'normal',
    },
    input_with_margin_top : {
      paddingTop: Metrics.baseMargin,
    }
  },
  components : {
    filter : {
      backgroundColor:Colors.dark,
      shadowColor: '#222',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      flexDirection:'row',
      alignItems:'center'
    },
    filter_with_margin : {
      marginBottom: Metrics.baseMargin,
    },
    filter_block : {
      paddingTop: Metrics.baseMargin,
      paddingBottom: Metrics.doubleBaseMargin,
      paddingHorizontal: Metrics.baseMargin,
      flexDirection: 'column',
      flex:1,
    },
    filter_title: {
      ...Fonts.style.small,
      paddingBottom: Metrics.baseMargin,
    },
    filter_text: {
      ...Fonts.style.normal,
      color: Colors.text
    },
    filter_icon: {
      width:20,
      height:20,
      margin: Metrics.doubleBaseMargin,
      resizeMode:'contain',
      flex:0
    },

    modal_block : {
      alignItems:'center',
      marginVertical: Metrics.doubleBaseMargin,
    },
    modal_label : {
      ...Fonts.style.normal,
      alignItems:'center',
      paddingBottom: Metrics.baseMargin,
    },
    modal_inline : {
      flexDirection: 'row',
      justifyContent: 'space-between'
    },
    modal_inline_item : {
      alignSelf: 'center',
      textAlign: 'center',
      flex:1
    },
    modal_inline_cursor : {
      padding: Metrics.doubleBaseMargin,
    },
    actions : {
      marginTop: Metrics.doubleBaseMargin,
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center'
    },
    alert : {
      marginHorizontal: Metrics.baseMargin,
      paddingHorizontal: Metrics.doubleBaseMargin,
      paddingVertical: Metrics.baseMargin,
      backgroundColor: 'rgba(0,0,0,0.2)',
      borderWidth:1,
      borderColor:'rgba(0,0,0,0.4)',
    },
    alert_text: {
      ...Fonts.style.description,
      color: Colors.text,
    },
    alert_warning : {
      backgroundColor: Colors.warning,
      color: Colors.white
    },
    alert_error : {
      backgroundColor: Colors.error
    },
    alert_info : {
      backgroundColor: Colors.info
    },
    alert_success : {
      backgroundColor: Colors.success
    },
    alert_dark : {
      backgroundColor: 'rgba(0,0,0,0.2)',
    },
    alert_light : {
      backgroundColor:  'rgba(255,255,255,0.2)',
    },
  },
  list_item : {
    list_item_title : {
      ...Fonts.style.h4,
      fontSize: Fonts.size.h5,
      marginBottom: Metrics.baseMargin
    },
    list_item_subtitle : {
      // marginTop: Metrics.doubleBaseMargin
    },
  },
  darkLabelContainer: {
    padding: Metrics.smallMargin,
    paddingBottom: Metrics.doubleBaseMargin,
    borderBottomColor: Colors.border,
    borderBottomWidth: 1,
    marginBottom: Metrics.baseMargin
  },
  darkLabel: {
    fontFamily: Fonts.type.bold,
    color: Colors.text
  },
  groupContainer: {
    margin: Metrics.smallMargin,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  sectionTitle: {
    ...Fonts.style.h4,
    color: Colors.coal,
    backgroundColor: Colors.ricePaper,
    padding: Metrics.smallMargin,
    marginTop: Metrics.smallMargin,
    marginHorizontal: Metrics.baseMargin,
    borderWidth: 1,
    borderColor: Colors.ember,
    alignItems: 'center',
    textAlign: 'center'
  }

}


ApplicationStyles.picker = {
  inputAndroid : {
    ...ApplicationStyles.forms.input,
    ...ApplicationStyles.screen.padded,
    fontFamily: Fonts.type.base,
    fontWeight:'normal',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
    // backgroundColor: Colors.transparent,
  },
  inputIOS : {
    ...ApplicationStyles.forms.input,
    ...ApplicationStyles.screen.padded,
    paddingVertical: 16,
    fontFamily: Fonts.type.base,
    fontWeight:'normal',//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  },

	placeholderColor: Colors.slightlyTransparentText,
	underline: {
    borderTopWidth: 0
  },
  viewContainer: {
    backgroundColor: Colors.transparent,
  },
  modalViewBottom: {
    backgroundColor: Colors.white,
    color: Colors.text,
    shadowColor: '#222',
    shadowOffset: { width: 0, height: -2 },
    shadowOpacity: 0.5,
    shadowRadius: 3,
  },

  iconContainer: {
      top: 20,
      right: 20,
  },
}
ApplicationStyles.picker_icon = {
  backgroundColor: 'transparent',
  borderTopWidth: 8,
  borderTopColor: Colors.slightlyTransparentText,
  borderRightWidth: 8,
  borderRightColor: 'transparent',
  borderLeftWidth: 8,
  borderLeftColor: 'transparent',
  width: 0,
  height: 0,
}

ApplicationStyles.calendar = {
  backgroundColor: Colors.transparent,
  calendarBackground: Colors.transparent,
  textSectionTitleColor: Colors.slightlyTransparentText,
  selectedDayBackgroundColor: Colors.success,
  selectedDayTextColor: '#ffffff',
  todayTextColor: Colors.info,
  dayTextColor: Colors.text,
  textDisabledColor: Colors.slightlyTransparentText,
  dotColor: Colors.info,
  selectedDotColor: Colors.text,
  arrowColor: Colors.info,
  monthTextColor: Colors.text,
  textDayFontFamily: Fonts.type.base,
  textMonthFontFamily: Fonts.type.base,
  textDayHeaderFontFamily: Fonts.type.base,
  textMonthFontWeight: 'bold',
  textDayFontSize: Fonts.size.regular,
  textMonthFontSize: Fonts.size.regular,
  textDayHeaderFontSize: Fonts.size.regular
}

ApplicationStyles.app_loading_view = {

  container: {
    flex: 1,
    backgroundColor: Colors.dark,
    alignItems:'center',
    justifyContent:'center',
  },
  loading_text: {
    ...Fonts.style.h5,
  }
}
export default ApplicationStyles
