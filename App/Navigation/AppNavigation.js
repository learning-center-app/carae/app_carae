//@flow
import { StackNavigator } from 'react-navigation'
import RendezVousScreen from '../Containers/RendezVousScreen'
import HorairesScreen from '../Containers/HorairesScreen'
import AgendaScreen from '../Containers/AgendaScreen'
import SideMenu from '../Containers/SideMenu'
import SuggestionScreen from '../Containers/SuggestionScreen'
import DemandeReferenceScreen from '../Containers/DemandeReferenceScreen'
import MesReservationsSalleScreen from '../Containers/MesReservationsSalleScreen'
import ReservationSalleScreen from '../Containers/ReservationSalleScreen'
import ReservationSalleDetailScreen from '../Containers/ReservationSalleDetailScreen'
import MesReservationsMaterielScreen from '../Containers/MesReservationsMaterielScreen'
import ReservationMaterielScreen from '../Containers/ReservationMaterielScreen'
import ReservationMaterielDetailScreen from '../Containers/ReservationMaterielDetailScreen'
import ActualitesScreen from '../Containers/ActualitesScreen'
import DescriptionScreen from '../Containers/DescriptionScreen'
import EmpruntLivreScreen from '../Containers/EmpruntLivreScreen'
import DispoSalleScreen from '../Containers/DispoSalleScreen'
import HomeScreen from '../Containers/HomeScreen'
import CguScreen from '../Containers/CguScreen'
import LaunchScreen from '../Containers/LaunchScreen'
import MentionsLegalesScreen from '../Containers/MentionsLegalesScreen'
import LoginScreen from '../Containers/LoginScreen'
import LoginKohaScreen from '../Containers/LoginKohaScreen'
import LoginCampusScreen from '../Containers/LoginCampusScreen'
import ModifierLangueScreen from '../Containers/ModifierLangueScreen'
import SetUtilisateurScreen from '../Containers/SetUtilisateurScreen'

import styles from './Styles/NavigationStyles'
import {Fonts, Metrics, Colors} from '../Themes/'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  RendezVousScreen: { screen: RendezVousScreen },
  HorairesScreen: { screen: HorairesScreen },
  AgendaScreen: { screen: AgendaScreen },
  SideMenu: { screen: SideMenu },
  SuggestionScreen: { screen: SuggestionScreen },
  DemandeReferenceScreen: { screen: DemandeReferenceScreen },
  EmpruntLivreScreen: { screen: EmpruntLivreScreen },
  MesReservationsSalleScreen: { screen: MesReservationsSalleScreen },
  ReservationSalleScreen: { screen: ReservationSalleScreen },
  ReservationSalleDetailScreen: { screen: ReservationSalleDetailScreen },
  MesReservationsMaterielScreen: { screen: MesReservationsMaterielScreen },
  ReservationMaterielScreen: { screen: ReservationMaterielScreen },
  ReservationMaterielDetailScreen: { screen: ReservationMaterielDetailScreen },
  ActualitesScreen: { screen: ActualitesScreen },
  DescriptionScreen: { screen: DescriptionScreen },
  DispoSalleScreen: { screen: DispoSalleScreen },
  HomeScreen: { screen: HomeScreen },
}, {
  // Default config for all screens
  // headerMode: 'none',
  initialRouteName: 'HomeScreen',
  navigationOptions: {
    headerStyle: styles.header,
    //Suppression utilisation SafeAreaView dans header, puisqu'on se sert du SafeAreaView de ReactNative v60 dans Rootcontainer
    headerForceInset: { top: 'never', bottom: 'never' }, //https://v1.reactnavigation.org/docs/stack-navigator.html#headerforceinset
    headerTitleStyle : styles.titleHeader,
    headerTintColor: 'white', //https://github.com/react-navigation/react-navigation/releases/tag/v1.0.0-beta.9
    headerBackTitle: null, //https://stackoverflow.com/a/50655905/1437016
  },
  //Style des cartes
  cardStyle: {
    backgroundColor: Colors.transparent,
  },
  //https://github.com/react-navigation/react-navigation/issues/2713
  transitionConfig: () => ({
    containerStyle: {
      backgroundColor: Colors.transparent,
    },
    //https://medium.com/async-la/custom-transitions-in-react-navigation-2f759408a053
    screenInterpolator: sceneProps => {
      const { layout, position, scene } = sceneProps

      const thisSceneIndex = scene.index
      const width = layout.initWidth

      const translateX = position.interpolate({
        inputRange: [thisSceneIndex - 1, thisSceneIndex],
        outputRange: [width, 0],
      })

      return { transform: [ { translateX } ] }
    },
  })
})


const RootStack = StackNavigator(
  {
    //Stack de navigation principale, mise en première pour être chargée et affichée par défaut
    Main: { screen: PrimaryNav },
    //Tous les écrans qui suivent sont des modaux
    CguScreen: { screen: CguScreen },
    LaunchScreen: { screen: LaunchScreen },
    MentionsLegalesScreen:  { screen: MentionsLegalesScreen },
    ModifierLangueScreen:  { screen: ModifierLangueScreen },
    LoginScreen:  { screen: LoginScreen },
    LoginKohaScreen:  { screen: LoginKohaScreen },
    LoginCampusScreen:  { screen: LoginCampusScreen },
    SetUtilisateurScreen: { screen: SetUtilisateurScreen },
  },
  {
    mode: 'modal',
    headerMode: 'none',
    //Style des cartes
    cardStyle: {
      backgroundColor: Colors.transparent,
    },
    //https://github.com/react-navigation/react-navigation/issues/2713
    transitionConfig: () => ({
      containerStyle: {
        backgroundColor: Colors.transparent,
      },
    })
  }
);

export default RootStack
