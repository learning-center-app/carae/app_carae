import { StyleSheet } from 'react-native'
import { Colors, Fonts } from '../../Themes/'

export default StyleSheet.create({
  header: {
    backgroundColor: Colors.transparent,
    borderBottomWidth: 0,
    shadowOpacity: 0,
    shadowOffset: {
      height: 0,
    },
    shadowRadius: 0,
    elevation: 0,
  },
  titleHeader : {
    fontFamily: Fonts.type.base,
    fontSize: 20,
    fontWeight: 'normal'//https://github.com/react-navigation/react-navigation/issues/542#issuecomment-345289122
  }
})
