import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import Analytics from 'appcenter-analytics';

import UpdateReservationsInSalles from '../Transforms/UpdateReservationsInSalles'
import FilterReservationsByUser from '../Transforms/FilterReservationsByUser'
import SortReservationsBySalles from '../Transforms/SortReservationsBySalles'
import OrderResaByStartDate from '../Transforms/OrderResaByStartDate'
import SortMaterielsByArea from '../Transforms/SortMaterielsByArea';
import SortSectionnedListByUtilisateurCampus from '../Transforms/SortSectionnedListByUtilisateurCampus'
import TransformKeysToValuesForSelect from '../Transforms/TransformKeysToValuesForSelect'
import EditReservation from '../Transforms/EditReservation'
import DeleteReservation from '../Transforms/DeleteReservation'
import PushService from '../Services/PushService'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  materielRequest: null,
  materielSuccess: ['raw'],
  materielFailure: null,

  materielReservationsRequest: null,
  materielReservationsSuccess: ['raw'],
  materielReservationsFailure: null,

  putMaterielReservationRequest : ['data'],
  putMaterielReservationSuccess : ['reservation'],
  putMaterielReservationFailure : null,
  putMaterielReservationSoftFailure : ['error'],

  postMaterielReservationRequest : ['data'],
  postMaterielReservationSuccess : ['reservation'],
  postMaterielReservationFailure : null,

  deleteMaterielReservationRequest : ['data'],
  deleteMaterielReservationSuccess : ['reservation'],
  deleteMaterielReservationFailure : ['error'],
})

export const MaterielTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: null,
  raw: null,
  reservations : {
    raw : [],
    fetching : null,
    sending : null,
    sending_error : null,
    lastReservation : null
  },
  configuration : {
    types_reservation : [ "A", "B", "C", "D", "E", "F", "G"]
  },
  error: null
})

/* ------------- Selectors ------------- */

export const MaterielSelectors = {
  getMaterielSortedByCampus : state => {
    // __DEV__ && console.tron.log({'state.materiel.reservations.raw.length' : state.materiel.reservations.raw.length})
    let _resas = SortReservationsBySalles(state.materiel.reservations.raw);
    // __DEV__ && console.tron.log({_resas : _resas})
    let _materiel = UpdateReservationsInSalles(_resas, state.materiel.raw);
    // __DEV__ && console.tron.log({_materiel : _materiel})
    let _sorted_by_campus = SortMaterielsByArea(_materiel);
    // __DEV__ && console.tron.log({_sorted_by_campus : _sorted_by_campus})
    let _sorted_by_campus_and_utilisateur = SortSectionnedListByUtilisateurCampus(_sorted_by_campus, state.utilisateur.campus && state.utilisateur.campus.id);
    // __DEV__ && console.tron.log({_sorted_by_campus_and_utilisateur : _sorted_by_campus_and_utilisateur})

    return _sorted_by_campus_and_utilisateur;
  },
  getReservationsByUser: state => {
    let reservations = OrderResaByStartDate(FilterReservationsByUser(state.materiel.reservations.raw, state.utilisateur.userId));
    // __DEV__ && console.tron.log(reservations);
    return reservations;
  }
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state) =>
  state.merge({ fetching: true})

// successful api lookup
export const success = (state, action) => {
  const { raw } = action
  return state.merge({ fetching: false, error: null, raw })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true })

/* Reservations */
// request the data from an api
export const requestReservations = (state) =>
  state.merge({ fetching: true})

// successful api lookup
export const successReservations = (state, action) => {
  const { raw } = action
  // __DEV__ && console.tron.log({state})
  // PushService.initScheduledNotificationsForReservationsUserAndType(raw, state.utilisateur, PushService.scheduleNotificationRetardMateriel)
  return state.merge({ fetching: false, error: null, reservations : {raw}})
}

// Something went wrong somewhere.
export const failureReservations = state =>
  state.merge({ fetching: false, error: true })


/* Envoi d'une réservation */

export const requestPutMaterielReservation = (state) =>
  state.merge({ reservations: {...state.reservations, sending: true, sending_error: null}})

export const successPutMaterielReservation = (state, action) => {
  const { reservation } = action
  Analytics.trackEvent('[Matériel] Réservation réussie', {nom: reservation.materiel, campus: reservation.campus_name});
  PushService.scheduleNotificationRetardMateriel(reservation);

  return state.merge({
    reservations: {
      raw : state.reservations.raw ? state.reservations.raw.concat([reservation]) : [reservation],
      sending: false,
      sending_error: null,
      lastReservation: reservation //Extraction de la dernière résa
    }
  })
}

//Erreur gérée par le serveur, contenant un message à montrer à l'utilisateur
export const softFailurePutMaterielReservation = (state, action) =>{
  Analytics.trackEvent('[Matériel] Réservation échouée', error);
  const { error } = action
  __DEV__ && console.tron.log(action)
  return state.merge({ reservations: {...state.reservations, sending: false, sending_error: error}})
}
//Erreur non-gérée par le serveur (erreur 500)
export const failurePutMaterielReservation = state =>{
  Analytics.trackEvent('[Matériel] Réservation échouée');
  return state.merge({ reservations: {...state.reservations, sending: false, sending_error: true}})
}


/********* Edition d'une réservation *********/

export const requestPostMaterielReservation = (state) =>
  state.merge({ reservations: {...state.reservations, sending: true, sending_error: null}})

export const successPostMaterielReservation = (state, action) => {
  const { reservation } = action
  Analytics.trackEvent('[Matériel] Modification d\'une réservation réussie', {nom: reservation.materiel, campus: reservation.campus_name});
  PushService.rescheduleNotificationRetardMateriel(reservation);
  return state.merge({
    reservations: {
      raw : EditReservation(state.reservations.raw, reservation),
      sending: false,
      sending_error: null,
      lastReservation: reservation //Extraction de la dernière résa
    }
  })
}

//Erreur gérée par le serveur, contenant un message à montrer à l'utilisateur
export const failurePostMaterielReservation = (state, action) =>{
  const { error } = action
  Analytics.trackEvent('[Matériel] Modification d\'une réservation échouée', error);
  state.merge({ reservations: {...state.reservations, sending: false, sending_error: error}})
}

/********* Suppression d'une réservation *********/

export const requestDeleteMaterielReservation = (state) =>
  state.merge({ reservations: {...state.reservations, sending: true, sending_error: null}})

export const successDeleteMaterielReservation = (state, action) => {
  const { reservation } = action
  Analytics.trackEvent('[Matériel] Suppression d\'une réservation réussie', {nom: reservation.materiel, campus: reservation.campus_name});
  PushService.cancelNotificationById(reservation.id);
  return state.merge({
    reservations: {
      raw : DeleteReservation(state.reservations.raw, reservation),
      sending: false,
      sending_error: null,
      lastReservation: reservation //Extraction de la dernière résa
    }
  })
}

//Erreur gérée par le serveur, contenant un message à montrer à l'utilisateur
export const failureDeleteMaterielReservation = (state, action) =>{
  const { error } = action
  Analytics.trackEvent('[Matériel] Suppression d\'une réservation échouée', error);
  return state.merge({ reservations: {...state.reservations, sending: false, sending_error: error}})
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.MATERIEL_REQUEST]: request,
  [Types.MATERIEL_SUCCESS]: success,
  [Types.MATERIEL_FAILURE]: failure,

  [Types.MATERIEL_RESERVATIONS_REQUEST]: requestReservations,
  [Types.MATERIEL_RESERVATIONS_SUCCESS]: successReservations,
  [Types.MATERIEL_RESERVATIONS_FAILURE]: failureReservations,

  [Types.PUT_MATERIEL_RESERVATION_REQUEST]: requestPutMaterielReservation,
  [Types.PUT_MATERIEL_RESERVATION_SUCCESS]: successPutMaterielReservation,
  [Types.PUT_MATERIEL_RESERVATION_FAILURE]: failurePutMaterielReservation,
  [Types.PUT_MATERIEL_RESERVATION_SOFT_FAILURE]: softFailurePutMaterielReservation,

  [Types.POST_MATERIEL_RESERVATION_REQUEST]: requestPostMaterielReservation,
  [Types.POST_MATERIEL_RESERVATION_SUCCESS]: successPostMaterielReservation,
  [Types.POST_MATERIEL_RESERVATION_FAILURE]: failurePostMaterielReservation,

  [Types.DELETE_MATERIEL_RESERVATION_REQUEST]: requestDeleteMaterielReservation,
  [Types.DELETE_MATERIEL_RESERVATION_SUCCESS]: successDeleteMaterielReservation,
  [Types.DELETE_MATERIEL_RESERVATION_FAILURE]: failureDeleteMaterielReservation,
})
