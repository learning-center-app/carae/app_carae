import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import Analytics from 'appcenter-analytics';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  empruntLivreRequest: ['data'],
  empruntLivreSuccess: ['last_book'],
  empruntLivreFailure: ['error'],
  empruntLivreReset: null,

  listeLivresRequest: ['data'],
  listeLivresSuccess: ['books'],
  listeLivresFailure: ['error'],
  listeLivresReset: null,
})

export const EmpruntLivreTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  //Envoi d'une réservation
  sending: null,
  error: null,
  last_book: null,
  //Récupération des livres
  fetchingListeLivres: null,
  books: [],
  errorFetching: null,
})

/* ------------- Selectors ------------- */

export const EmpruntLivreSelectors = {
  getError: state => state.error
}

/* ------------- Reducers ------------- */

// request the data from an api
export const requestEmprunt = (state) =>
  state.merge({ sending: true, error: null, last_book: null })

// successful api lookup
export const successEmprunt = (state, action) => {
  Analytics.trackEvent('[EmpruntLivre] Réservation réussie');
  return state.merge({ sending: false, error: null, last_book : action.last_book})
}

// Something went wrong somewhere.
export const failureEmprunt = (state, action) =>{
  let { error } = action;
  Analytics.trackEvent('[EmpruntLivre] Réservation échouée', error);
  return state.merge({ sending: false, error: error, last_book: null })
}

// Remise à zero du state
export const resetEmprunt = (state) =>
  state.merge({ sending: false, error: null, last_book: null })
  

// request the data from an api
export const requestListeLivres = (state) =>
  state.merge({ fetchingListeLivres: true, errorFetching: null })

// successful api lookup
export const successListeLivres = (state, action) => {
  // Analytics.trackEvent('[ListeLivres] Récupération des livres');
  return state.merge({ fetchingListeLivres: false, errorFetching: null, books: action.books })
}

// Something went wrong somewhere.
export const failureListeLivres = (state, action) => {
  let { error } = action;
  // Analytics.trackEvent('[ListeLivres] Récupération échouée', error);
  return state.merge({ fetchingListeLivres: false, errorFetching: error || true })
}

// Remise à zero du state
export const resetListeLivres = (state) =>
  state.merge({ fetchingListeLivres: false, errorFetching: null, books: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.EMPRUNT_LIVRE_REQUEST]: requestEmprunt,
  [Types.EMPRUNT_LIVRE_SUCCESS]: successEmprunt,
  [Types.EMPRUNT_LIVRE_FAILURE]: failureEmprunt,
  [Types.EMPRUNT_LIVRE_RESET]: resetEmprunt,

  [Types.LISTE_LIVRES_REQUEST]: requestListeLivres,
  [Types.LISTE_LIVRES_SUCCESS]: successListeLivres,
  [Types.LISTE_LIVRES_FAILURE]: failureListeLivres,
  [Types.LISTE_LIVRES_RESET]: resetListeLivres,
})
