import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import Analytics from 'appcenter-analytics';

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  demandeReferenceRequest: ['data'],
  demandeReferenceSuccess: null,
  demandeReferenceFailure: ['error']
})

export const DemandeReferenceTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  sending: null,
  success: null,
  error: null
})

/* ------------- Selectors ------------- */

export const DemandeReferenceSelectors = {
  getError: state => state.error
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state) => {
  return state.merge({ sending: true, error: null, success: null })
}

// successful api lookup
export const success = (state, action) => {
  Analytics.trackEvent('[DemandeReference] Envoi réussi');
  return state.merge({ sending: false, error: null, success: true})
}

// Something went wrong somewhere.
export const failure = (state, action) =>{
  let { error } = action;
  if (typeof error != "string") {
    error = JSON.stringify(error);
  }
  Analytics.trackEvent('[DemandeReference] Envoi échoué', error);
  return state.merge({ sending: false, error: error, success: false })
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.DEMANDE_REFERENCE_REQUEST]: request,
  [Types.DEMANDE_REFERENCE_SUCCESS]: success,
  [Types.DEMANDE_REFERENCE_FAILURE]: failure
})
