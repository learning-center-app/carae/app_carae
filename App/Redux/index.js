import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'
import ReduxPersist from '../Config/ReduxPersist'

// listen for the action type of 'RESET', you can change this.
import { resettableReducer } from 'reduxsauce'

const resettable = resettableReducer('RESET')

/* ------------- Assemble The Reducers ------------- */
export const reducers = combineReducers({
  nav: require('./NavigationRedux').reducer,
  screen: resettable(require('./ScreenRedux').reducer),
  utilisateur: resettable(require('./UtilisateurRedux').reducer),
  salles: resettable(require('./SallesRedux').reducer),
  materiel: resettable(require('./MaterielRedux').reducer),
  html: resettable(require('./HtmlRedux').reducer),
  suggestion: require('./SuggestionRedux').reducer,
  demande_reference: require('./DemandeReferenceRedux').reducer,
  rendez_vous: require('./RendezVousRedux').reducer,
  emprunt_livre: require('./EmpruntLivreRedux').reducer,
  // github: require('./GithubRedux').reducer,
  // search: require('./SearchRedux').reducer
})

export default () => {
  let finalReducers = reducers
  // If rehydration is on use persistReducer otherwise default combineReducers
  if (ReduxPersist.active) {
    const persistConfig = ReduxPersist.storeConfig
    finalReducers = persistReducer(persistConfig, reducers)
  }

  let { store, sagasManager, sagaMiddleware } = configureStore(finalReducers, rootSaga)

  if (module.hot) {
    module.hot.accept(() => {
      const nextRootReducer = require('./').reducers
      store.replaceReducer(nextRootReducer)

      const newYieldedSagas = require('../Sagas').default
      sagasManager.cancel()
      sagasManager.done.then(() => {
        sagasManager = sagaMiddleware.run(newYieldedSagas)
      })
    })
  }

  return store
}
