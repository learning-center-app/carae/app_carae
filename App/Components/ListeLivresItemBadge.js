import moment from 'moment';
import PropTypes from 'prop-types';

import React, { PureComponent } from "react";
import { Text, View } from 'react-native';

import styles from './Styles/ListeLivresItemBadgeStyle'

export default class ListeLivresItemBadge extends PureComponent {
  static propTypes = {
    date: PropTypes.string.isRequired,
    daysBeforeRenew: PropTypes.number.isRequired,
    returnDateFormat: PropTypes.string.isRequired,
  }
  getBadgeType (date) {
    if (date.isSameOrAfter(null, "day"))
      return "late"
    else if (date.isBefore(moment().add(this.props.daysBeforeRenew, "days"), "day"))
      return "soon"
    else
      return "ok"
  }
  render() {
    const date = moment(this.props.date, this.props.returnDateFormat);
    let badgeType = this.getBadgeType(date);
    
    return (
      <View style={[styles.badge, styles[badgeType]]}>
        <Text style={styles.label}>{date.format('DD/MM')}</Text>
      </View>
    );
  }
}