//@flow
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types';
import HTMLView from './HTMLView';

import { View, Text, Image, TouchableOpacity, Animated } from 'react-native'

import styles from './Styles/ListItemStyle'
import { Images, Fonts, Colors, ApplicationStyles } from '../Themes'
import AvailableIcon from './AvailableIcon'
import AvailableLabel from './AvailableLabel'
import { translate } from '../I18n';

const MIN_HEIGHT = 100;

export default class ListItem extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      animation : new Animated.Value(MIN_HEIGHT),
      minHeight:MIN_HEIGHT,
    };
  }
  // componentWillReceiveProps(props) {
  //   __DEV__ && console.tron.log({received_props : props.item.room_name})
  //   this.setState({
  //     item_availability: props.item.getAvailability(this.props.utilisateur, this.props.start_time, this.props.end_time)
  //   })
  // }
  // // Prop type warnings
  static propTypes = {
    item: PropTypes.object.isRequired,
    section: PropTypes.object.isRequired,
    utilisateur: PropTypes.object.isRequired,
    handlers: PropTypes.object,
    backgroundColor: PropTypes.string,
    start_time : PropTypes.object,
    end_time : PropTypes.object,
    gender: PropTypes.oneOf(['masculine', 'feminine']), // Masculin ou féminin
  }

  // // Defaults for props
  static defaultProps = {
    item: {},
    section: {}
  }

  _setMaxHeight(event){
    // __DEV__ && console.tron.log(event.nativeEvent.layout)
    this.setState({
        maxHeight   : event.nativeEvent.layout.height
    });
  }

  _setMinHeight(event){
    this.setState({
        minHeight   : event.nativeEvent.layout.height
    });
  }
  toggle(){
    //Step 1
    let initialValue    = this.state.expanded? this.state.maxHeight : this.state.minHeight,
        finalValue      = this.state.expanded? this.state.minHeight : this.state.maxHeight;

    this.setState({
        expanded : !this.state.expanded  //Step 2
    });

    this.state.animation.setValue(initialValue);  //Step 3
    Animated.spring(     //Step 4
        this.state.animation,
        {
            toValue: finalValue
        }
    ).start();  //Step 5
  }

  render () {
    // __DEV__ && console.tron.log({ [this.props.item.room_name] : this.props.item.image})
    let _item_availability = this.props.item.getAvailability(this.props.utilisateur, this.props.start_time, this.props.end_time)
    return (
      <Animated.View style={[styles.container, {backgroundColor:this.props.backgroundColor}, {height: this.state.animation}]} >
        <TouchableOpacity onPress={() => this.toggle()}  onLayout={this._setMaxHeight.bind(this)}>
          <View style={[styles.row, styles.inline, {justifyContent:'flex-start'}]}>
            {
              //Cast en booléen pour éviter erreur : https://github.com/facebook/react-native/issues/18773#issuecomment-380237027
              !!this.props.item.image && <Image source={{uri: this.props.item.image}} style={styles.illustration} />
            }
            <View style={styles.rowContent}>
              <View style={styles.inline_no_wrap}>
                <View style={{flex:1}}>
                  <Text
                    numberOfLines={2}
                    style={styles.list_item_title}>{this.props.item.room_name}</Text>
                  <AvailableLabel style={styles.list_item_subtitle} gender={this.props.gender} status={_item_availability}/>
                </View>
                <AvailableIcon
                  status={_item_availability}
                  onAvailable={()=>{this.props.handlers.onAvailable(this.props.item)}}
                  onConflict={this.props.handlers.onConflict}
                  onUnavailable={this.props.handlers.onUnavailable}></AvailableIcon>
              </View>
            </View>
          </View>
          <View>
            { !!this.props.item.description && <Text style={styles.label}>{this.props.item.description}</Text>}
            { !!this.props.item.comment_room && <HTMLView style={styles.html} value={this.props.item.comment_room}></HTMLView>}
            {/* <Text style={styles.label}>{this.props.item.room_name}</Text> */}
            {!!this.props.item.moderate && <Text style={[styles.label, ApplicationStyles.components.alert, ApplicationStyles.components.alert_warning]}>⚠︎ {translate('FORM.LABEL.moderation')}</Text>}
          </View>
        </TouchableOpacity>
      </Animated.View>

    )
  }
}
