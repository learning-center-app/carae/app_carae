import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { KeyboardAvoidingView } from 'react-native'

// Pour gérer l'offset pour le clavier
import { Header } from 'react-navigation';
import { getStatusBarHeight } from 'react-native-status-bar-height'

export default class BetterKeyboardAvoidingView extends Component {
  // Prop type warnings
  static propTypes = {
    style: PropTypes.object,
    offset: PropTypes.number,
    behavior: PropTypes.string,
    contentContainerStyle: PropTypes.string,
  }
  //
  // // Defaults for props
  // static defaultProps = {
  //   someSetting: false
  // }

  render () {
    return (
      <KeyboardAvoidingView
        style={this.props.style}
        keyboardVerticalOffset={this.props.offset || Header.HEIGHT + getStatusBarHeight(true)}
        behavior={this.props.behavior || 'position'}
        contentContainerStyle={this.props.contentContainerStyle}>
        {this.props.children}
      </KeyboardAvoidingView>
    )
  }
}
