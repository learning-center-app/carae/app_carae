//@flow
import React, { Component } from 'react'
import PropTypes from 'prop-types';

import { View, Text, TouchableOpacity } from 'react-native'
import styles from './Styles/ModalStyle'
import Modal from "react-native-modal";

import SemanticButton from './SemanticButton'

export default class _Modal extends Component {
  // Typage des props
  static propTypes = {
    isVisible: PropTypes.bool.isRequired,
    title : PropTypes.func.isRequired,
    buttons: PropTypes.array,
    onCancel : PropTypes.func.isRequired,
  }

  // Valeur par défaut
  static defaultProps = {
    isVisible: false,
    title: "Modal par défaut",
    onCancel : () => {},
    buttons: [
      {
        text : 'Annuler',
        type : 'warning',
        onPress : null
      },
      {
        text : 'Ok',
        type : 'success',
        onPress : null
      }
    ]
  }
  //Par défaut, le modal n'est pas visible. Changer à true pour debug
  state = {
    isVisible : false
  }

  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    //Transformation de la propriété isVisible passée en state,
    //pour gérer l'état aussi en interne et pouvoir utiliser setState()
    //Le state est gardé synchro avec les props via componentDidUpdate()
    this.state.isVisible = props.isVisible;
  }

  //Hook sur le changement de props
  //https://stackoverflow.com/a/44750890/1437016
  componentDidUpdate(prevProps, prevState, snapshot) {
    //Mise à jour du state uniquement si la prop à changer
    if (prevProps.isVisible !== this.props.isVisible) {
      //On ne fait pas d'inversion du state interne : on prend directement la valeur du props
      this.setState((partialState)=> ({
        isVisible : this.props.isVisible
      }))
    }
  }
  //Fonctions sorties du render() pour performances et bind correct de this via méthode d'instance arrow function
  //https://medium.com/@adamjacobb/react-native-performance-arrow-functions-binding-3f09cbd57545
  mapButtons = (button) => {
    return <SemanticButton 
              text={button.text} 
              type={button.type} 
              //Fonction contextuelle pour désactiver en fonction du state (résa en cours ou pas) || props directement passée || false
              disabled={button.disabledFn && button.disabledFn() || button.disabled || false }
              onPress={()=>this.getDefaultFunction(button)}></SemanticButton>
  }
  //Wrapper pour appeler le onCancel par défaut s'il n'y a pas de onPress passé pour le bouton
  getDefaultFunction = (button) => {
    __DEV__ && console.tron.log(typeof button.onPress == "function");
    typeof button.onPress == "function" ? button.onPress() : this.props.onCancel()

    this.setState((partialState)=> ({
      isVisible : false
    }))
  }

  render () {
    return (
      <View>
        <Modal
          isVisible={this.state.isVisible}
          onBackdropPress={() => {this.props.onCancel()}}
          onSwipe={() => {this.props.onCancel()}}
          swipeDirection='down'
          swipeThreshold={300}
          backdropOpacity={0.9}
          avoidKeyboard={true}
          >
          <View style={styles.modal_container}>
            <Text style={styles.modal_title}>{this.props.title}</Text>
            <View style={styles.modal_view}>
              {this.props.children}
            </View>
            <View style={styles.modal_actions}>
              { this.props.buttons.map(this.mapButtons) }
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}
