import React, {  PureComponent } from 'react'
import { Text, View, Animated, TouchableOpacity } from 'react-native'

import PropTypes from 'prop-types';
import styles from './Styles/ListeLivresItemStyle'
import ListeLivresItemBadge from './ListeLivresItemBadge';

import { translate } from '../I18n';
import SemanticButton from './SemanticButton';
import moment from 'moment';

const DAYS_BEFORE_RENEW = 7;
const RETURN_DATE_FORMAT = 'DD/MM/YYYY';
/**
 * 
 * [
    {
        "date": "2021-09-16 12:18:07",
        "title": "Découvrez vos points forts",
        "author": "Marcus Buckingham & Donald Clifton",
        "book_number": "SH 4.1 BUC",
        "return_date": "15/03/2022",
        "barcode": "019341",
        "renewable": false
    },
    {
        "date": "2019-06-18 15:21:09",
        "title": "Manager le travail à distance et le télétravail",
        "author": "Daniel Ollivier ",
        "book_number": "10.2343 OLLI",
        "return_date": "30/09/2021",
        "barcode": "2075431137",
        "renewable": true
    }
]
 * @param {object} bookEntry
 * @returns Liste item
 */
const MIN_HEIGHT = 60;

export class ListeLivresItem extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      animation: new Animated.Value(MIN_HEIGHT),
      minHeight: MIN_HEIGHT,
    };
  }
  static propTypes = {
    item: PropTypes.object.isRequired,
  }
  _setMaxHeight(event) {
    // __DEV__ && console.tron.log(event.nativeEvent.layout)
    this.setState({
      maxHeight: event.nativeEvent.layout.height
    });
  }

  _setMinHeight(event) {
    this.setState({
      minHeight: event.nativeEvent.layout.height
    });
  }
  toggle() {
    //Step 1
    let initialValue = this.state.expanded ? this.state.maxHeight : this.state.minHeight,
      finalValue = this.state.expanded ? this.state.minHeight : this.state.maxHeight;

    this.setState({
      expanded: !this.state.expanded  //Step 2
    });

    this.state.animation.setValue(initialValue);  //Step 3
    Animated.spring(     //Step 4
      this.state.animation,
      {
        toValue: finalValue
      }
    ).start();  //Step 5
  }
  render() {
    const item = this.props.item,
          item_soon_return = moment(item.return_date, RETURN_DATE_FORMAT).isBefore(moment().add(DAYS_BEFORE_RENEW, "days"), "day");
    return (
      <Animated.View style={[styles.container, { backgroundColor: this.props.backgroundColor }, { height: this.state.animation }]} >
        <TouchableOpacity onPress={() => this.toggle()} onLayout={this._setMaxHeight.bind(this)}>
          <View style={[styles.row, styles.inline, { justifyContent: 'flex-start' }]}>
            <View style={[styles.rowContent, styles.inline_no_wrap, {justifyContent: "space-between",  alignItems:"center"}]}>
              <Text
                adjustsFontSizeToFit
                numberOfLines={2}
                style={[styles.title]}>{item.title}</Text>
              <ListeLivresItemBadge style={styles.list_item_subtitle} date={item.return_date} daysBeforeRenew={DAYS_BEFORE_RENEW} returnDateFormat={RETURN_DATE_FORMAT}/>
            </View>
          </View>
          <View style={styles.details}>
            <View style={[styles.inline, styles.details_date ]}>
              <View >
                <Text style={styles.label}>{translate('SCREEN.EMPRUNT_LIVRE.date')} : {item.date}</Text>
                <Text style={styles.label}>{translate('SCREEN.EMPRUNT_LIVRE.return_date')} : {item.return_date}</Text>
              </View>
              {item_soon_return && (item.renewable
                ? <SemanticButton
                  size="small"
                  type="success"
                  style={styles.label}
                  text={translate('SCREEN.EMPRUNT_LIVRE.renewable')} 
                  onPress={() => {this.props.onRenew({barcode: item.barcode, confirmed: 1})}}
                  />
                : <SemanticButton 
                  size="small" 
                  type="warning" 
                  disabled 
                  style={styles.label} 
                  text={translate('SCREEN.EMPRUNT_LIVRE.unrenewable')} />
              )}
            </View>
            <Text style={styles.label}>{translate('SCREEN.EMPRUNT_LIVRE.title')} : {item.title}</Text>
            <Text style={styles.label}>{translate('SCREEN.EMPRUNT_LIVRE.author')} : {item.author}</Text>
            <Text style={styles.label}>{translate('SCREEN.EMPRUNT_LIVRE.book_number')} : {item.book_number}</Text>
            <Text style={styles.label}>{translate('SCREEN.EMPRUNT_LIVRE.barcode')} : {item.barcode}</Text>
          </View>
        </TouchableOpacity>
      </Animated.View>
    )
  }
}