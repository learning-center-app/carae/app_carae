import React, { Component, PureComponent } from 'react'
import { ActivityIndicator, Text } from 'react-native'
import { FlatList, View, Animated, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'


import EmpruntLivreActions from '../Redux/EmpruntLivreRedux'
import UtilisateurActions from '../Redux/UtilisateurRedux'
import { ListeLivresItem } from './ListeLivresItem'



class ListeLivres extends Component {
  componentDidMount() {
    this.props.getListeLivres(this.props.utilisateur.id_koha);
  }
  render() {

    const { fetchingListeLivres, errorFetching, books } = this.props;
    if(fetchingListeLivres) {
      return (
        <ActivityIndicator size="large"/>
      )
    }
    if (errorFetching && !books) {
      console.tron.log({'errorFetching' : errorFetching})
      return (
        <Text>{JSON.stringify(errorFetching, null, 2)}</Text>
      )
    }
    return (
      <FlatList
        data={books}
        renderItem={(props) => 
          <ListeLivresItem {...props} backgroundColor={this.props.backgroundColor} onRenew={({barcode, confirmed}) => { this.props.postEmpruntLivre({utilisateur: this.props.utilisateur, barcode, confirmed})}}/>
        }
        keyExtractor={item => item.barcode}
      />
    )
  }
}

ListeLivres.defaultProps = {
  fetchingListeLivres: {},
  errorFetching: {},
  last_book: {},
}
const mapStateToProps = (state) => {
  return {
    fetchingListeLivres: state.emprunt_livre.fetchingListeLivres,
    errorFetching: state.emprunt_livre.errorFetching,
    books: state.emprunt_livre.books,
    utilisateur: state.utilisateur,
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    utilisateurShowLogin: () => { dispatch(UtilisateurActions.utilisateurShowLogin()) },
    postEmpruntLivre: data => { dispatch(EmpruntLivreActions.empruntLivreRequest(data)) },
    getListeLivres: id_koha => { dispatch(EmpruntLivreActions.listeLivresRequest(id_koha)) },
    resetEmpruntLivre: () => { dispatch(EmpruntLivreActions.empruntLivreReset()) },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListeLivres)