import { StyleSheet } from 'react-native'
import { Metrics } from '../../Themes'
export default StyleSheet.create({

  padded: {
    paddingLeft: Metrics.doubleBaseMargin,
    paddingRight: Metrics.baseMargin,
    paddingHorizontal: Metrics.doubleBaseMargin,
  },
  container: {
    flex: 1
  },
  icon: {
    width: 30,
    height: 30,
    padding: 10,
    resizeMode: 'contain'
  }
})
