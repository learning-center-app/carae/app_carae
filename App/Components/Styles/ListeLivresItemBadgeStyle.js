import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors, Fonts, Metrics } from '../../Themes'

export default StyleSheet.create({
  label: {
    color: Colors.white,
    fontWeight: "bold"
    // fontSize: Fonts.size.small
  },
  badge: {
    paddingVertical: 2,
    paddingHorizontal: Metrics.baseMargin,
    borderRadius: 30
  },
  late: {
    backgroundColor: Colors.alert,
  },
  soon: {
    backgroundColor: Colors.warning,
  },
  ok: {
    backgroundColor: Colors.success,
  }
})
