import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Fonts } from '../../Themes'

export default StyleSheet.create({
  inline: {
    flexDirection:'row',
    alignSelf:'center',
    alignItems:'center',
    justifyContent:'center',
    // flexWrap:'nowrap',
    // height:38,
    textAlign:'center'
  },
  icon : {
    marginRight:8,
    resizeMode: 'contain',
    width:26,
    height:20,
  },
  title: {
    ...Fonts.style.normal,
    fontSize:19,
  }
})
