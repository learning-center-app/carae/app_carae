import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Metrics } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  ...ApplicationStyles.list_item,
  container: {
    overflow:'hidden'
  },
  row : {
    width: '90%',
    marginHorizontal: '5%'
  },
  rowContent: {
    padding: 12,
    height:100,
    flex:1
  },

  illustration: {
    height:80,
    width:80,
    resizeMode: 'cover',
    borderRadius:8,
  },
  label :{
    ...Fonts.style.normal,
    paddingVertical:Metrics.baseMargin,
    paddingHorizontal:Metrics.baseMargin,
  },
  html :{
    paddingHorizontal:Metrics.baseMargin,
    paddingBottom: Metrics.baseMargin,
  }

})
