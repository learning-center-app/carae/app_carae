import { Images, Fonts, Colors } from '../../Themes'
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    overflow:'hidden'
  },
  row : {
    width: '90%',
    marginHorizontal: '5%'
  },
  rowContent: {
    padding: 12,
    height:100,
    flex:1
  },
  inline: {
    flexWrap: 'wrap',
    alignItems: 'center',
    flexDirection:'row',
    justifyContent:'space-between'
  },

  illustration: {
    height:80,
    width:80,
    resizeMode: 'cover',
    borderRadius:8,
  },
  label :{
    ...Fonts.style.normal,
    paddingVertical:20,
    paddingHorizontal:10,
  }

})
