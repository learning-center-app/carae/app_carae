import { StyleSheet } from 'react-native'
import { Fonts, Colors, Metrics } from '../../Themes/'

export default StyleSheet.create({
  button: {
    borderRadius: 0,
    paddingVertical: Metrics.doubleBaseMargin,
    paddingHorizontal: Metrics.doubleBaseMargin,
    // marginHorizontal: Metrics.section,
    marginVertical: Metrics.baseMargin,
    backgroundColor: Colors.info,
    justifyContent: 'center',
    minWidth: 100,
  },
  button_small : {
    paddingVertical: Metrics.baseMargin,
    paddingHorizontal: Metrics.baseMargin,
    marginVertical: Metrics.smallMargin,
    minWidth: 60,
  },
  buttonText: {
    color: Colors.dark,
    textAlign: 'center',
    fontWeight: 'normal',
    fontSize: Fonts.size.regular,
    fontFamily: Fonts.type.base,
  },
  button_text_small : {
    fontSize: Fonts.size.small,
  }
})
