import { StyleSheet } from 'react-native'
import { ApplicationStyles, Fonts, Metrics, Colors } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  ...ApplicationStyles.list_item,
  container: {
    overflow: 'hidden',
    borderBottomColor: "rgba(0,0,0,0.1)",
    borderBottomWidth: 1,
    // paddingBottom: Metrics.baseMargin,
  },
  row : {
    width: '90%',
    marginHorizontal: '5%'
  },
  rowContent: {
    paddingVertical: Metrics.baseMargin,
    height:60,
    flex:1
  },
  title: {
    ...Fonts.style.h5,
    maxWidth: '80%'
  },
  label :{
    ...Fonts.style.description,
    color: Colors.white,
  },
  details :{
    marginHorizontal:Metrics.baseMargin *2,
    paddingBottom: Metrics.baseMargin *3,
  },
  details_date: {
    width: '100%',
    justifyContent: "space-between",
    alignItems: "center",
    paddingBottom: Metrics.baseMargin,
  }

})
