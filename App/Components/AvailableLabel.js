import React, { PureComponent } from 'react'
// import PropTypes from 'prop-types';
import { View, Text } from 'react-native'
import styles from './Styles/AvailableLabelStyle'

import PropTypes from 'prop-types';

import { Colors } from '../Themes'

//Internationalisation
import { translate } from '../I18n'

export default class AvailableLabel extends PureComponent {
  constructor(props) {
    super(props);
    this.state = this.getButtonTypeFromStatus(props.status)
  }

  //Mise à jour du state si les props changent
  componentWillReceiveProps(nextProps) {
    this.setState(this.getButtonTypeFromStatus(nextProps.status))
  }

  getButtonTypeFromStatus = (type) => {
    let text, color;
    switch (type) {
      case 'conflict':
        color = Colors.warning;
        text = translate('COMPONENT.AVAILABILITY.conflit');
        break;
      case 'available':
        color = Colors.success;
        text = translate('COMPONENT.AVAILABILITY.disponible');
        break;
      case 'unavailable':
        color = Colors.error;
        text = translate('COMPONENT.AVAILABILITY.indisponible');
        break;
      case 'reserved':
        color = Colors.info;
        text = `${translate('COMPONENT.AVAILABILITY.reserve' + (this.props.gender == 'feminine' ? 'e' : ''))} ${translate('COMPONENT.AVAILABILITY.par_moi')}`;
        break;
      default:
        color = Colors.info;
        text = translate('COMPONENT.AVAILABILITY.inconnu');
        break;
    }
    return {text, color};
  }
  // bgColor = {}

  static propTypes = {
    status: PropTypes.string,
    gender: PropTypes.oneOf(['masculine', 'feminine']), // Masculin ou féminin
    onPress: PropTypes.func
  }

  render () {
    return (
      <View style={[styles.inline]}>
        <Text style={[styles.white_block, styles.base, {color: this.state.color}]}>{this.state.text}</Text>
      </View>
    )
  }
}
