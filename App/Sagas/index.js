import { takeLatest, all } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { GithubTypes } from '../Redux/GithubRedux'
import { UtilisateurTypes } from '../Redux/UtilisateurRedux'
import { HtmlTypes } from '../Redux/HtmlRedux'
import { RendezVousTypes } from '../Redux/RendezVousRedux'
import { EmpruntLivreTypes } from '../Redux/EmpruntLivreRedux'
import { DemandeReferenceTypes } from '../Redux/DemandeReferenceRedux'
import { SuggestionTypes } from '../Redux/SuggestionRedux'
import { SallesTypes } from '../Redux/SallesRedux'
import { MaterielTypes } from '../Redux/MaterielRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { getUserAvatar } from './GithubSagas'

import { postSuggestion } from './SuggestionSagas'
import { postDemandeReference } from './DemandeReferenceSagas'
import { postRendezVous } from './RendezVousSagas'
import { getListeLivres, postEmpruntLivre } from './EmpruntLivreSagas'
import { login, upsertUtilisateurGrr } from './UtilisateurSagas'
import { getHtmlCgu } from './HtmlSagas'
import { getSalles, getSallesReservations, putSalleReservation, postSalleReservation, deleteSalleReservation} from './SallesSagas'
import { getMateriel, getMaterielReservations, putMaterielReservation, postMaterielReservation, deleteMaterielReservation} from './MaterielSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),

    // Récupération des dernières CGUs
    takeLatest(HtmlTypes.REQUEST_CGU, getHtmlCgu, api),


    /*   SALLES    */
    // Récupération des salles de collaboration
    takeLatest(SallesTypes.SALLES_REQUEST, getSalles, api),

    // Récupération des réservations effectuées sur les salles de collaboration
    takeLatest(SallesTypes.SALLES_RESERVATIONS_REQUEST, getSallesReservations, api),

    // Envoi d'une résa appelle l'API
    takeLatest(SallesTypes.PUT_SALLE_RESERVATION_REQUEST, putSalleReservation, api),

    // Modification d'une résa appelle l'API
    takeLatest(SallesTypes.POST_SALLE_RESERVATION_REQUEST, postSalleReservation, api),

    // Suppression d'une résa appelle l'API
    takeLatest(SallesTypes.DELETE_SALLE_RESERVATION_REQUEST, deleteSalleReservation, api),

    /*   MATOS    */
    // Récupération des salles de collaboration
    takeLatest(MaterielTypes.MATERIEL_REQUEST, getMateriel, api),

    // Récupération des réservations effectuées sur les salles de collaboration
    takeLatest(MaterielTypes.MATERIEL_RESERVATIONS_REQUEST, getMaterielReservations, api),

    // Envoi d'une résa appelle l'API
    takeLatest(MaterielTypes.PUT_MATERIEL_RESERVATION_REQUEST, putMaterielReservation, api),

    // Modification d'une résa appelle l'API
    takeLatest(MaterielTypes.POST_MATERIEL_RESERVATION_REQUEST, postMaterielReservation, api),

    // Suppression d'une résa appelle l'API
    takeLatest(MaterielTypes.DELETE_MATERIEL_RESERVATION_REQUEST, deleteMaterielReservation, api),

    /*   UTILISATEUR    */
    // Login via Shibboleth
    takeLatest(UtilisateurTypes.UTILISATEUR_SUCCESS, upsertUtilisateurGrr, api),
    takeLatest(UtilisateurTypes.UPSERT_UTILISATEUR_GRR, upsertUtilisateurGrr, api),

    /*    SUGGESTIONS   */
    // Envoi d'une suggestion
    takeLatest(SuggestionTypes.SUGGESTION_REQUEST, postSuggestion, api),

    /*   RENDEZ-VOUS   */
    // Envoi d'une demande de rendez-vous
    takeLatest(RendezVousTypes.RENDEZ_VOUS_REQUEST, postRendezVous, api),

    /*   EMPRUNT LIVRE   */
    // Envoi d'une demande de réservation
    takeLatest(EmpruntLivreTypes.EMPRUNT_LIVRE_REQUEST, postEmpruntLivre, api),

    /*   LISTE LIVRES   */
    // Récupération des livres actuellement empruntés
    takeLatest(EmpruntLivreTypes.LISTE_LIVRES_REQUEST, getListeLivres, api),
    
    /*   DEMANDE DE REFERENCE   */
    // Envoi d'une demande de référence
    takeLatest(DemandeReferenceTypes.DEMANDE_REFERENCE_REQUEST, postDemandeReference, api),

  ])
}
