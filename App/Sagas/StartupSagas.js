import { put, select } from 'redux-saga/effects'
import GithubActions, { GithubSelectors } from '../Redux/GithubRedux'
import UtilisateurActions, { UtilisateurSelectors } from '../Redux/UtilisateurRedux'
import HtmlActions from '../Redux/HtmlRedux'
import SallesActions from '../Redux/SallesRedux'
import MaterielActions from '../Redux/MaterielRedux'
import { is } from 'ramda'

// exported to make available for tests
export const selectAvatar = GithubSelectors.selectAvatar
export const getCguVersion = UtilisateurSelectors.getCguVersion

// process STARTUP actions
export function * startup (action) {
  if (__DEV__ && console.tron) { }

  //Récupération des CGUs à chaque lancement
  yield put(HtmlActions.requestCgu())

  //Reload des salles à chaque startup
  yield put(SallesActions.sallesRequest())
  //Chopage des résas de salles
  yield put(SallesActions.sallesReservationsRequest())

  //Reload des materiel à chaque startup
  yield put(MaterielActions.materielRequest())
  //Chopage des résas de materiel
  yield put(MaterielActions.materielReservationsRequest())

}
