/* ***********************************************************
* A short word on how to use this automagically generated file.
* We're often asked in the ignite gitter channel how to connect
* to a to a third party api, so we thought we'd demonstrate - but
* you should know you can use sagas for other flow control too.
*
* Other points:
*  - You'll need to add this saga to sagas/index.js
*  - This template uses the api declared in sagas/index.js, so
*    you'll need to define a constant in that file.
*************************************************************/

import { call, put } from 'redux-saga/effects'
import EmpruntLivreActions from '../Redux/EmpruntLivreRedux'
import UtilisateurActions from '../Redux/UtilisateurRedux'

export function * postEmpruntLivre (api, action) {
  const { data } = action
  // make the call to the api
  const response = yield call(api.postEmpruntLivre, data)

  if (response.ok) {
    yield put(EmpruntLivreActions.empruntLivreSuccess(response.data))
  }
  else {
    if (!response.status){
      yield put(EmpruntLivreActions.empruntLivreReset())
      yield put(UtilisateurActions.utilisateurShowInternet())
    }
    else if ([401, 403, 404, 409].indexOf(response.status) >= 0)
      yield put(EmpruntLivreActions.empruntLivreFailure(response.data))
    else
      yield put(EmpruntLivreActions.empruntLivreFailure())
  }
}

export function* getListeLivres (api, action) {
  const { data } = action
  // make the call to the api
  const response = yield call(api.getListeLivres, {id_koha : data})

  if (response.ok) {
    yield put(EmpruntLivreActions.listeLivresSuccess(response.data))
  }
  else {
    if (!response.status){
      yield put(UtilisateurActions.utilisateurShowInternet())
      yield put(EmpruntLivreActions.listeLivresFailure())
    }
    else if ([401, 403, 404, 409].indexOf(response.status) >= 0)
      yield put(EmpruntLivreActions.listeLivresFailure(response.data))
    else
      yield put(EmpruntLivreActions.listeLivresFailure())
  }
}
