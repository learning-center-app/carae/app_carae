// @flow

import React, { Component } from 'react'
import { View, StatusBar, Image, SafeAreaView } from 'react-native'
import { connect } from 'react-redux'
import StartupActions from '../Redux/StartupRedux'
import ReduxPersist from '../Config/ReduxPersist'

// Styles
import styles from './Styles/RootContainerStyles'

import LoginModal from '../Components/LoginModal'
import InternetModal from '../Components/InternetModal'

import AppNavigation from '../Navigation/AppNavigation'

import { Images } from '../Themes'
import NavigationService from '../Services/NavigationService'
//https://v1.reactnavigation.org/docs/handling-iphonex.html
// import { SafeAreaView } from 'react-navigation';

class RootContainer extends Component {
  componentDidMount () {
    // if redux persist is not active fire startup action
    if (!ReduxPersist.active) {
      this.props.startup()
    }
  }

  render () {
    return (
      <SafeAreaView style={styles.applicationView}>
        <StatusBar barStyle='light-content' />
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch'></Image>
        <AppNavigation ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}/>
        <LoginModal />
        <InternetModal />
      </SafeAreaView>
    )
  }
}

RootContainer.defaultProps = {
}

const mapStateToProps = (state) => {
  return {
  }
}

// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = (dispatch) => ({
  startup: () => dispatch(StartupActions.startup())
})

export default connect(null, mapDispatchToProps)(RootContainer)
