//@flow
import React, { Component } from 'react'
import { Platform, StatusBar, ScrollView, Text, View } from 'react-native'
import { connect } from 'react-redux'

import { getStatusBarHeight } from 'react-native-status-bar-height'

import SemanticButton from '../Components/SemanticButton'
import Picker from '../Components/Picker'
import UtilisateurActions, {LIST_CAMPUS} from '../Redux/UtilisateurRedux'

// Styles
import styles from './Styles/LoginCampusScreenStyle'
import { Colors} from '../Themes'
//Internationalisation
import { translate } from '../I18n'

class LoginCampusScreen extends Component {
  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {
      ...props.navigation.state.params,
      ...this.state,
      id_campus : props.campus ? props.campus.id : null,
      color: Colors.background,
    };

    Platform.OS === "ios" && StatusBar.setBarStyle('dark-content', true);
  }
  componentWillUnmount () {
    Platform.OS === "ios" && StatusBar.setBarStyle('light-content', true);
  }

  getExplication = () => {
    return `${this.state.error == "no_campus" ? translate('SCREEN.LOGIN_CAMPUS.campus_error') +"\n" : ""}${translate('SCREEN.LOGIN_CAMPUS.description')}`
  }
  setUtilisateurCampus = () => {
    let _campus = LIST_CAMPUS.find(campus => { return campus.id == this.state.id_campus})
    if (_campus) {
      this.props.setUtilisateurCampus(_campus);
      this.props.navigation.goBack();
    }
  }

  /** Traduit les options récupérées du redux via i18n */
  renderLocalizedCampusOptions = () => {
    let categories = translate('LABEL_MAP.VILLE');
    return Object.keys(categories)
      .map((key) => ({ value: key, label: categories[key] }))
  }
  render () {
    return (
      <View style={[styles.container, {backgroundColor: this.state.color}]}>
        <View style={[styles.sub_nav, styles.modalPaddingIphone]}><Text style={styles.sub_nav_text}>{translate('SCREEN.LOGIN_CAMPUS.subtitle')}</Text></View>
        <ScrollView>
          <View>
            <Text style={[styles.label, styles.padded, styles.align_center]}>
              {this.getExplication()}
            </Text>
          </View>
          <View>
            <Picker
              placeholder={{
                label: translate('FORM.PLACEHOLDER.campus'),
                value:null
              }}
              items={this.renderLocalizedCampusOptions()}
              onValueChange={(value, index) => this.setState({id_campus : value})}
              value={this.state.id_campus}
              />
          </View>
          <View style={[styles.alert, styles.alert_info, styles.margedVertical]}>
            <Text style={[styles.label, styles.align_center]}>{translate('SCREEN.LOGIN_CAMPUS.note')}</Text>
          </View>
          <View style={[styles.actions]}>
            <SemanticButton
              text={translate('COMMON.annuler')}
              type="warning"
              onPress={()=>this.props.navigation.goBack()}
            />
            <SemanticButton
              text={translate('COMMON.valider')}
              type="success"
              onPress={()=>this.setUtilisateurCampus()}
            />
          </View>
        </ScrollView>

      </View>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    campus : state.utilisateur.campus,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setVersionCguAccepte : (version_cgu_accepte) => {dispatch(UtilisateurActions.setVersionCguAccepte(version_cgu_accepte))},
    setUtilisateur : (utilisateur) => {dispatch(UtilisateurActions.utilisateurSuccess(utilisateur))},
    setUtilisateurCampus : (campus) => {dispatch(UtilisateurActions.setUtilisateurCampus(campus))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginCampusScreen)
