//@flow
import React, { Component } from 'react'
import { ScrollView, Text,TextInput, View } from 'react-native'
import { connect } from 'react-redux'

import BetterKeyboardAvoidingView from '../Components/BetterKeyboardAvoidingView';
import SemanticButton from '../Components/SemanticButton'

import Picker from '../Components/Picker'

// Styles
import { Fonts, Colors } from '../Themes'
import styles from './Styles/RendezVousScreenStyle'
import Modal from '../Components/Modal'
import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'

// const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

import RendezVousActions from '../Redux/RendezVousRedux'
import UtilisateurActions from '../Redux/UtilisateurRedux'

//Internationalisation
import { translate } from '../I18n'

class RendezVousScreen extends Component {
  static navigationOptions = ({navigation}) => ({
    //Utilisation d'un composant custom dans headerTitle (et pas title, crash sur android)
    //https://github.com/facebook/react-native/issues/10232#issuecomment-398950751
    headerTitle: <NavbarTitleWithIcon icon={navigation.state.params.icon} title={translate(`SCREEN_NAME_MAP.${navigation.state.params.screen}`)} />,
  })
  static localStyle = ({navigation}) => ({
    color: navigation.state.params.title,
  })

  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {
      ...props.navigation.state.params,
      ...this.state
    };
  }
  state = {
    data: {
      objet: {value: null, index: null},
      message: {value: null, index: null},
      destinataire: {value: null, index: null},
      campus: {value: null, index: null},
      modalite: {value: null, index: null},
      priorite: {value: null, index: null},
    },
    modalsuccess:false,
    modalfail:false,
    button_label: translate('COMMON.envoyer'),
  }
  FAIL_BUTTONS = [
    {
      text: translate('COMMON.reessayer'),
      type : 'error',
      onPress : () => {
        this.setState((partialState)=> ({
          ...partialState,
          modalfail : false
        }))
      }
    }
  ]
  SUCCESS_BUTTONS = [
    {
      text: translate('COMMON.ok'),
      type : 'success',
      onPress : () => {
        this.setState((partialState)=> ({
          ...partialState,
          modalsuccess : false
        }))
      }
    }
  ]

  //Hook sur le changement de props
  //https://stackoverflow.com/a/44750890/1437016
  componentWillReceiveProps(props) {
    if (props.sending) {
      this.setState({ button_label: translate('COMMON.en_cours') })
    }
    else if (!props.sending) {
      if (!this.state.modalfail && props.error) {
        this.setState({ modalfail: true, button_label: translate('COMMON.reessayer') })
      }
      else if (!this.state.modalsuccess) {
        this.setState({ modalsuccess: true, button_label: translate('COMMON.envoye') })
      }
    }
  }

  postRendezVous = () => {
    //Si on est déjà en train d'envoyer, on ignore le touch pour éviter le double envoi
    if (this.props.sending) {
      return;
    }
    if (!this.props.utilisateur.userId) {
      return this.props.utilisateurShowLogin();
    }

    try {
      let data = {
        rendez_vous: {
          objet:this.state.data.objet,
          message:this.state.data.message,
          destinataire: {
            value: this.state.data.destinataire.value,
            label: translate(`LABEL_MAP.DESTINATAIRE.${this.state.data.destinataire.value}`)
          },
          campus: {
            value: this.state.data.campus.value,
            label: translate(`LABEL_MAP.CAMPUS.${this.state.data.campus.value}`)
          },
          modalite: {
            value: this.state.data.modalite.value,
            label: translate(`LABEL_MAP.MODALITE.${this.state.data.modalite.value}`)
          },
          priorite: {
            value: this.state.data.priorite.value,
            label: translate(`LABEL_MAP.PRIORITE.${this.state.data.priorite.value}`)
          },
          text: this.state.data.text,
        },
        utilisateur: this.props.utilisateur
      }
      // __DEV__ && console.tron.log({postFormWith: data});
      this.props.postRendezVous(data);
    }
    catch(e) {
      __DEV__ && console.tron.log({errorFormRdv : e})
      this.setState({ modalfail: true, button_label: translate('COMMON.reessayer'), error: translate('ERROR.champs_requis'), });
    }
  }


  /** Traduit les options récupérées du redux via i18n 
   * @param i18n_key - ex: 'LABEL_MAP.SUGGESTION_CATEGORIES'
   * @returns Options pour un select - ex: [{value:X, label:Y}]
  */
  renderLocalizedOptions = (i18n_key) => {
    let options = translate(i18n_key);
    return Object.keys(options)
      .map((key) => ({ value: key, label: options[key] }))
  }

  render () {
    return (
      <View style={[styles.container, {backgroundColor: this.state.color}]}>
        <View style={styles.sub_nav}><Text style={styles.sub_nav_text}>{translate('SCREEN.RENDEZ_VOUS.subtitle')}</Text></View>
        <ScrollView>
          <BetterKeyboardAvoidingView>
            <View style={styles.input_with_margin_top}>
              <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.destinataire')}</Text>
              <Picker
                placeholder={{
                  label:translate('FORM.PLACEHOLDER.destinataire'),
                  value:null
                }}
                items={this.renderLocalizedOptions('LABEL_MAP.DESTINATAIRE')}
                onValueChange={(value, index) => this.setState({data : {...this.state.data, destinataire: {value: value, index: index-1}}})}
              />
            </View>
            <View style={styles.input_with_margin_top}>
              <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.campus')}</Text>
              <Picker
              placeholder={{
                label:translate('FORM.PLACEHOLDER.campus'),
                value:1
              }}
              items={this.renderLocalizedOptions('LABEL_MAP.VILLE')}
              onValueChange={(value, index) => this.setState({data : {...this.state.data, campus: {value: value, index: index-1}}})}
              />
            </View>
            <View style={styles.input_with_margin_top}>
              <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.rdv_priorite')}</Text>
              <Picker
                placeholder={{
                  label:translate('FORM.PLACEHOLDER.rdv_priorite'),
                  value:null
                }}
                items={this.renderLocalizedOptions('LABEL_MAP.PRIORITE')}
                onValueChange={(value, index) => this.setState({data : {...this.state.data, priorite: {value: value, index: index-1}}})}
              />
            </View>
            <View style={styles.input_with_margin_top}>
              <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.rdv_modalite')}</Text>
              <Picker
                placeholder={{
                  label:translate('FORM.PLACEHOLDER.rdv_modalite'),
                  value:null
                }}
                items={this.renderLocalizedOptions('LABEL_MAP.MODALITE')}
                onValueChange={(value, index) => this.setState({data : {...this.state.data, modalite: {value: value, index: index-1}}})}
              />
            </View>

            <View style={styles.input_with_margin_top}>
              <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.descriptif_demande')}</Text>
              <TextInput
                placeholder={translate('FORM.PLACEHOLDER.rdv_objet_demande')}
                placeholderTextColor={Colors.slightlyTransparentText}
                onChangeText={(text) => this.setState({data : {...this.state.data, objet:text}})}
                style={[styles.input, styles.padded]}
              >
              </TextInput>
            </View>

            <View style={styles.input_with_margin_top}>
              <TextInput
                multiline={true}
                maxLength={350}
                onChangeText={(text) => this.setState({data : {...this.state.data, message:text}})}
                placeholder={translate('FORM.PLACEHOLDER.descriptif_demande')}
                placeholderTextColor={Colors.slightlyTransparentText}
                style={[styles.multilineText, styles.input, styles.padded]}
              />
            </View>
            <View style={styles.actions}>
              <SemanticButton
                text={this.state.button_label}
                onPress={()=>{ this.postRendezVous() }}
                type="success" />
            </View>
          </BetterKeyboardAvoidingView>
        </ScrollView>
        <Modal
          isVisible={this.state.modalsuccess}
          onCancel={()=>this.setState({modalsuccess: false})}
          title=""
          buttons={this.SUCCESS_BUTTONS}>

          <View>
            <Text style={[
              Fonts.style.h1,
              styles.centerText,
              ]}
            >
              {translate('MODAL.RDV.SUCCESS.title')}
            </Text>
            <Text style={[
              Fonts.style.h4,
              styles.centerText
              ]}
            >
              {translate('MODAL.RDV.SUCCESS.body')}
            </Text>
          </View>
        </Modal>
        <Modal
          isVisible={this.state.modalfail}
          onCancel={()=>this.setState({modalfail: false})}
          title=""
          buttons={this.FAIL_BUTTONS}>

          <View>
            <Text style={[
              Fonts.style.h1,
              styles.centerText
              ]}
            >
              {translate('MODAL.RDV.ERROR.title')}
            </Text>

            <Text style={[
                Fonts.style.h3,
                styles.centerText,
                {paddingTop:30, paddingBottom:30}
                ]}>
                {this.props.error || this.state.error}
            </Text>
          </View>
        </Modal>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    sending : state.rendez_vous.sending,
    error : state.rendez_vous.error,
    utilisateur : state.utilisateur,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    utilisateurShowLogin: ()=>{dispatch(UtilisateurActions.utilisateurShowLogin())},
    postRendezVous : data => {dispatch(RendezVousActions.rendezVousRequest(data))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RendezVousScreen)
