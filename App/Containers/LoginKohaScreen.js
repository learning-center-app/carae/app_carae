//@flow
import React, { Component } from 'react'
import { TextInput, Platform, Image, StatusBar, ScrollView, Text, View } from 'react-native'
import { connect } from 'react-redux'

import { getStatusBarHeight } from 'react-native-status-bar-height'
import QRCodeScanner from 'react-native-qrcode-scanner'

import BetterKeyboardAvoidingView from '../Components/BetterKeyboardAvoidingView';
import SemanticButton from '../Components/SemanticButton'
import UtilisateurActions from '../Redux/UtilisateurRedux'

//Internationalisation
import { translate } from '../I18n'

// Styles
import styles from './Styles/LoginKohaScreenStyle'
import { Images, Colors} from '../Themes'
import { RNCamera } from 'react-native-camera';

class LoginKohaScreen extends Component {
  static navigationOptions = ({navigation}) => {
    const params = navigation.state.params || {};

    return {
      title: params.title,
    }

  }


  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {
      ...props.navigation.state.params,
      ...this.state,
      focusedScreen: false,
      id_koha : props.id_koha,
    };

    Platform.OS === "ios" && StatusBar.setBarStyle('dark-content', true);
  }
  componentDidMount() {
    //Ajout d'un booléen de state qui check si l'écran est affiché : si on charge la caméra dans l'écran de scan de badge, ça freeze la caméra de cet écran
    // -> focusedScreen utilise les events de la navigation pour forcer un re-render quand on revient sur l'écran
    const { navigation } = this.props;
    navigation.addListener('willFocus', () =>
      setTimeout(()=>this.setState({ focusedScreen: true }), 200)
    );
    navigation.addListener('willBlur', () =>
      this.setState({ focusedScreen: false })
    );
  }
  componentWillUnmount () {
    Platform.OS === "ios" && StatusBar.setBarStyle('light-content', true);
  }
  //Référence scanner QRCode
  scanner = {}

  getExplication = () => {
    if (this.state.error == "incorrect_id")
      return translate("SCREEN.LOGIN_KOHA.explication_error");
    return translate("SCREEN.LOGIN_KOHA.explication");
  }

  render () {
    return (
      <View style={[styles.container, {backgroundColor: this.state.color}]}>
        <View style={[styles.sub_nav, styles.modalPaddingIphone]}><Text style={styles.sub_nav_text}>{translate("SCREEN.LOGIN_KOHA.subtitle")}</Text></View>
        <ScrollView>
          <BetterKeyboardAvoidingView>
            <View>
              <Text style={[styles.label, styles.padded, styles.align_center]}>
                {this.getExplication()}
              </Text>
              <Image source={Images.carte} style={styles.image_carte}></Image>
            </View>
            <View>
              {!!this.state.focusedScreen && (
                <QRCodeScanner
                  onRead={(event) => this.setState({'id_koha':event.data})}
                  cameraProps={{
                    captureAudio: false,
                    // onBarCodeRead: null,
                    // googleVisionBarcodeType: RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeType.CODABAR,
                    // onGoogleVisionBarcodesDetected: (event) => {
                    //   // __DEV__ && console.tron.log({...event, _barcodeDetected: event.barcodes.length})
                    //   if (event.barcodes && event.barcodes.length) {
                    //     __DEV__ && console.tron.log({ data: event.barcodes[0].data, event })
                    //     this.setState({ 'id_koha': event.barcodes[0].data })
                    //   }
                    // }
                  }}
                  reactivate={true}
                  reactivateTimeout={3000}
                  ref={(node) => { this.scanner = node }}
                  showMarker={true}
                  containerStyle={{ height: 150 }}
                  cameraContainerStyle={{ height: 150 }}
                  cameraStyle={{ height: 150, overflow: 'hidden' }} //https://github.com/moaazsidat/react-native-qrcode-scanner/issues/173#issuecomment-506326477
                  markerStyle={{ height: 75 }}
                />)
              }
              <Text style={[styles.label, styles.padded, styles.align_center]}>{translate('SCREEN.EMPRUNT_LIVRE.input_code')}</Text>
              <TextInput
                style={[styles.input, styles.padded, styles.textInput]}
                placeholder="ex : 2400001234567"
                keyboardType='number-pad'
                placeholderTextColor = {Colors.slightlyTransparentText}
                enablesReturnKeyAutomatically={false}
                value={this.state.id_koha}
                onChangeText={(value)=>this.setState({'id_koha':value})}
              />
            </View>
            <View style={[styles.actions]}>
              <SemanticButton
                text={translate("COMMON.annuler")}
                type="warning"
                onPress={()=> {this.props.navigation.goBack();}}
              />
              <SemanticButton
                text={translate("COMMON.valider")}
                type="success"
                onPress={()=> {this.props.setUtilisateurIdKoha(this.state.id_koha); this.props.navigation.goBack();}}
              />
            </View>
          </BetterKeyboardAvoidingView>
        </ScrollView>

      </View>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    id_koha : state.utilisateur.id_koha,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setVersionCguAccepte : (version_cgu_accepte) => {dispatch(UtilisateurActions.setVersionCguAccepte(version_cgu_accepte))},
    setUtilisateur : (utilisateur) => {dispatch(UtilisateurActions.utilisateurSuccess(utilisateur))},
    setUtilisateurIdKoha : (id_koha) => {dispatch(UtilisateurActions.setUtilisateurIdKoha(id_koha))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginKohaScreen)
