//@flow
import React from 'react'
import { View, SectionList, Text, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'

import moment from 'moment'
import 'moment-round'

import ListItem from '../Components/ListItem'

import Modal from '../Components/Modal'
import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'

//Actions redux
import MaterielActions, { MaterielSelectors } from '../Redux/MaterielRedux'
import UtilisateurActions from '../Redux/UtilisateurRedux'

import { Calendar } from 'react-native-calendars'
// More info here: https://facebook.github.io/react-native/docs/sectionlist.html

//Internationalisation
import { translate } from '../I18n'
import i18n from 'i18n-js'

// Styles
import styles from './Styles/ReservationMaterielScreenStyle'
import { ApplicationStyles, Images, Fonts, Colors } from '../Themes'
import { TextInput } from 'react-native'
import SanitizeStringForSearch from '../Transforms/SanitizeStringForSearch'

class ReservationMaterielScreen extends React.PureComponent {
  static navigationOptions = ({navigation}) => ({
    //Utilisation d'un composant custom dans headerTitle (et pas title, crash sur android)
    //https://github.com/facebook/react-native/issues/10232#issuecomment-398950751
    headerTitle: <NavbarTitleWithIcon icon={navigation.state.params.icon} title={translate(`SCREEN_NAME_MAP.${navigation.state.params.screen}`)} />,

    //https://github.com/react-navigation/react-navigation/issues/145#issuecomment-281418095
    headerRight: <TouchableOpacity style={styles.navbar_button} onPress={() => { navigation.state.params.showSearch() }}>
      <Image source={Images.actions_icons.dark.recherche} style={[styles.navbar_icon]}></Image>
    </TouchableOpacity>,
  })

  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {
      ...props.navigation.state.params,
      filters: {
        start_time: moment().add(1, 'day').hour(9).minutes(0),
        end_time: moment().add(2, 'day').hour(14).minutes(0)
      },
      modal: {
        calendar: false,
        hours: false,
      },
      data: props.sorted_by_campus,
      section_displayed: null, //props.sorted_by_campus ? props.sorted_by_campus[0].id : null, //Affichage de la première section par défaut
      search: null
    };
    //Utilisation de la locale actuelle pour moment
    moment.locale(i18n.locale)
  }
  componentDidMount () {
    //Chopage des résas de materiel au cas où
    // this.props.materielReservationsRequest();
    this.props.navigation.setParams({ 
      showSearch: () => this.setState({ search: this.state.search == null ? "" : null })
    });
  }
  componentDidUpdate (prevProps, prevState) {
    if(prevState.search != this.state.search){
      const sanitized_search = SanitizeStringForSearch(this.state.search);
      let filtered_data = this.props.sorted_by_campus.reduce((acc, campus) => {
        //Recherche sur nom d'item et description
        let filtered_items = campus.data.filter(item => {
          return SanitizeStringForSearch(item.room_name).indexOf(sanitized_search) != -1
              || SanitizeStringForSearch(item.description).indexOf(sanitized_search) != -1
        });
        if(filtered_items.length)
          acc.push({
            ...campus,
            data: filtered_items
          })
        return acc;
      },[]);

      this.setState({ data: filtered_data })
    }
  }

  getDateFormat = (date) => {
    return date.format('ddd D MMM YYYY');
  }
  getCreneauFormat = (date) => {
    return date.format('A');
  }

  /* MODAL CALENDRIER */
  showCalendarStart = () => {
    this.setState({
      modal:{
        calendar_start:true,
      }
    })
  }

  showCalendarEnd = () => {
    this.setState({
      modal:{
        calendar_end:true,
      }
    })
  }

  hideCalendar = () => {
    this.setState({
      modal:{
        calendar_start:false,
        calendar_end:false,
      }
    })
  }

  setDay = (day, type) => {
    __DEV__ && console.tron.log({day})
    let _filters = {...this.state.filters};
    //Affectation des heures dans le jour sélectionné
    _filters[type].set({
      year: day.year,
      month: day.month -1,
      date: day.day
    })

    //Attention à ne pas avoir un date de début postérieur à la date de fin
    if (_filters.end_time.isSameOrBefore(_filters.start_time)) {
      //si on est en train d'avancer l'heure de début, on avance l'heure de fin
      if (type == 'start_time')
        _filters.end_time = _filters.start_time.clone().add(1, 'day')
      //si on est en train de reculer l'heure de fin, on recule l'heure de début
      else
        _filters.start_time = _filters.end_time.clone().subtract(1, 'day')
    }
    this.setState({
      filters: {
        start_time: _filters.start_time.clone(),
        end_time: _filters.end_time.clone(),
      }
    })

    this.hideCalendar()
  }
  //Options pour le modal
  //Définitions des boutons
  CALENDAR_BUTTONS = [
    {
      text: translate('COMMON.annuler'),
      type : 'warning',
      onPress : this.hideCalendar
    }
  ]


  /******** MODAL Heures *********/
  showHoursStart = () => {
    this.setState({
      modal:{
        hours_start:true,
      }
    })
  }

  showHoursEnd = () => {
    this.setState({
      modal:{
        hours_end:true,
      }
    })
  }

  hideHours = () => {
    this.setState({
      modal:{
        hours_start:false,
        hours_end:false,
      }
    })
  }

  setAM = (time, type, hour = 9) => {
    this._updateHours('hours', hour, time, type)
  }

  setPM = (time, type, hour = 14) => {
    this._updateHours('hours', hour, time, type)
  }
  //Privates
  //Helper
  _updateHours = (operation = 'add', number = 9, time, type) => {
    time = moment.isMoment(time) ? time.clone() : moment(time);
    time[operation](number);
    //Copy des filtres actuels
    let _filters = {...this.state.filters};
    _filters[type] = time;

    this.setState({
      filters: {
        start_time: _filters.start_time.clone(),
        end_time: _filters.end_time.clone(),
      }
    })
  }

  //Options pour le modal
  //Définitions des boutons
  HOURS_BUTTONS = [
    {
      text: translate('COMMON.valider'),
      type : 'success',
      onPress : this.hideHours
    }
  ]

  //Handlers pour les items de la liste
  handlers = {
    onAvailable : (materiel) => {
      this.props.navigation.navigate('ReservationMaterielDetailScreen', {
        ...this.state,
        materiel,
        filter_dates : {
          start_time : this.state.filters.start_time.unix(),
          end_time : this.state.filters.end_time.unix(),
        }
      })
    },
    // onConflict : ()=>{alert('Probleme')},
    // onUnavailable : ()=>{alert('Impossible')},
  }

  renderItem = ({section, item}) => {
    return this.state.section_displayed == section.id ? (
      <ListItem
        backgroundColor={this.state.color}
        utilisateur={this.props.utilisateur}
        section={section}
        item={item}
        handlers={this.handlers}
        start_time={this.state.filters.start_time}
        end_time={this.state.filters.end_time}
        key={item.id}
      ></ListItem>
    ) : null //<View key={item.id}></View>
  }

  // Conditional branching for section headers, also see step 3
  renderSectionHeader = ({section}) => {
    return (
      <TouchableOpacity key={section.id} onPress={() => this.setState({ section_displayed: this.state.section_displayed == section.id ? null : section.id})} style={[styles.inline_no_wrap, styles.section_header]}>
        <View>
          <Text style={styles.sectionFixedHeaderWithSub}>{section.type_materiel}</Text>
          <Text style={styles.sectionFixedSubHeader}>{translate(`LABEL_MAP.CAMPUS.${section.campus_id}`, { defaultValue: section.campus_name })}</Text>
        </View>
        <Image source={Images.actions_icons.light.chevron} style={[styles.chevron_icon, this.state.section_displayed == section.id ? styles.chevron_icon_rotated : {}]}></Image>
      </TouchableOpacity>
    )
  }


  // Show this when data is empty
  renderEmpty = () =>
    <Text style={styles.list_empty}>{translate('COMMON.aucun_materiel')}</Text>

  renderSeparator = ({section}) => {
    return this.state.section_displayed == section.id 
      ? <View style={styles.row_separator}/>
      : null
  }


  // The default function if no Key is provided is index
  // an identifiable key is important if you plan on
  // item reordering.  Otherwise index is fine
  keyExtractor = (item, index) => {item.id}

  // How many items should be kept im memory as we scroll?
  oneScreensWorth = 6

  render () {
    return (
      <View style={styles.container}>

        <View style={styles.sub_nav}>
         { this.state.search == null
          ? <Text style={styles.sub_nav_text}>{translate('COMMON.nouvelle_reservation')}</Text>
          : <TextInput
              style={[styles.input, styles.padded, styles.textInput, styles.sub_nav_text]}
              placeholder="ex: vidéo, télévoteur"
              placeholderTextColor={Colors.darkText}
              enablesReturnKeyAutomatically={false}
              autoFocus={true}
              value={this.state.search}
              onChangeText={(value) => this.setState({ 'search': value })}
            />
          }
        </View>
        <View style={[styles.filter, styles.filter_with_margin]}>
          <View style={styles.filter_block}>
            <TouchableOpacity onPress={this.showCalendarStart}>
              <Text style={styles.filter_title}>{translate('SCREEN.MATERIEL.date_debut')}</Text>
              <Text style={styles.filter_text}>{this.getDateFormat(this.state.filters.start_time)}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.filter_block}>
            <TouchableOpacity onPress={this.showHoursStart}>
              <Text style={styles.filter_title}>{translate('SCREEN.MATERIEL.creneau')}</Text>
              <Text style={styles.filter_text}>{this.getCreneauFormat(this.state.filters.start_time)}</Text>
            </TouchableOpacity>
          </View>
          <Image source={Images.actions_icons.light.calendrier} style={styles.filter_icon}></Image>
        </View>
        <View style={[styles.filter]}>
          <View style={styles.filter_block}>
            <TouchableOpacity onPress={this.showCalendarEnd}>
              <Text style={styles.filter_title}>{translate('SCREEN.MATERIEL.date_fin')}</Text>
              <Text style={styles.filter_text}>{this.getDateFormat(this.state.filters.end_time)}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.filter_block}>
            <TouchableOpacity onPress={this.showHoursEnd}>
              <Text style={styles.filter_title}>{translate('SCREEN.MATERIEL.creneau')}</Text>
              <Text style={styles.filter_text}>{this.getCreneauFormat(this.state.filters.end_time)}</Text>
            </TouchableOpacity>
          </View>
          <Image source={Images.actions_icons.light.calendrier} style={styles.filter_icon}></Image>
        </View>
        {/* <View style={styles.filter}>
          <View style={styles.filter_block}>
            <Text style={styles.filter_title}>Options</Text>
            <Text style={styles.filter_text}>Campus de Nantes, moins de 10 places</Text>
          </View>
          <Image source={Images.actions_icons.light.filtres} style={styles.filter_icon}></Image>
        </View> */}
        <SectionList
          renderSectionHeader={this.renderSectionHeader}
          sections={this.state.data}
          contentContainerStyle={styles.listContent}
          renderItem={this.renderItem}
          // keyExtractor={this.keyExtractor}
          stickySectionHeadersEnabled={false}
          initialNumToRender={this.oneScreensWorth}
          // maxToRenderPerBatch={1}
          // updateCellsBatchingPeriod={400} // Increase time between renders
          // windowSize={6} // Reduce the window size

          ListEmptyComponent={this.renderEmpty}
          ItemSeparatorComponent={this.renderSeparator}
          extraData={[this.state.filters, this.state.search]}
          refreshing={false}
          onRefresh={this.props.materielReservationsRequest}
        />

        <Modal
          isVisible={this.state.modal.calendar_start}
          title={translate('MODAL.JOURS.selectionner_debut')}
          buttons={this.CALENDAR_BUTTONS}>
          <Calendar
            // Initially visible month. Default = Date()
            current={this.state.filters.start_time.toDate()}
            //Montrer le jour en cours
            markedDates={{[this.state.filters.start_time.format('YYYY-MM-DD')]: {selected: true}}}
            // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
            minDate={Date()}
            // Handler which gets executed on day press. Default = undefined
            onDayPress={(day)=>this.setDay(day, 'start_time')}
            // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
            monthFormat={'MMMM yyyy'}
            // Do not show days of other months in month page. Default = false
            hideExtraDays={true}
            // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
            firstDay={1}
            // Handler which gets executed when press arrow icon left. It receive a callback can go back month
            onPressArrowLeft={substractMonth => substractMonth()}
            // Handler which gets executed when press arrow icon left. It receive a callback can go next month
            onPressArrowRight={addMonth => addMonth()}
            theme={ApplicationStyles.calendar}
            />
        </Modal>

        <Modal
          isVisible={this.state.modal.calendar_end}
          title={translate('MODAL.JOURS.selectionner_fin')}
          buttons={this.CALENDAR_BUTTONS}>
          <Calendar
            // Initially visible month. Default = Date()
            current={this.state.filters.end_time.toDate()}
            //Montrer le jour en cours
            markedDates={{[this.state.filters.end_time.format('YYYY-MM-DD')]: {selected: true}}}
            // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
            minDate={Date()}
            // Handler which gets executed on day press. Default = undefined
            onDayPress={(day)=>this.setDay(day, 'end_time')}
            // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
            monthFormat={'MMMM yyyy'}
            // Do not show days of other months in month page. Default = false
            hideExtraDays={true}
            // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
            firstDay={1}
            // Handler which gets executed when press arrow icon left. It receive a callback can go back month
            onPressArrowLeft={substractMonth => substractMonth()}
            // Handler which gets executed when press arrow icon left. It receive a callback can go next month
            onPressArrowRight={addMonth => addMonth()}
            theme={ApplicationStyles.calendar}
            />
        </Modal>
        <Modal
          isVisible={this.state.modal.hours_start}
          title={translate('MODAL.CRENEAU.selectionner_debut')}
          buttons={this.HOURS_BUTTONS}>
          <View style={styles.modal_block}>
            <View style={styles.modal_inline}>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.setAM(this.state.filters.start_time, 'start_time')}>
                <Text style={Fonts.style.h1}>-</Text>
              </TouchableOpacity>
              <Text style={[styles.modal_inline_item, Fonts.style.h2]}>{this.getCreneauFormat(this.state.filters.start_time)}</Text>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.setPM(this.state.filters.start_time, 'start_time')}>
                <Text style={Fonts.style.h1}>+</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <Modal
          isVisible={this.state.modal.hours_end}
          title={translate('MODAL.CRENEAU.selectionner_fin')}
          buttons={this.HOURS_BUTTONS}>
          <View style={styles.modal_block}>
            <View style={styles.modal_inline}>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.setAM(this.state.filters.end_time, 'end_time', 12)}>
                <Text style={Fonts.style.h1}>-</Text>
              </TouchableOpacity>
              <Text style={[styles.modal_inline_item, Fonts.style.h2]}>{this.getCreneauFormat(this.state.filters.end_time)}</Text>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.setPM(this.state.filters.end_time, 'end_time', 18)}>
                <Text style={Fonts.style.h1}>+</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    sorted_by_campus : MaterielSelectors.getMaterielSortedByCampus(state),
    utilisateur : state.utilisateur,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    utilisateurShowLogin: ()=>{dispatch(UtilisateurActions.utilisateurShowLogin())},
    materielReservationsRequest: ()=>{dispatch(MaterielActions.materielReservationsRequest())},
    putMaterielReservationRequest: (data)=>{dispatch(MaterielActions.putMaterielReservationRequest(data))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReservationMaterielScreen)
