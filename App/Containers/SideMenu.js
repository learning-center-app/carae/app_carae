//@flow
import React from 'react'
import { View, Text, TouchableOpacity, Image} from 'react-native'
import { connect } from 'react-redux'
import Analytics from 'appcenter-analytics';
//Pour récupération numéro de version
import DeviceInfo from 'react-native-device-info'

import { Fonts, Images } from '../Themes'

import Modal from "react-native-modal"
import { getStatusBarHeight } from 'react-native-status-bar-height';

import SemanticButton from '../Components/SemanticButton'
import UtilisateurActions from '../Redux/UtilisateurRedux'

// More info here: https://facebook.github.io/react-native/docs/flatlist.html

//Internationalisation
import { translate } from '../I18n'

// Styles
import styles from './Styles/SideMenuStyle'

class SideMenu extends React.Component {
  constructor(props){
    super(props);
    // this.state = {
    //   isVisible: props.isVisible
    // }
  }
  // componentWillReceiveProps(nextProps){
  //   if (nextProps.isVisible !== this.props.isVisible) {
  //     __DEV__ && console.tron.log({changement_prop: nextProps.isVisible})
  //     this.setState({ isVisible: nextProps.isVisible })
  //   }
  // }
  getConnectedUserText = (utilisateur) => {
    // __DEV__ && console.tron.log(this.state);
    return (utilisateur.userId ? `${utilisateur.name}` : translate('SCREEN.SIDE_MENU.connectez_vous'))
  }

  isUserConnected = (utilisateur) => {
    // __DEV__ && console.tron.log(this.state);
    return (utilisateur.userId ? true : false)
  }

  hideIfLoggedIn = (utilisateur) => {
    return (this.isUserConnected(utilisateur) ? {opacity:0} : {opacity:1})
  }
  showIfLoggedIn = (utilisateur) => {
    return (this.isUserConnected(utilisateur) ? {opacity:1} : {opacity:0})
  }

  navigateToScreen = (item) => {

    this.hideModal(()=>{
      setTimeout(
        ()=>{
          //S'il y a besoin d'être connecté et qu'on ne l'est pas
          if (item.needs_login && !this.props.utilisateur.userId)
          this.props.utilisateurShowLogin();
          else
          this.props.navigation.navigate(item.screen, item);

        }, 500
      )
    })
  }
  renderRow = ({item, index}) =>{
    return (
      <TouchableOpacity style={[styles.row, styles.inline]} onPress={() => this.navigateToScreen(item)}>
        <View style={[styles.icon_wrapper, styles.rotate, {backgroundColor:item.color}]}>
          <Image source={Images.screen_icons[item.icon]} style={[styles.inverse_rotate, styles.icon]}></Image>
        </View>
        <Text style={[styles.item_title]}>{item.title}</Text>
      </TouchableOpacity>
    )
  }

  hideModal = (callback) => {
    typeof this.props.onCancel == "function" && (typeof callback == "function" ? this.props.onCancel(callback) : this.props.onCancel())
  }
  generateMenu = (menu_items) => {
    return menu_items.map((item) => {
      return (
        <TouchableOpacity style={[styles.menu_item]} onPress={() => this.navigateToScreen(item)}>
          <Text style={[styles.menu_item_title]}>{translate(`SCREEN_NAME_MAP.${item.screen}`)}</Text>
        </TouchableOpacity>
      )
    })
  }
  render () {
    return (
        <Modal
          isVisible={this.props.isVisible}
          onBackdropPress={this.hideModal}
          onSwipe={this.hideModal}
          animationIn='slideInLeft'
          // animationOut='slideInLeft'
          swipeDirection="left"
          style={{justifyContent:'flex-start'}}>
          <View style={[styles.header, styles.inline, styles.login_header, styles.modalMarginIphoneMonobrow, {justifyContent:'space-between'}]}>
            <TouchableOpacity onPress={()=> this.hideModal(()=>{ Analytics.trackEvent('[SideMenu] Modification du campus'); this.props.navigation.navigate("LoginCampusScreen")})}>
              <Text style={Fonts.style.h5}>{this.getConnectedUserText(this.props.utilisateur)}</Text>
            {!!this.props.utilisateur.userId && <Text style={styles.campus}>{this.props.utilisateur.campus && this.props.utilisateur.campus.id ? translate(`LABEL_MAP.CAMPUS.${this.props.utilisateur.campus.id}`) : 'Campus inconnu'}</Text>}
            </TouchableOpacity>
            {
              this.isUserConnected(this.props.utilisateur)
               ? <SemanticButton text={translate('SCREEN.SIDE_MENU.deconnexion')} onPress={()=>{this.props.utilisateurLogout()}} type='warning' size='small'/>
               : <SemanticButton text={translate('SCREEN.SIDE_MENU.connexion')} onPress={()=>{this.hideModal(()=>{Analytics.trackEvent('[SideMenu] Connexion via bouton'); this.props.navigation.navigate("LoginScreen")})}} type='success' size='small'/>
            }
          </View>
          <View style={[styles.header]}>
            { this.generateMenu(this.props.menu) }
          <TouchableOpacity style={[styles.menu_item]} onPress={() => this.hideModal(() => { Analytics.trackEvent('[SideMenu] Modification de la langue'); this.props.navigation.navigate("ModifierLangueScreen") })}>
              <Text style={[styles.menu_item_title]}>{translate('SCREEN.SIDE_MENU.changer_langue')}</Text>
            </TouchableOpacity>
              <Text style={[styles.menu_item_without_bg]}>Version {DeviceInfo.getVersion()}</Text>
          </View>
        </Modal>
    )
  }
}

SideMenu.defaultProps = {
  isVisible: false
}
const mapStateToProps = (state) => {
  return {
    // screens: state.screen.list,
    menu: state.screen.menu,
    utilisateur : state.utilisateur,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    utilisateurRequest: ()=>{dispatch(UtilisateurActions.utilisateurRequest())},
    utilisateurShowLogin: ()=>{dispatch(UtilisateurActions.utilisateurShowLogin())},
    utilisateurLogout: ()=>{dispatch(UtilisateurActions.utilisateurLogout())}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SideMenu)
