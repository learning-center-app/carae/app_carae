import React, { Component } from 'react'
import { Platform, ScrollView, View, Text, ActivityIndicator, TouchableOpacity, FlatList } from 'react-native'
import ViewPager from '@react-native-community/viewpager';
import { Pages } from 'react-native-pages';
import PageControl from 'react-native-page-control'
import Timeline from 'react-native-timeline-flatlist'

import { connect } from 'react-redux'

import moment from 'moment'
import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'

//API
import API from '../Services/Api'

// Styles
import styles from './Styles/HorairesScreenStyle'
import { Fonts, Colors } from '../Themes'

//Internationalisation
import { translate } from '../I18n'
import i18n from 'i18n-js'

class HorairesScreen extends Component {
  static navigationOptions = ({navigation}) => ({
    //Utilisation d'un composant custom dans headerTitle (et pas title, crash sur android)
    //https://github.com/facebook/react-native/issues/10232#issuecomment-398950751
    headerTitle: <NavbarTitleWithIcon icon={navigation.state.params.icon} title={translate(`SCREEN_NAME_MAP.${navigation.state.params.screen}`)} />,
  })

  constructor(props){
    super(props)

    //Définition de la langue pour MomentJS : http://momentjs.com/docs/#/i18n/loading-into-nodejs/
    //Utilisation de la locale actuelle pour moment
    moment.locale(i18n.locale)

    this.api = API.create();
    this.horaires = []
    this.flatListMoisRef = [] //références aux flatlist jours de chaque campus

    this.state = {
                  ...this.props.navigation.state.params,
                  is_loading: false,
                  selected: null,
                  waiting: false,
                  horaires: [],
                  current_page: 0,
                  current_jour: moment().format('YYYY-M-D'),
                  number_of_pages: 0,//correspond au nombre de campus
                  }

  }

  componentDidMount() {
    this.setState({ is_loading: true })
    this.getHoraires()
  }

  extraireJours = (horaires_datas) => {
    let jours = []
    let one_selected = false

    horaires_datas
    .sort((a, b) => moment(a.date, 'YYYY-M-D').diff(moment(b.date, 'YYYY-M-D')))
    .map((item, index) => {
      //Utilisation des clés internationalisées pour les jours relatifs
      let texte = moment(item.date, 'YYYY-M-D').calendar(null, translate('LABEL_MAP.MOMENT_CALENDAR'));

      var selected = moment(item.date, 'YYYY-M-D').startOf('date').diff(moment(this.state.current_jour, 'YYYY-M-D').startOf('date'), 'days') === 0
      if(selected)
        one_selected = true

      jours = [...jours, {date: item.date, texte: texte, selected: selected}]
    })

    //si aucun élément n'est sélectionné alors on prend le 1er
    if(jours.length > 0 && !one_selected)
      jours[0].selected = true

    return jours
  }

  estOuvert(horaires_datas){
    let ouvert = false
    horaires_datas.map((item, index) => {
      //si date d'aujourd'hui
      if(moment(item.date, 'YYYY-M-D').startOf('date').diff(moment().startOf('date'), 'days') === 0){
        if(!item.horaires.length)
          item.horaires = [item.horaires];//on transforme en tableau car il peut y avoir plusieurs horaires normalement
        item.horaires.map((hor) => {
          if(moment(hor.deb, 'hmm').diff(moment(), 'minutes') <= 0 && moment(hor.fin, 'hmm').diff(moment(), 'minutes') >= 0){
            ouvert = true
          }
        })
      }
    })
    return ouvert
  }


  getHoraires(){

    this.api.getHoraires().then(apiDatas => {//récupération des horaires
      if(apiDatas.ok === true){
        if(apiDatas.data.length > 0)
          this.horaires = apiDatas.data
      }

      this.setState({
        is_loading: false,
        horaires: this.horaires,
        number_of_pages: this.horaires.length
      })
    })
  }

  displayLoading() {
    if (this.state.is_loading) {
      return (
        <View style={styles.loading_container}>
          <ActivityIndicator size='large' />
        </View>
      )
    }
  }

  displayPageControl = () => {
    if (!this.state.is_loading) {
      return (
        <PageControl
          style={styles.pagecontrol_container}
          numberOfPages={this.state.number_of_pages}
          currentPage={this.state.current_page}
          hidesForSinglePage
          pageIndicatorTintColor='white'
          currentPageIndicatorTintColor='rgb(155,202,65)'
          indicatorStyle={{borderRadius: 0}}
          currentIndicatorStyle={{borderRadius: 0}}
          indicatorSize={{width:8, height:8}}
          onPageIndicatorPress={this.onItemTap}
        />
      )
    }
  }

  displayPagerHoraires = () => {
    if (!this.state.is_loading) {
      if(Platform.OS === 'ios'){
        return (
          <Pages
            style={[styles.content_container, {backgroundColor: this.state.color}]}
            indicatorColor='#000'
            indicatorPosition='none'
            startPage={0}
            onScrollEnd={(index) => {this.changePage(index)}}>
            { this.displayHoraires() }
          </Pages>
        )
      }else{
        return (
          <ViewPager
            style={[styles.content_container, {backgroundColor: this.state.color}]}
            initialPage={0}
            onPageSelected={(e) => {this.changePage(e.nativeEvent.position)}}>
            { this.displayHoraires() }
          </ViewPager>
        )
      }
    }
  }
  //Calcul de la taille des éléments pour optimisation de l'affichage, et scrollTo sur élément non visible.
  //Codé, mais non utilisé pour l'instant.
  getFlatListItemLayout = (data, index) => {
    let widths = [152, 124.5, 101.5],
        item_width = index < widths.length ? widths[index] : widths[widths.length-1],
        item_offset = (index > 0 ? widths.reduce(function(previous, current, _index){ return _index < index ? previous + current : previous; }) : 0)
                      + (index >= widths.length ? widths[widths.length-1] * (index - widths.length-1) : 0);

    // __DEV__ && console.tron.log({length: item_width, offset: item_offset, index})
    return {length: item_width, offset: item_offset, index};
  }

  displayHoraires = () => {
    if(this.state.horaires)
    {
      return this.state.horaires
      .sort((a, b) => a.campus_name.localeCompare(b.campus_name))
      .reduce((acc, campus) => {
        //Placement du campus concernant l'utilsateur connecté en premier
        //https://stackoverflow.com/a/37551373/1437016
        // __DEV__ && console.tron.log(campus, this.props.campus_utilisateur)
        if (this.props.campus_utilisateur && campus.campus_id == this.props.campus_utilisateur.id)
          return [campus, ...acc];
        return [...acc, campus];
      }, [])
      .map((tab_campus_horaires, index_campus) => {
        let est_ouvert = this.estOuvert(tab_campus_horaires.datas)
        let jours = this.extraireJours(tab_campus_horaires.datas)
        let index_jour_select = jours.findIndex(x => x.selected);
        return (
          <View key={index_campus+1}>
            <View style={styles.titre_container}>
              <Text style={[Fonts.style.h1, styles.titre_texte]}>{translate('COMMON.carae')} {translate(`LABEL_MAP.VILLE.${tab_campus_horaires.campus_id}`).toUpperCase()}</Text>
              {!!tab_campus_horaires.subname && (
                <Text style={[styles.titre_texte, Fonts.style.h5]}>{tab_campus_horaires.subname[i18n.locale]}</Text>
              )}
              <Text style={[styles.status, styles.white_block, (est_ouvert ? styles.ouvert_texte : styles.ferme_texte)]}>{est_ouvert ? translate('SCREEN.HORAIRES.ouvert') : translate('SCREEN.HORAIRES.ferme')}</Text>
            </View>
            <View style={styles.jours_container}>
              <View style={styles.prec_suiv_container}>
                <TouchableOpacity style={styles.precsuiv_bouton}
                  onPress={() => this.changeJour(index_campus, index_jour_select - 1, jours[index_jour_select - 1] ? jours[index_jour_select - 1].date : null)} >
                  <Text style={{color: 'white', fontSize: 18}}>{'<'}</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.btns_jours_container}>
                <FlatList
                  ref={(ref) => { this.flatListMoisRef[index_campus] = ref; }}
                  // initialScrollIndex={index_jour_select}
                  // getItemLayout={this.getFlatListItemLayout}
                  initialNumToRender={7}
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  data={jours}
                  renderItem={({item, index}) => {
                    return (
                      <TouchableOpacity style={item.selected ? styles.jour_bouton_select : styles.jour_bouton}
                        onPress={() => this.changeJour(index_campus, index, item.date)} >
                        <Text style={[Fonts.style.normal, {color: Colors.darkText}]}>{item.texte}</Text>
                      </TouchableOpacity>
                    )
                  }}
                  keyExtractor={(item, index) => item.date}
                />
              </View>
              <View style={styles.prec_suiv_container}>
                <TouchableOpacity style={styles.precsuiv_bouton}
                  onPress={() => this.changeJour(index_campus, index_jour_select + 1, jours[index_jour_select + 1] ? jours[index_jour_select + 1].date : null)} >
                  <Text style={{color: 'white', fontSize: 18}}>{'>'}</Text>
                </TouchableOpacity>
              </View>
            </View>
            <ScrollView>
                {this.displayTimeline(tab_campus_horaires.datas, index_jour_select)}
                <View style={[styles.alert, styles.alert_dark]}>
                  <View style={[styles.inline_no_wrap, {alignItems:'flex-start'}]}>
                    <View style={[styles.timeline_bar, {borderColor:'rgb(255, 255, 255)'}]}></View><Text style={[styles.alert_text]}>: {translate("SCREEN.HORAIRES.description_acces_libre")}</Text>
                  </View>
                  <View style={[styles.inline_no_wrap, {alignItems:'flex-start'}, styles.with_margin_top]}>
                    <View style={[styles.timeline_bar, {borderColor:'rgb(126, 255, 72)'}]}></View><Text style={[styles.alert_text]}>: {translate("SCREEN.HORAIRES.description_accueil")}</Text>
                  </View>
                  <View style={[styles.inline_no_wrap, {alignItems:'flex-start'}, styles.with_margin_top]}>
                    <View style={[styles.timeline_bar, {borderColor:'rgb(0, 255, 255)'}]}></View><Text style={[styles.alert_text]}>: {translate("SCREEN.HORAIRES.description_permanence")}</Text>
                  </View>
                </View>
            </ScrollView>
          </View>
        )
      })
    }
  }

  displayTimeline = (datas, index_selected) => {
    if(datas.length > 0){
      let index = index_selected < 0 ? 0 : index_selected
      let horaires = datas[index].horaires;//on prend pour l'instant les horaires du 1er jour
      let horaires_inter = datas[index].horaires_inter;
      let timelines = []

      //transformation en tableau si ce n'est pas le cas
      if(!horaires.length)
        horaires = [horaires];
      if(!horaires_inter.length)
        horaires_inter = [horaires_inter];
      return(
        horaires
        .sort((a, b) => a.deb - b.deb)
        .map((horaire)=>{
          let horaires_timeline = []//horaires mis en forme pour le composant timeline
          let horaire_timeline_deb = {time: moment(horaire.deb, 'hmm').format('H[h]mm').replace(/^00+|00+$/g, ''), title: '', description: '', color: ''}
          let horaire_timeline_fin = {time: moment(horaire.fin, 'hmm').format('H[h]mm').replace(/^00+|00+$/g, ''), title: '', description: '', color: ''}
          //on intercale les horaires intermédiaires s'il y en a
          let deb = horaire.deb*1
          let fin = horaire.fin*1
          horaires_inter
          .sort((a, b) => a.deb - b.deb)
          .map((horaire_inter)=>{
            if(horaire_inter.deb*1 >= deb && horaire_inter.fin*1 <= horaire.fin*1)
            {
              //1.le début de l'horaire intermédiaire coïncide avec l'horaire principal, on change seulement le titre de l'hor princ
              if(horaire_inter.deb === deb){
                horaire_timeline_deb.title = horaire_inter.nom
                horaire_timeline_deb.description = `${translate("COMMON.de")} ${moment(horaire_inter.deb, 'hmm').format('H[h]mm').replace(/^00+|00+$/g, '')} ${translate("COMMON.a")} ${moment(horaire_inter.fin, 'hmm').format('H[h]mm').replace(/^00+|00+$/g, '')} ${(horaire_inter.description != "" ? '\n' + horaire_inter.description : '')}`;
                horaire_timeline_deb.color = horaire_inter.couleur
              }else{
                horaires_timeline = [...horaires_timeline, horaire_timeline_deb]
                horaire_timeline_deb = {
                  time: '', 
                  title: horaire_inter.nom, 
                  description: `${translate("COMMON.de")} ${moment(horaire_inter.deb, 'hmm').format('H[h]mm').replace(/^00+|00+$/g, '')} ${translate("COMMON.a")} ${moment(horaire_inter.fin, 'hmm').format('H[h]mm').replace(/^00+|00+$/g, '')}\n${horaire_inter.description}`, 
                  color: horaire_inter.couleur
                }
                deb = horaire_inter.deb*1
              }
              fin = horaire_inter.fin*1
            }
          })


          horaires_timeline = [...horaires_timeline, horaire_timeline_deb]
          if(fin < horaire.fin*1)
              horaires_timeline = [...horaires_timeline, {time: '', title: '', description: '', color: ''}]
          horaires_timeline = [...horaires_timeline, horaire_timeline_fin]

          return(
            <View style={styles.timeline_container}>
              <Timeline
                circleSize={10}
                circleColor='white'
                lineWidth={4}
                lineColor='white'

                data={horaires_timeline}
                renderDetail={this.renderDetail}
                timeContainerStyle={styles.time_timeline}
                timeStyle={[Fonts.style.normal, styles.time_text_timeline]}
                titleStyle={styles.title_timeline}
                detailContainerStyle={styles.details_timeline}

              />
            </View>
          )

        })
      )

    }
  }

  renderDetail(rowData, sectionID, rowID) {
    if(rowData.title && rowData.description)
    {
      return (
        <View style={styles.details_timeline2}>
          <View style={[styles.details_barre_timeline, {backgroundColor: rowData.color}]} />
          <View>
            <Text style={[Fonts.style.h4, styles.title_timeline]}>{rowData.title}</Text>
            <Text style={[Fonts.style.normal, styles.description_timeline]}>{rowData.description}</Text>
          </View>
        </View>
      )
    }else{
      if(!rowData.time){
        return (
          <View style={styles.details_timeline2} />
        )
      }
    }
}

  onScrollHoraires = (event) => {
    let current_page = Math.round(event.nativeEvent.contentOffset.x / event.nativeEvent.layoutMeasurement.width);
    if (this.state.current_page !== current_page) {
      this.setState({current_page});
    }
  }
  changePage = (current_page) => {
    if (this.state.current_page !== current_page)
      this.setState({current_page});
  }
  changeJour = (index_campus, index_jour, current_jour) => {
    if(current_jour !== null && this.state.current_jour !== current_jour){
      let index = index_jour - 1 >= 0 ? index_jour - 1 : 0;
      //Mise à jour du scroll de TOUS les flatlist, pas que celui en cours
        for (let flatlist of this.flatListMoisRef) {
          try{
            flatlist.scrollToIndex({animated: true, index: index});
          }
          catch(e) {
            __DEV__ && console.tron.log({reason: 'Out of bounds du scrollToIndex pour les autres FlatList', e})
          }
        }
        this.setState({current_jour});
    }
  }

  render () {
    return (
      <View style={styles.main_container}>
          {this.displayLoading()}
          {this.displayPageControl()}
          {this.displayPagerHoraires()}
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    campus_utilisateur: state.utilisateur.campus
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HorairesScreen)
