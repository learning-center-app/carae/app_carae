import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Colors, Fonts } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  menu: {
    flex: 1,
    backgroundColor: Colors.black
  },
  row: {
    flex: 1,
    backgroundColor: Colors.transparent,
    height: 60,
    justifyContent: 'flex-start',
  },
  inline: {
    alignItems:'center',
    flexDirection:'row',
    flexWrap:'wrap'
  },
  boldLabel: {
    fontWeight: 'bold',
    alignSelf: 'center',
    color: Colors.snow,
    textAlign: 'center',
    marginBottom: Metrics.smallMargin
  },
  icon_wrapper : {
    padding:5,
    width:35,
    height:35,
    marginLeft:20,
    marginRight:20,
  },
  icon: {
    resizeMode: 'contain',
    width:25,
    height:25,
  },
  label: {
    textAlign: 'center',
    color: Colors.snow
  },
  listContent: {
    marginTop: Metrics.baseMargin
  },
  item_title : {
    fontSize: Fonts.size.h6,
    fontFamily: Fonts.type.base,
    color: Colors.text
  },
  rotate : {
    transform: [
      {rotate: '45deg'}
    ]
  },
  inverse_rotate : {
    transform: [
      {rotate: '-45deg'}
    ]
  },
  login_header : {
    borderBottomWidth: 0.5,
    borderColor: 'rgba(255,255,255,0.4)',
    paddingVertical: Metrics.baseMargin
  },
  campus : {
    ...Fonts.style.normal,
    marginTop: Metrics.smallMargin,
  },
  menu_item : {
    backgroundColor:'white',
    shadowColor: '#222',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    marginVertical:Metrics.smallMargin,
    paddingVertical: Metrics.baseMargin,
    paddingHorizontal: Metrics.baseMargin,
  },
  menu_item_title: {
    ...Fonts.style.normal,
    color: Colors.darkText
  },
  menu_item_without_bg: {
    ...Fonts.style.h6,
    color: Colors.white
  },
})
