import { StyleSheet } from 'react-native'
import { ApplicationStyles, Metrics, Colors, Fonts } from '../../Themes'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  ...ApplicationStyles.components,
  container: {
    flex: 1,
    backgroundColor: Colors.transparent
  },
  row: {
    flex: 1,
    marginVertical: Metrics.smallMargin,
    justifyContent: 'center'
  },
  boldLabel: {
    fontWeight: 'bold',
    alignSelf: 'center',
    color: Colors.snow,
    textAlign: 'center',
    marginBottom: Metrics.smallMargin
  },
  label: {
    textAlign: 'center',
    color: Colors.snow
  },
  listContent: {
    marginTop: Metrics.baseMargin
  },

  recherche_icon: {
    marginRight: 10,
    resizeMode: 'contain',
    width: 24,
    height: 24,
    paddingHorizontal: Metrics.baseMargin,
  },
  section_header : {
    justifyContent: 'space-between',
    width: "100%",
  },

  chevron_icon: {
    marginHorizontal: Metrics.doubleBaseMargin,
    resizeMode: 'contain',
    width: 24,
    height: 24,
    paddingHorizontal: Metrics.baseMargin,
  },
  chevron_icon_rotated : {
    transform: [
      { rotate: (Platform.OS === 'ios') ? '90deg' : (3.14159 / 2) + 'rad' }
    ]
  }
})
