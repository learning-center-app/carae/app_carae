import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  main_container: {
    flex: 1,
    backgroundColor: Colors.transparent
  },
  content_container: {
    flex: 1,
  },
  loading_container: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.transparent
  },

  pagecontrol_container: {
    marginTop: 5,
    marginBottom: 10,
  },
  descriptionWebbox: {
    //marginTop:Metrics.marginVertical,
    width:Metrics.screenWidth,
    backgroundColor: Colors.transparent,
  },

})
