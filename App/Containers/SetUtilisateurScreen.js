//@flow
import React, { Component } from 'react'
import { Platform, ScrollView, StatusBar, Text, TextInput, View } from 'react-native'
import { connect } from 'react-redux'
import BetterKeyboardAvoidingView from '../Components/BetterKeyboardAvoidingView'
import SemanticButton from '../Components/SemanticButton'
//Internationalisation
import { translate } from '../I18n'
import UtilisateurActions from '../Redux/UtilisateurRedux'
import { Colors } from '../Themes'
// Styles
import styles from './Styles/SetUtilisateurScreenStyle'
import utilisateurDefault from '../Fixtures/utilisateur'

const columns = [
  "userId", //colin
  "email", //christian.colin@imt-atlantique.fr
  "eppn", //colin@emn.fr
  "name", //Christian COLIN
  "nom", //COLIN
  "prenom", //Christian
  "organization", //imt atlantique
  "id_koha", //1159376134816640
  "campus",
  "formation", //null
  "service", //null
  "version_cgu_accepte", //2019-11-06T10:45:33+0000
  // "data", //null
  // "fetchingUser", //null
  // "payload", //null
  // "error", //null
  "tutoriel_est_vu", //true
  "langue", //null
  // "show_login", //false
  // "show_internet", //false
  // "fetching", //false
]
class SetUtilisateurScreen extends Component {
  constructor(props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {
      ...props.navigation.state.params,
      ...this.state,
      utilisateur: props.utilisateur,
    };

    Platform.OS === "ios" && StatusBar.setBarStyle('dark-content', true);
  }
  componentWillUnmount() {
    Platform.OS === "ios" && StatusBar.setBarStyle('light-content', true);
  }


  /** Traduit les options récupérées du redux via i18n */
  renderLocalizedCampusOptions = () => {
    let categories = translate('LABEL_MAP.VILLE');
    return Object.keys(categories)
      .map((key) => ({ value: key, label: categories[key] }))
  }
  setUtilisateurRedux = (utilisateur) => {
    this.props.setUtilisateur(utilisateur); 
    this.props.navigation.goBack()
  }
  castToString = (val) => {
    switch (typeof val) {
      case "string":
        return val
      case "object":
        return JSON.stringify(val)
      default :
        return val +""
    }
  }
  render() {
    return (
      <View style={[styles.container, { backgroundColor: this.state.color }]}>
        <View style={[styles.sub_nav, styles.modalPaddingIphone]}><Text style={styles.sub_nav_text}>Dev menu</Text></View>
        <ScrollView>
          <BetterKeyboardAvoidingView behavior="padding" offset={140}>
            {columns.map(key => <View style={styles.input_with_margin_top}>
              <Text style={[styles.label, styles.padded]}>{key}</Text>
              <TextInput
                maxLength={150}
                autoCorrect={false}
                defaultValue={this.castToString(this.props.utilisateur[key])}
                onChangeText={(value) => this.setState({ utilisateur: { ...this.state.utilisateur, [key]:value } })}
                style={[styles.input, styles.input_with_margin_top, styles.padded, styles.textInput]}
              />
            </View>)}
            <View style={[styles.actions]}>
              <SemanticButton
                text={translate('COMMON.annuler')}
                type="warning"
                onPress={() => this.props.navigation.goBack()}
              />
              <SemanticButton
                text={translate('COMMON.valider')}
                type="success"
                onPress={() => this.setUtilisateurRedux(this.state.utilisateur)}
              />
            </View>
            <View style={[styles.actions]}>
              <SemanticButton
                text="Update GRR"
                type="info"
                // size="small"
                onPress={() => this.props.upsertUtilisateurGrr(this.state.utilisateur)}
              />
              <SemanticButton
                text="Set default"
                type="info"
                // size="small"
                onPress={() => { this.setState({ utilisateur: utilisateurDefault }); this.setUtilisateurRedux(utilisateurDefault) }}
              />
            </View>
          </BetterKeyboardAvoidingView>
        </ScrollView>

      </View>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    utilisateur: state.utilisateur,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setUtilisateur: (utilisateur) => { dispatch(UtilisateurActions.utilisateurSuccess(utilisateur)) },
    upsertUtilisateurGrr: (utilisateur) => { dispatch(UtilisateurActions.upsertUtilisateurGrr(utilisateur)) },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SetUtilisateurScreen)
