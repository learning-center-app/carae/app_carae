//@flow
import React, { Component } from 'react'
import { ScrollView, Text, TextInput, View, TouchableOpacity, Image } from 'react-native'
import { connect } from 'react-redux'

import BetterKeyboardAvoidingView from '../Components/BetterKeyboardAvoidingView';
import SemanticButton from '../Components/SemanticButton'

import DebugConfig from '../Config/DebugConfig'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import { Images, Fonts, Colors, ApplicationStyles } from '../Themes'
import styles from './Styles/DemandeReferenceScreenStyle'

import DemandeReferenceActions from '../Redux/DemandeReferenceRedux'
import UtilisateurActions from '../Redux/UtilisateurRedux'

// import colors from '../Themes/Colors'
import Modal from '../Components/Modal'
import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'
//Internationalisation
import { translate } from '../I18n'

class DemandeReferenceScreen extends Component {
  static navigationOptions = ({navigation}) => ({
    //Utilisation d'un composant custom dans headerTitle (et pas title, crash sur android)
    //https://github.com/facebook/react-native/issues/10232#issuecomment-398950751
    headerTitle: <NavbarTitleWithIcon icon={navigation.state.params.icon} title={translate(`SCREEN_NAME_MAP.${navigation.state.params.screen}`)} />,
  })


  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {
      ...props.navigation.state.params,
      ...this.state
    };
  }

  state = {
    data : {},
    show_form: null,
    modalsuccess:false,
    modalfail:false,
    button_label:translate('COMMON.envoyer'),
  }
  FAIL_BUTTONS = [
    {
      text : translate('COMMON.reessayer'),
      type : 'warning',
      onPress : () => {
        this.setState((partialState)=> ({
          ...partialState,
          modalfail : false
        }))
      }
    }
  ]
  SUCCESS_BUTTONS = [
    {
      text : translate('COMMON.ok'),
      type : 'success',
      onPress : () => {
        this.setState((partialState)=> ({
          ...partialState,
          modalsuccess : false
        }))
      }
    }
  ]


  postDemandeReference() {
    if ((this.state.show_form == "article" && (
          !this.state.data.titre_revue ||
          !this.state.data.titre_article ||
          !this.state.data.auteurs
        )) || (this.state.show_form == "livre" && (
          !this.state.data.titre_livre ||
          !this.state.data.auteurs ||
          !this.state.data.isbn 
        ))) {
      return this.setState({ modalfail: true, error: translate('ERROR.champs_requis') });
    }
    //Si on est déjà en train d'envoyer, on ignore le touch pour éviter le double envoi
    if (this.props.sending) {
      return;
    }
    if (!this.props.utilisateur.userId) {
      return this.props.utilisateurShowLogin();
    }
    try {
        let data = {
          data: {
            ...this.state.data
          },
          campus: {
            nom: translate(`LABEL_MAP.VILLE.${this.props.utilisateur.campus.id}`),
            ville: translate(`LABEL_MAP.CAMPUS.${this.props.utilisateur.campus.id}`)
          },
          utilisateur: this.props.utilisateur
        }

        this.props.postDemandeReference(data);
      }
      catch(e) {
        __DEV__ && console.tron.log(e);
        this.setState({ modalfail: true, error: translate('ERROR.champs_requis')});
      }
  }

  //Hook sur le changement de props
  //https://stackoverflow.com/a/44750890/1437016
  componentWillReceiveProps(props) {
    if (props.sending) {
      this.setState({ button_label: translate('COMMON.en_cours')})
    }
    else if (!props.sending) {
      if (!this.state.modalfail && props.error) {
        this.setState({ modalfail: true, button_label: translate('COMMON.reessayer')})
      }
      else if (!this.state.modalsuccess) {
        this.setState({ modalsuccess: true, button_label: translate('COMMON.envoye')})
      }
    }

  }
  getSubLabel = () => {
    switch (this.state.show_form){
      case 'livre':
        return translate("SCREEN.DEMANDE_REFERENCE.subtitle_livre")
      case 'article':
        return translate("SCREEN.DEMANDE_REFERENCE.subtitle_article")
      default: 
        return translate("SCREEN.DEMANDE_REFERENCE.subtitle")
    }
  }
  render () {
    return (
    <View style={[styles.container, {backgroundColor: this.state.color}]}>
      <View style={styles.sub_nav}>
        <Text style={styles.sub_nav_text}>{this.getSubLabel()}</Text>
      </View>
      <ScrollView>
        {!this.state.show_form ? (
          <View style={[styles.padded]}>
            <View style={[styles.alert, styles.margedVertical]}>
              <Text style={[styles.label, styles.align_center]}>
                {translate('SCREEN.DEMANDE_REFERENCE.explication_1')}
              </Text>
              <Text style={[styles.label, styles.marginTop]}>
                {translate('SCREEN.DEMANDE_REFERENCE.explication_2')}
              </Text>
              <Text style={[styles.label, styles.marginTop]}>
                {translate('SCREEN.DEMANDE_REFERENCE.explication_3')}
              </Text>
            </View>
              <Text style={[styles.boldLabel, styles.marginTop3]}>{translate('SCREEN.DEMANDE_REFERENCE.label_choix')}</Text>
            <View style={[styles.padded, styles.inline_no_wrap, { alignSelf: 'center'}]}>
              <TouchableOpacity style={[styles.button_block]} onPress={() => this.setState({ show_form: 'livre' })}>
                <Image source={Images.screen_icons.emprunt_livre} style={[styles.icon]} />
                <Text style={[styles.boldLabel]}>{translate('SCREEN.DEMANDE_REFERENCE.livre')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={[styles.button_block]} onPress={() => this.setState({ show_form: 'article' })}>
                <Image source={Images.screen_icons.article} style={[styles.icon]} />
                <Text style={[styles.boldLabel]}>{translate('SCREEN.DEMANDE_REFERENCE.article')}</Text>
              </TouchableOpacity>
            </View>
          </View>
        ) : (
          <View>
            {this.state.show_form == 'livre' && (
              <BetterKeyboardAvoidingView behavior="padding" offset={140}>
                <View style={styles.input_with_margin_top}>
                  <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.livre_titre')}</Text>
                  <TextInput
                    multiline={true}
                    maxLength={2000}
                    autoCorrect={false}
                    onChangeText={(titre_livre) => this.setState({ data: {...this.state.data, titre_livre} })}
                    placeholder={translate('FORM.PLACEHOLDER.titre_livre')}
                    placeholderTextColor="#ffffff"
                    style={[styles.multilineText, styles.input, styles.input_with_margin_top]}
                  />
                </View>
                <View style={styles.input_with_margin_top}>
                  <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.livre_auteurs')}</Text>
                  <TextInput
                    maxLength={150}
                    autoCorrect={false}
                    onChangeText={(auteurs) => this.setState({ data: {...this.state.data, auteurs} })}
                    //placeholder={translate('FORM.PLACEHOLDER.facultatif')}
                    //placeholderTextColor="#aaa"
                    style={[styles.input, styles.input_with_margin_top, styles.padded, styles.textInput]}
                  />
                </View>
                <View style={styles.input_with_margin_top}>
                  <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.livre_annee')}</Text>
                  <TextInput
                    maxLength={150}
                    autoCorrect={false}
                    onChangeText={(annee) => this.setState({ data: {...this.state.data, annee} })}
                    //placeholder={translate('FORM.PLACEHOLDER.facultatif')}
                    //placeholderTextColor="#aaa"
                    style={[styles.input, styles.input_with_margin_top, styles.padded, styles.textInput]}
                  />
                </View>
                <View style={styles.input_with_margin_top}>
                  <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.livre_editeur')}</Text>
                  <TextInput
                    maxLength={150}
                    autoCorrect={false}
                    onChangeText={(editeur) => this.setState({ data: {...this.state.data, editeur} })}
                    //placeholder={translate('FORM.PLACEHOLDER.facultatif')}
                    //placeholderTextColor="#aaa"
                    style={[styles.input, styles.input_with_margin_top, styles.padded, styles.textInput]}
                  />
                </View>
                <View style={styles.input_with_margin_top}>
                  <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.livre_isbn')}</Text>
                  <TextInput
                    maxLength={200}
                    autoCorrect={false}
                    onChangeText={(isbn) => this.setState({ data: {...this.state.data, isbn} })}
                    //placeholder={translate('FORM.PLACEHOLDER.facultatif')}
                    //placeholderTextColor="#aaa"
                    style={[styles.input, styles.input_with_margin_top, styles.padded, styles.textInput]}
                  />
                </View>
                <View style={styles.actions}>
                  <SemanticButton
                    text={this.state.button_label}
                    onPress={() => { this.postDemandeReference() }}
                    disabled={this.props.sending}
                    type="success"
                  />
                </View>
              </BetterKeyboardAvoidingView>
            )}
            {this.state.show_form == 'article' && (
              <BetterKeyboardAvoidingView behavior="padding" offset={160}>
                <View style={styles.input_with_margin_top}>
                  <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.article_titre_revue')}</Text>
                  <TextInput
                    multiline={true}
                    maxLength={2000}
                    autoCorrect={false}
                    onChangeText={(titre_revue) => this.setState({ data: { ...this.state.data, titre_revue } })}
                    placeholder={translate("FORM.PLACEHOLDER.titre_article")}
                    placeholderTextColor="#ffffff"
                    style={[styles.multilineText, styles.input, styles.input_with_margin_top]}
                  />
                </View>
                <View style={styles.input_with_margin_top}>
                  <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.article_volume')}</Text>
                  <TextInput
                    maxLength={150}
                    autoCorrect={false}
                    onChangeText={(volume) => this.setState({ data: { ...this.state.data, volume } })}
                    //placeholder={translate('FORM.PLACEHOLDER.facultatif')}
                    //placeholderTextColor="#aaa"
                    style={[styles.input, styles.input_with_margin_top, styles.padded, styles.textInput]}
                  />
                </View>
                <View style={styles.input_with_margin_top}>
                  <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.article_numero')}</Text>
                  <TextInput
                    maxLength={150}
                    autoCorrect={false}
                    onChangeText={(numero) => this.setState({ data: { ...this.state.data, numero } })}
                    //placeholder={translate('FORM.PLACEHOLDER.facultatif')}
                    //placeholderTextColor="#aaa"
                    style={[styles.input, styles.input_with_margin_top, styles.padded, styles.textInput]}
                  />
                </View>
                <View style={styles.input_with_margin_top}>
                  <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.article_titre_article')}</Text>
                  <TextInput
                    maxLength={150}
                    autoCorrect={false}
                    onChangeText={(titre_article) => this.setState({ data: { ...this.state.data, titre_article } })}
                    //placeholder={translate('FORM.PLACEHOLDER.facultatif')}
                    //placeholderTextColor="#aaa"
                    style={[styles.input, styles.input_with_margin_top, styles.padded, styles.textInput]}
                  />
                </View>
                <View style={styles.input_with_margin_top}>
                  <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.article_auteurs')}</Text>
                  <TextInput
                    maxLength={200}
                    autoCorrect={false}
                    onChangeText={(auteurs) => this.setState({ data: { ...this.state.data, auteurs } })}
                    //placeholder={translate('FORM.PLACEHOLDER.facultatif')}
                    //placeholderTextColor="#aaa"
                    style={[styles.input, styles.input_with_margin_top, styles.padded, styles.textInput]}
                  />
                </View>
                <View style={styles.input_with_margin_top}>
                  <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.article_pages')}</Text>
                  <TextInput
                    maxLength={200}
                    autoCorrect={false}
                    onChangeText={(pages) => this.setState({ data: { ...this.state.data, pages } })}
                    //placeholder={translate('FORM.PLACEHOLDER.facultatif')}
                    //placeholderTextColor="#aaa"
                    style={[styles.input, styles.input_with_margin_top, styles.padded, styles.textInput]}
                  />
                </View>
                <View style={styles.input_with_margin_top}>
                  <Text style={[styles.label, styles.padded]}>{translate('FORM.LABEL.article_date')}</Text>
                  <TextInput
                    maxLength={200}
                    autoCorrect={false}
                    onChangeText={(date_publication) => this.setState({ data: { ...this.state.data, date_publication } })}
                    //placeholder={translate('FORM.PLACEHOLDER.facultatif')}
                    //placeholderTextColor="#aaa"
                    style={[styles.input, styles.input_with_margin_top, styles.padded, styles.textInput]}
                  />
                </View>
                <View style={styles.actions}>
                  <SemanticButton
                    text={this.state.button_label}
                    onPress={() => { this.postDemandeReference() }}
                    disabled={this.props.sending}
                    type="success"
                  />
                </View>
              </BetterKeyboardAvoidingView>
            )}
            
          </View>
        )}
        
      </ScrollView>

      <Modal
        isVisible={this.state.modalsuccess}
        title=""
        onCancel={()=> {this.setState({modalsuccess : false})}}
        buttons={this.SUCCESS_BUTTONS}>

        <View>
          <Text style={[
            Fonts.style.h1,
            styles.centerText,
            ]}
          >
            {translate('MODAL.SUGGESTION.SUCCESS.title')}
          </Text>
          <Text style={[
            Fonts.style.h4,
            styles.centerText
            ]}
          >
            {translate('MODAL.SUGGESTION.SUCCESS.body')}
          </Text>
        </View>
      </Modal>
      {/* / MODAL ERREUR */}
      <Modal
        isVisible={this.state.modalfail}
        title=""
        onCancel={()=> {this.setState({modalfail : false})}}
        buttons={this.FAIL_BUTTONS}>

        <View>
          <Text style={[
            Fonts.style.h1,
            styles.centerText
            ]}
          >
            {translate('MODAL.SUGGESTION.ERROR.title')}
          </Text>

          <Text style={[
              Fonts.style.h3,
              styles.centerText,
              {paddingTop:30, paddingBottom:30}
              ]}>
              {this.props.error || this.state.error}
          </Text>
        </View>
      </Modal>
    </View>
    )
  }
}

const mapStateToProps = (state) => {
  
  return {
    sending : state.demande_reference.sending,
    error : state.demande_reference.error,
    utilisateur : state.utilisateur,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    utilisateurShowLogin: ()=>{dispatch(UtilisateurActions.utilisateurShowLogin())},
    postDemandeReference : data => {dispatch(DemandeReferenceActions.demandeReferenceRequest(data))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DemandeReferenceScreen)
