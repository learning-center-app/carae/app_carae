//@flow
import React from 'react'
import { View, SectionList, Text, Image } from 'react-native'
import { connect } from 'react-redux'

import moment from 'moment'
import 'moment-round'

import ListItem from '../Components/ListItem'

import Modal from '../Components/Modal'
import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'

//Actions redux
import SallesActions, { SallesSelectors } from '../Redux/SallesRedux'
import UtilisateurActions from '../Redux/UtilisateurRedux'

//Internationalisation
import { translate } from '../I18n'
import i18n from 'i18n-js'


// More info here: https://facebook.github.io/react-native/docs/sectionlist.html

// Styles
import styles from './Styles/DispoSalleScreenStyle'
import { Fonts, ApplicationStyles } from '../Themes'
import PushService from '../Services/PushService'
import { CHANNEL_IDS } from '../Services/PushConfiguration'
import { withNavigationFocus } from "react-navigation"

class DispoSalleScreen extends React.PureComponent {
  static navigationOptions = ({navigation}) => ({
    //Utilisation d'un composant custom dans headerTitle (et pas title, crash sur android)
    //https://github.com/facebook/react-native/issues/10232#issuecomment-398950751
    headerTitle: <NavbarTitleWithIcon icon={navigation.state.params.icon} title={translate(`SCREEN_NAME_MAP.${navigation.state.params.screen}`)}/>,
  })

  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {
      lastReservation : null,
      updated : false,
      ...props.navigation.state.params
    };
    moment.locale(i18n.locale)
  }
  state = {
    lastReservation : null,
    updated : false,
  }
  componentDidMount () {
    //Chopage des résas de salles au cas où
    // this.props.sallesReservationsRequest();
  }

  //Hook sur le changement de props
  //https://stackoverflow.com/a/44750890/1437016
  componentDidUpdate(prevProps, prevState, snapshot) {
    // __DEV__ && console.tron.log(this.props.sorted_by_campus)
    // __DEV__ && console.tron.log(prevProps.sorted_by_campus !== this.props.sorted_by_campus)
    if (this.props.isFocused) {
      if (prevProps.sending_error !== this.props.sending_error
          && this.props.sending_error) {
        if (typeof this.props.sending_error == "object")
          Alert.alert(translate('ERROR.indisponible_avec_raison'),this.props.sending_error.message);
        else
          Alert.alert(translate('ERROR.erreur'), translate('ERROR.indisponible'));
      }
    }
    //Mise à jour du state uniquement si la prop à changer
    //Et qu'il y a une réservation de faite
    if (prevProps.lastReservation !== this.props.lastReservation) {

      //On ne fait pas d'inversion du state interne : on prend directement la valeur du props
      this.setState((partialState)=> ({
        lastReservation : this.props.lastReservation
      }))
    }
    
    //Gestion des notifications && si on est connecté
    if (prevProps.utilisateur.userId && prevProps.reservations_raw != this.props.reservations_raw) {
      PushService.initScheduledNotificationsForReservationsUserAndType(this.props.reservations_raw, this.props.utilisateur, PushService.scheduleNotificationRappelSalle, CHANNEL_IDS.RAPPEL_SALLE)
    }
  }

  //
  // componentWillReceiveProps(nextProps) {
  //   __DEV__ && console.tron.log("Props reservations_raw changé : ", nextProps.reservations_raw.length)
  //   this.setState(state => ({
  //     updated: nextProps.reservations_raw.length
  //   }), ()=>{
  //
  //     __DEV__ && console.tron.log('Mise à jour du state refresh :', this.state.refresh )
  //   })
  //
  // }
  //Options pour le modal
  //Définitions des boutons
  SUCCESS_RESERVATION_BUTTONS = [
    {
      text : 'Ok',
      type : 'success',
      onPress : () => {
        this.props.lastReservation = null
      }
    }
  ]
  //Mise à jour du state qui controle la visibilité du modal
  hideModal = () => {
    // __DEV__ && console.tron.log('HideModal : ' + this.state.lastReservation);
    this.setState({
      lastReservation: null
    })
  }
  //Texte à afficher à l'intérieur du modal (récupéré via this.props.children dans Modal)
  modalText = () => {
    if (this.state.lastReservation)
      return `${this.state.lastReservation.salle} ${translate("MODAL.RESERVATION.SUCCESS.body")} ${moment.unix(this.state.lastReservation.end_time).format('LLLL')} ${translate("MODAL.RESERVATION.SUCCESS.moderation")}`
    else
      return ''
  }

  //Handlers pour les items de la liste
  handlers = {
    onAvailable : (salle) => {
      //Anti double touch, si c'est déjà en cours d'envoi
      if (this.props.sending) return;

      //Si l'utilisateur est pas connecté, on pop le login
      if (!this.props.utilisateur.userId) {
        return this.props.utilisateurShowLogin();
      }
      let start_time = moment().floor(30, 'minutes').unix(),
            end_time = moment().ceil(30, 'minutes').add(1, 'hour').unix();

      let data = {
        start_time : start_time, //Date de début
        end_time : end_time, //Date de fin = maintenant + 60 minutes
        room_id : salle.id, //Identifiant de la salle
        room_name : salle.room_name, //Nom de la salle
        campus_name: salle.campus_name, //Nom du campus, défini via GRR
        moderate : salle.moderate, //Si la réservation de la salle a besoin d'être modérée par un administrateur, déclenchera une alerte mail côté serveur
        user: this.props.utilisateur,
        description : translate('COMMON.description_reservation_defaut')   //Description libre
      }
      this.props.putSalleReservationRequest(data)
    },
  }

  renderItem = ({section, item}) => {
    return (
      <ListItem backgroundColor={this.state.color} utilisateur={this.props.utilisateur} section={section} item={item} handlers={this.handlers} gender='feminine'></ListItem>
    )
  }

  // Conditional branching for section headers, also see step 3
  renderSectionHeader ({section}) {
    return <Text style={styles.sectionFixedHeader}>{translate(`LABEL_MAP.CAMPUS.${section.campus_id}`, { defaultValue: section.campus_name })}</Text>
  }


  // Show this when data is empty
  renderEmpty = () =>
    <Text style={styles.list_empty}>{translate("SCREEN.DISPO_SALLE.empty")}</Text>

  renderSeparator = () =>
    <View style={styles.row_separator}/>


  // The default function if no Key is provided is index
  // an identifiable key is important if you plan on
  // item reordering.  Otherwise index is fine
  // keyExtractor = (item, index) => (item.id)
  keyExtractor = (item, index) => index

  // How many items should be kept im memory as we scroll?
  oneScreensWorth = 6


  render () {
    __DEV__ && console.tron.log('Render list')
    return (
      <View style={styles.container}>
        <View style={styles.sub_nav}><Text style={styles.sub_nav_text}>{translate("SCREEN.DISPO_SALLE.subtitle")}</Text></View>
        <SectionList
          renderSectionHeader={this.renderSectionHeader}
          sections={this.props.sorted_by_campus}
          contentContainerStyle={styles.listContent}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          stickySectionHeadersEnabled={false}
          initialNumToRender={this.oneScreensWorth}
          ListEmptyComponent={this.renderEmpty}
          ItemSeparatorComponent={this.renderSeparator}
          extraData={this.props.sorted_by_campus}
          refreshing={false}
          onRefresh={this.props.sallesReservationsRequest}
        />
        <Modal
          isVisible={this.state.lastReservation}
          title={translate("MODAL.RESERVATION.SUCCESS.title")}
          onCancel={this.hideModal}
          buttons={this.SUCCESS_RESERVATION_BUTTONS}>
          {this.state.lastReservation && (
            <>
              <Text style={{ ...Fonts.style.normal }}>{this.state.lastReservation.salle} {translate("MODAL.RESERVATION.SUCCESS.body")} </Text>
              <Text style={{ ...Fonts.style.normal }}>{moment.unix(this.state.lastReservation.end_time).format('LLLL')} </Text>
              {!!this.state.lastReservation.moderate && <Text style={[Fonts.style.normal, ApplicationStyles.components.alert, ApplicationStyles.components.alert_warning]}>{translate("MODAL.RESERVATION.SUCCESS.moderation")}</Text>}
            </>
          )}
        </Modal>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    sorted_by_campus : SallesSelectors.getSallesSortedByCampus(state),
    reservations_raw : state.salles.reservations.raw,
    sending : state.salles.reservations.sending,
    sending_error : state.salles.reservations.sending_error,
    lastReservation : state.salles.reservations.lastReservation,
    utilisateur : state.utilisateur,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    utilisateurShowLogin: ()=>{dispatch(UtilisateurActions.utilisateurShowLogin())},
    sallesReservationsRequest: ()=>{dispatch(SallesActions.sallesReservationsRequest())},
    putSalleReservationRequest: (data)=>{dispatch(SallesActions.putSalleReservationRequest(data))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(DispoSalleScreen))
