import React, { Component } from 'react'
import { ScrollView, Text, View, ActivityIndicator, TouchableWithoutFeedback, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import moment from 'moment'
import Timeline from 'react-native-timeline-flatlist'

import Modal from '../Components/Modal'
import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'

//API
import API from '../Services/Api'

// Styles
import styles from './Styles/AgendaScreenStyle'
import { Fonts } from '../Themes'

//Internationalisation
import { translate } from '../I18n'
import i18n from 'i18n-js'

class AgendaScreen extends Component {

  static navigationOptions = ({navigation}) => ({
    //Utilisation d'un composant custom dans headerTitle (et pas title, crash sur android)
    //https://github.com/facebook/react-native/issues/10232#issuecomment-398950751
    headerTitle: <NavbarTitleWithIcon icon={navigation.state.params.icon} title={translate(`SCREEN_NAME_MAP.${navigation.state.params.screen}`)} />,
  })

  constructor(props){
    super(props)
    this.api = API.create()
    
    this.state = {
      ...this.props.navigation.state.params,
      selected: null,
      waiting: false,
      agenda: [],
    }
    //Utilisation de la locale actuelle pour moment
    moment.locale(i18n.locale)
  }

  MODAL_BUTTONS = [
    {
      text: translate('COMMON.ok'),
      type : 'success',
      onPress : () => {
        this.setState((partialState)=> ({
          ...partialState,
          selected: null
        }))
      }
    }
  ]
  componentDidMount() {
    this.getAgenda()
  }

  getAgenda(){
    this.setState({ waiting: true })

    this.api.getAgenda().then(apiDatas => {//récupération du 1er lot dans agenda
      
      //l'agenda est dans le désordre, on l'ordonne par rapport aux dates des événements
      if (apiDatas.ok && apiDatas.data.length > 0){
        const agenda = apiDatas.data
        .sort((a, b) => {
          if(a.data.length > 0 && b.data.length > 0)
            return moment(a.data[0].date, 'YYYY-M-D').diff(moment(b.data[0].date, 'YYYY-M-D'))
          else
            return 1000
        })
        agenda.forEach((evts, index) => {
          if(evts.data.length > 0)
            evts.data.sort((a, b) => moment(a.date, 'YYYY-M-D').diff(moment(b.date, 'YYYY-M-D')))
        })
        // __DEV__ && console.tron.log({ 'agenda': agenda })
        this.setState({
          waiting: false,
          //tri par rapport à la date de la 1ère données
          agenda,
        })
      }
    })
  }

  //Options pour le modal

  //Mise à jour du state qui controle la visibilité du modal
  hideModal = () => {
    // __DEV__ && console.tron.log(this.state);
    this.setState({
      selected: null
    })
  }

  renderTimelines = () => {
    if(this.state.agenda)
    {
      return(
        this.state.agenda.map((data, i) => {
          __DEV__ && console.tron.log(data);
          let _month = moment().month(data.month);
          //Construction d'un moment avec le mois passé et l'année (fix #4, au cas où on chevauche deux années)
          //server_carae@d2ad536f
          //https://momentjs.com/docs/#/get-set/month/
          if(data.year)
            _month.year(data.year);

          return (
              <View>
              <View style={styles.sub_nav}><Text style={styles.sub_nav_text}>{_month.format("MMMM YYYY")}</Text></View>
                <Timeline
                  circleSize={7}
                  circleColor='white'
                  lineWidth={0.7}
                  lineColor='white'

                  data={data.data}
                  style={styles.container_timeline}
                  detailContainerStyle={[styles.details_timeline, {backgroundColor: this.state.color}]}

                  renderTime={this.renderTime}
                  renderDetail={this.renderDetail}

                  onEventPress={this.onEventPress}

                />
              </View>
          )
        })
      )
    }
    else {
      return (
          <View>
            <View style={styles.sub_nav}><Text style={styles.sub_nav_text}>{translate('SCREEN.AGENDA.subtitle_empty')}</Text></View>
          </View>
      )
    }
  }

  renderTime = (rowData, sectionID, rowID) => {
    return (
      <View style={styles.time_timeline}>
        <Text style={styles.time_text_timeline}>
          {moment(rowData.date).format('ddd') + '\n' + moment(rowData.date).format('D')/*ex : lun. 17*/}
        </Text>
      </View>
    )
  }

  renderDetail = (rowData, sectionID, rowID) => {
    let title = <Text style={[styles.title_timeline, Fonts.style.h5]}>{rowData.titre}</Text>
    let heures_vides = (moment(rowData.deb, 'hmm').hours() + moment(rowData.deb, 'hmm').minutes() + moment(rowData.fin, 'hmm').hours() + moment(rowData.fin, 'hmm').minutes()) === 0;

    heure_lieu = (
      <View >
        {heures_vides ? null : <Text style={Fonts.style.description}>{moment(rowData.deb, 'hmm').format('HH:mm') + ' - ' + moment(rowData.fin, 'hmm').format('HH:mm')}</Text>}
        <Text style={Fonts.style.description}>{rowData.lieu}</Text>
      </View>
    )

    return (
      <View>
        {title}
        {heure_lieu}
      </View>
    )
  }

  onEventPress = (data) => {
    this.setState({selected: data});
  }

  flashScrollIndicators = (ref) => {
    //Flash du scroll view
    this.modal_scrollview && this.modal_scrollview.flashScrollIndicators()
  }


  modalSelected = () => {
      if(this.state.selected){
        let heures_vides = (moment(this.state.selected.deb, 'hmm').hours() + moment(this.state.selected.deb, 'hmm').minutes() + moment(this.state.selected.fin, 'hmm').hours() + moment(this.state.selected.fin, 'hmm').minutes()) === 0;

        return (
          <Modal
            isVisible={!!this.state.selected}
            title={this.state.selected.titre}
            onCancel={this.hideModal}
            buttons={this.MODAL_BUTTONS}>
            <ScrollView style={styles.scrollView}
              showsVerticalScrollIndicator={true}
              indicatorStyle='white'
              ref={(ref)=>{this.modal_scrollview = ref;}}
              onLayout={()=>{this.flashScrollIndicators()}}>   
              <TouchableOpacity>
                <TouchableWithoutFeedback>
                  <View>
                    {heures_vides ? null : <Text style={styles.modalDate}>{moment(this.state.selected.deb, 'hmm').format('HH:mm') + ' - ' + moment(this.state.selected.fin, 'hmm').format('HH:mm')}</Text>}
                    {!this.state.selected.lieu ? null : <Text style={styles.modalDesc}>{this.state.selected.lieu}</Text>}
                    {!this.state.selected.description ? null : <Text style={styles.modalDesc}>{this.state.selected.description}</Text>}
                  </View>
                </TouchableWithoutFeedback>
              </TouchableOpacity>
            </ScrollView>
          </Modal>
        )
      }
  }



  scroll = (e) => {
    if(!this.state.waiting){
      let paddingToBottom = 10;
          paddingToBottom += e.nativeEvent.layoutMeasurement.height;

      //Il était prévu de charger les éléments suivants en bas de page => Annulé pour le moment (tout est chargé d'un coup au démarrage)
      //if(e.nativeEvent.contentOffset.y >= e.nativeEvent.contentSize.height - paddingToBottom){
      //  this.getAgenda()
      //}
    }
  }

  renderWaiting = () => {
    if(this.state.waiting)
      return <ActivityIndicator />
  }

  render () {
    return (
      <ScrollView style={styles.container} onScroll={(e) => this.scroll(e)}>
        {this.modalSelected()}
        {this.renderTimelines()}
        {this.renderWaiting()}
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AgendaScreen)
