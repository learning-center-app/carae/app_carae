//@flow
import React from 'react'
import { View, FlatList, Text, Image, TouchableOpacity, Alert } from 'react-native'
import { connect } from 'react-redux'
import memoizeOne from 'memoize-one'

import Modal from '../Components/Modal'
import Salle from '../Classes/Salle'

import moment from 'moment'
import 'moment/locale/fr'
import 'moment-round'
//Internationalisation
import { translate } from '../I18n'
import i18n from 'i18n-js'

import AvailableLabel from '../Components/AvailableLabel'
import ReservationItem from '../Components/ReservationItem'
import SemanticButton from '../Components/SemanticButton'
import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'

//Actions redux
import SallesActions, { SallesSelectors} from '../Redux/SallesRedux'
import UtilisateurActions from '../Redux/UtilisateurRedux'

import PutFilteredReservationsInItem from '../Transforms/PutFilteredReservationsInItem'

// More info here: https://facebook.github.io/react-native/docs/sectionlist.html

// Styles
import styles from './Styles/MesReservationsSalleScreenStyle'
import { Images, Fonts } from '../Themes'
import PushService from '../Services/PushService'
import { CHANNEL_IDS } from '../Services/PushConfiguration'
import { withNavigationFocus } from "react-navigation"

class MesReservationsSalleScreen extends React.PureComponent {
  static navigationOptions = ({navigation}) => ({
    //Utilisation d'un composant custom dans headerTitle (et pas title, crash sur android)
    //https://github.com/facebook/react-native/issues/10232#issuecomment-398950751
    headerTitle: <NavbarTitleWithIcon icon={navigation.state.params.icon} title={translate(`SCREEN_NAME_MAP.${navigation.state.params.screen}`)}/>,

    //https://github.com/react-navigation/react-navigation/issues/145#issuecomment-281418095
    headerRight: <TouchableOpacity style={styles.navbar_button} onPress={() => {navigation.navigate('ReservationSalleScreen', navigation.state.params)}}>
                  <Image source={Images.actions_icons.dark.add} style={styles.navbar_icon}></Image>
                </TouchableOpacity>
  })

  constructor (props) {
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {...props.navigation.state.params};

    this.state.modal = {
      edit: false,
    }
    this.state.data = this.DEFAULT_DATA;

    __DEV__ && console.tron.log(this.state);
    //Utilisation de la locale actuelle pour moment
    moment.locale(i18n.locale)
  }

  componentDidMount () {
    //Chopage des résas de salles au cas où

    //Si l'utilisateur est pas connecté, on pop le login
    if (!this.props.utilisateur.userId) {
      return this.props.utilisateurShowLogin();
    }
    else {
      this.props.sallesReservationsRequest();
    }
    // __DEV__ && console.tron.log(this.props.sorted_by_campus)
  }

  //https://stackoverflow.com/a/44750890/1437016
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.isFocused) {
      //Affichage de l'erreur
      if (prevProps.sending_error !== this.props.sending_error
          && this.props.sending_error && !this.state.showing_error) {
        //Flag dans le state pour éviter les multiples affichages
        this.setState((partialState)=>({...partialState, showing_error:true}), ()=>{
          //Obligé de mettre un timeout, car conflit entre modal et alert : https://github.com/facebook/react-native/issues/10471
          setTimeout(()=>{
            if(typeof this.props.sending_error == "object")
              Alert.alert(
                translate('ERROR.erreur'),
                this.props.sending_error.error,
                [{text: 'OK', onPress: () => this.setState((partialState)=>({showing_error:false}))}],
                {cancelable: false}
              );
            else
              Alert.alert(
                translate('ERROR.erreur'),
                translate('ERROR.indisponible'));
          }, 600)
        })
      }
    }

    //Gestion des notifications && si on est connecté
    if (prevProps.utilisateur.userId && prevProps.all_reservations != this.props.all_reservations) {
      PushService.initScheduledNotificationsForReservationsUserAndType(this.props.all_reservations, this.props.utilisateur, PushService.scheduleNotificationRappelSalle, CHANNEL_IDS.RAPPEL_SALLE)
    }
  }

  DEFAULT_DATA = {
    start_time : moment(),
    end_time : moment(),
  }

  //Options pour le modal
  //Définitions des boutons
  EDIT_BUTTONS = [
    {
      text : translate('COMMON.supprimer'),
      type : 'error',
      onPress : () => {
        //Appel à l'API via Redux
        this.props.deleteSalleReservationRequest({
          ...this.state.data,
          start_time: this.state.data.start_time.unix(),
          end_time: this.state.data.end_time.unix(),
        });
        //Hide du modal
        this.setState((partialState)=> ({
          ...partialState,
          modal : {
            edit: false
          }
        }))
      }
    },
    {
      text : translate('COMMON.modifier'),
      type : 'success',
      onPress : () => {
        //Vérification de la disponibilité du matériel.
        let _availability = this.getReservationsOfSalle(this.state.data).getAvailability(this.props.utilisateur, this.state.data.start_time, this.state.data.end_time, true)
        __DEV__ && console.tron.log(_availability);

        if(_availability != "available")
          return;
        //Appel à l'API via Redux
        this.props.postSalleReservationRequest({
          ...this.state.data,
          start_time: this.state.data.start_time.unix(),
          end_time: this.state.data.end_time.unix(),
        });
        //Hide du modal
        this.setState((partialState)=> ({
          ...partialState,
          modal : {
            edit: false
          }
        }))
      }
    }
  ]

  subtract30minutes = (time, type) => {
    this._updateMinutes('subtract', 30, 'minutes', time, type)
  }

  add30minutes = (time, type) => {
    this._updateMinutes('add', 30, 'minutes', time, type)
  }
  //Privates
  //Helper
  _updateMinutes = (operation = 'add', number = 30, unity = 'minutes', time, type) => {
    time = moment.isMoment(time) ? time.clone() : moment(time);
    time[operation](number, unity);
    this._setHours(time, type)
  }
  //time : objet moment
  //type : propriété à changer
  _setHours = (time, type = 'start_time') => {
    //Copy des filtres actuels
    let _data = {...this.state.data};
    //Affectation des heures dans le jour sélectionné
    _data[type].hours(time.hours()).minutes(time.minutes())

    //Attention à ne pas avoir un date de début postérieur à la date de fin
    if (_data.end_time.isSameOrBefore(_data.start_time)) {
      //si on est en train d'avancer l'heure de début, on avance l'heure de fin
      if (type == 'start_time')
        _data.end_time.add(30, 'minutes')
      //si on est en train de reculer l'heure de fin, on recule l'heure de début
      else
        _data.start_time.subtract(30, 'minutes')
    }

    this.setState({
      data: {
        ...this.state.data,
        start_time: _data.start_time,
        end_time: _data.end_time,
      }
    })
  }

  showModalEdit = (item) => {
    __DEV__ && console.tron.log(item)
    this.setState((partialState)=> ({
      ...partialState,
      data : {
        ...item,
        start_time : moment.unix(item.start_time),
        end_time : moment.unix(item.end_time),
      },
      modal : {
        edit: true
      },
      // data : item
    }))
  }

  hideModalEdit = (item) => {
    this.setState((partialState)=> ({
      modal : {
        edit: false
      },
      data : this.DEFAULT_DATA
    }));
  }

  handlers = {
    onReserved : this.showModalEdit,
    onAvailable : () => { },
    onConflict : ()=>{ },
    onUnavailable : ()=>{ },
  }

  getReservationsOfSalle = memoizeOne((salle) => {
    let _salle = PutFilteredReservationsInItem(this.props.all_reservations, salle, Salle);
    __DEV__ && console.tron.log(_salle);
    return _salle;
  }, (a, b) => a.room_id == b.room_id)


  renderItem = ({item}) => {
    return (
      <ReservationItem backgroundColor={this.state.color} item={item} handlers={this.handlers}></ReservationItem>
    )
  }

  // Conditional branching for section headers, also see step 3
  renderSectionHeader ({section}) {
    return <Text style={styles.sectionFixedHeader}>{section.name}</Text>
  }

  // Show this when data is empty
  renderEmpty = () =>
    <View>
      <Text style={styles.list_empty}>{translate('COMMON.aucune_reservation')}</Text>
      <View style={styles.inline}>
        <SemanticButton onPress={() => { this.props.navigation.navigate('ReservationSalleScreen', this.props.navigation.state.params) }} text={translate('COMMON.nouvelle_reservation')} type='success'>
        </SemanticButton>
      </View>
    </View>

  renderSeparator = () =>
    <View style={styles.row_separator}/>

  // item reordering.  Otherwise index is fine
  keyExtractor = (item, index) => {item.id}

  // How many items should be kept im memory as we scroll?
  oneScreensWorth = 20

  render () {
    let _item_availability = this.getReservationsOfSalle(this.state.data).getAvailability(this.props.utilisateur, this.state.data.start_time, this.state.data.end_time, true);
    // __DEV__ && console.tron.log({rendering: 'list', item: _item.reservations.length})
    return (
      <View style={styles.container}>
        <View style={styles.sub_nav}><Text style={styles.sub_nav_text}>{translate('COMMON.mes_reservations')}</Text></View>
        <FlatList
          // renderSectionHeader={this.renderSectionHeader}
          data={this.props.sorted_by_campus}
          contentContainerStyle={styles.listContent}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          initialNumToRender={this.oneScreensWorth}
          stickySectionHeadersEnabled={false}
          ListHeaderComponent={this.renderHeader}
          // SectionSeparatorComponent={this.renderSectionSeparator}
          // ListFooterComponent={this.renderFooter}
          ListEmptyComponent={this.renderEmpty}
          ItemSeparatorComponent={this.renderSeparator}
          // renderSectionFooter={this.renderSectionFooter}
          extraData={this.props}
        />
        <Modal
          isVisible={this.state.modal.edit}
          title={translate('MODAL.HEURES.modifier')}
          onCancel={this.hideModalEdit}
          buttons={this.EDIT_BUTTONS}>
          <View style={styles.modal_block}>
            <Text style={styles.modal_label}>{translate('MODAL.HEURES.heure_debut')}</Text>
            <View style={styles.modal_inline}>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.subtract30minutes(this.state.data.start_time, 'start_time')}>
                <Text style={Fonts.style.h1}>-</Text>
              </TouchableOpacity>
              <Text style={[styles.modal_inline_item, Fonts.style.h2]}>{this.state.data.start_time.format('H[h]mm')}</Text>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.add30minutes(this.state.data.start_time, 'start_time')}>
                <Text style={Fonts.style.h1}>+</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.modal_block}>
            <Text style={styles.modal_label}>{translate('MODAL.HEURES.heure_fin')}</Text>
            <View style={styles.modal_inline}>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.subtract30minutes(this.state.data.end_time, 'end_time')}>
                <Text style={Fonts.style.h1}>-</Text>
              </TouchableOpacity>
              <Text style={[styles.modal_inline_item, Fonts.style.h2]}>{this.state.data.end_time.format('H[h]mm')}</Text>
              <TouchableOpacity style={styles.modal_inline_cursor} onPress={()=>this.add30minutes(this.state.data.end_time, 'end_time')}>
                <Text style={Fonts.style.h1}>+</Text>
              </TouchableOpacity>
            </View>
            <AvailableLabel status={_item_availability}/>
          </View>
        </Modal>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    sorted_by_campus : SallesSelectors.getReservationsByUser(state),
    utilisateur : state.utilisateur,
    all_reservations : state.salles.reservations.raw,
    sending_error: state.salles.reservations.sending_error,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    utilisateurShowLogin: ()=>{dispatch(UtilisateurActions.utilisateurShowLogin())},
    sallesReservationsRequest: ()=>{dispatch(SallesActions.sallesReservationsRequest())},
    postSalleReservationRequest: (data)=>{dispatch(SallesActions.postSalleReservationRequest(data))},
    deleteSalleReservationRequest: (data)=>{dispatch(SallesActions.deleteSalleReservationRequest(data))},
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(MesReservationsSalleScreen))
