//@flow
import React, { Component } from 'react'
import { Image, ScrollView, Text, TextInput, View, ActivityIndicator, TouchableOpacity } from 'react-native'
import Modal from '../Components/Modal';
import BetterKeyboardAvoidingView from '../Components/BetterKeyboardAvoidingView';
import NavbarTitleWithIcon from '../Components/NavbarTitleWithIcon'

import { connect } from 'react-redux'
import QRCodeScanner from 'react-native-qrcode-scanner'

//Internationalisation
import { translate } from '../I18n'

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

import EmpruntLivreActions from '../Redux/EmpruntLivreRedux'
import UtilisateurActions from '../Redux/UtilisateurRedux'

// Styles
import styles from './Styles/EmpruntLivreScreenStyle'

import { Images, Fonts, Colors } from '../Themes'
import { RNCamera } from 'react-native-camera';
import ListeLivres from '../Components/ListeLivres';

class EmpruntLivreScreen extends Component {
  static navigationOptions = ({navigation}) => ({
    //Utilisation d'un composant custom dans headerTitle (et pas title, crash sur android)
    //https://github.com/facebook/react-native/issues/10232#issuecomment-398950751
    headerTitle: <NavbarTitleWithIcon icon={navigation.state.params.icon} title={translate(`SCREEN_NAME_MAP.${navigation.state.params.screen}`)} />,

    //https://github.com/react-navigation/react-navigation/issues/145#issuecomment-281418095

    headerRight: <TouchableOpacity onPress={() => {navigation.state.params.showModalLoginKoha()}}>
                  <Image source={Images.actions_icons.light.carte} style={[styles.menu_icon]}></Image>
                </TouchableOpacity>,

  })
  static localStyle = ({navigation}) => ({
    color: navigation.state.params.title,
  })

  constructor(props){
    super(props);
    //Récupération du state
    //https://github.com/react-navigation/react-navigation/issues/147#issuecomment-276853739
    this.state = {
      ...props.navigation.state.params,
      ...this.state,
      focusedScreen: true
    };
  }

  componentDidMount(){
    this.props.navigation.setParams({ showModalLoginKoha: this.showModalLoginKoha });
    this.props.navigation.setParams({ reset: ()=> this.props.reset() });

    //Ajout d'un booléen de state qui check si l'écran est affiché : si on charge la caméra dans l'écran de scan de badge, ça freeze la caméra de cet écran
    // -> focusedScreen utilise les events de la navigation pour forcer un re-render quand on revient sur l'écran
    const { navigation } = this.props;
    navigation.addListener('willFocus', () =>
      setTimeout(() => this.setState({ focusedScreen: true }), 200)
    );
    navigation.addListener('willBlur', () =>
      this.setState({ focusedScreen: false })
    );
  }
  showModalLoginKoha = () => {
    this.props.navigation.navigate("LoginKohaScreen", {color : this.state.color});
  }

  state = {
    modal:{
      success: false,
      erreur: false,
    },
  };

  SUCCESS_BUTTONS = [
    {
      text : "Retour",
      type : 'success',
      onPress : () => { this.hideModal('success') }
    }
  ]
  ERROR_BUTTONS = [
    {
      text : "Retour",
      type : 'warning',
      onPress : () => { this.hideModal('error') }
    }
  ]
  ERROR_CARD_BUTTONS = [
    {
      text: translate('ERROR_MAP.err_carte_non_reconnue.buttons.consulter_horaires'),
      type: 'success',
      onPress: () => { 
        this.hideModal('error') 
        this.props.navigation.goBack();
        // this.props.navigation.navigate("HorairesScreen", this.props.navigation.state.params);
        this.props.navigation.navigate("HorairesScreen", this.props.screens.find(redux_screen => redux_screen.screen == "HorairesScreen"));
      }
    },
    {
      text: translate('ERROR_MAP.err_carte_non_reconnue.buttons.modifier_numero_carte'),
      type: 'warning',
      onPress: () => { 
        this.hideModal('error')
        this.props.navigation.navigate("LoginKohaScreen", { color: this.state.color });
      }
    }
  ]

  scanner = {}

  //Hook sur le changement de props
  //https://stackoverflow.com/a/44750890/1437016
  componentWillReceiveProps(props) {
    if (props.sending) {
      this.setState({ button_label : "En cours..." })
    }
    else if(!props.sending){
      if (!this.state.modal.error && props.error) {
        this.setState({ modal : {error : true}})
      }
      else if (!this.state.modal.success && props.last_book) {
        this.setState({ modal : {success : true}})
      }
    }
  }

  postEmpruntLivre = (barcode) => {
    __DEV__ && console.tron.log({"Scan de code " :barcode})
    if (!this.props.utilisateur.userId) {
      return this.props.utilisateurShowLogin();
    }
    //Vérification qu'il y a bien un identifiatn KOHA pour réserver les bouquins
    if (!this.props.utilisateur.id_koha) {
      this.props.navigation.navigate("LoginKohaScreen", {color : this.state.color});
    }
    let data = {
      barcode: barcode,
      utilisateur: {...this.props.utilisateur}
    }
    //Check du flag sending avant d'envoyer, pour éviter le spam de requête
    //Check aussi d'une erreur affichée, ou d'un bouquin emprunté avec succès ; il est possible que le 
    //scanner ait été réactivé, qu'il n'y ait aucune requête en cours, mais que le scanner continue à scanner
    //alors qu'un modal est présenté. Le triple check réellement empêche le spam de requête, et la superposition des modaux
    !this.props.sending && !this.props.error && !this.props.last_book && this.props.postEmpruntLivre(data);
  }

  showModal = (modal_name) => {
    let modal = {};
    modal[modal_name] = true;
    //On attend que le modal soit caché avant de mettre à jour le state redux
    this.setState({modal}, ()=>{
      //Reset du state redux, qui est persitant
      //Après un timeout de 1 seconde, le temps que le modal soit caché
      setTimeout(this.props.resetEmpruntLivre, 1000)
    });
  }

  hideModal = (modal_name) => {
    let modal = {};
    modal[modal_name] = false;
    //On attend que le modal soit caché avant de mettre à jour le state redux
    this.setState({modal}, ()=>{
      //Réactivation du scanner via methode sur référence du scanner
      //https://www.npmjs.com/package/react-native-qrcode-scanner#reactivate
      this.scanner && this.scanner.reactivate();
      //Reset du state redux, qui est persitant
      //Après un timeout de 1 seconde, le temps que le modal soit caché
      setTimeout(this.props.resetEmpruntLivre, 1000)
    });
  }

  render () {
    return (
      <View style={[styles.container]}>
        <View style={styles.sub_nav}><Text style={styles.sub_nav_text}>{translate('SCREEN.EMPRUNT_LIVRE.subtitle')}</Text></View>
        <ScrollView style={[styles.container]}>
          { !!this.props.sending && <ActivityIndicator size="large" color="#FFFFFF" animating={this.props.sending} style={styles.activity_indicator}/>}
          <BetterKeyboardAvoidingView>
            { !!this.state.focusedScreen && (
              <QRCodeScanner 
                onRead={(event)=>this.postEmpruntLivre(event.data)} 
                ref={(node) => { this.scanner = node }} 
                containerStyle={{height:200}}
                cameraContainerStyle={{height:200}}
                cameraProps={{ 
                  captureAudio: false, 
                  // Désactivation de l'utilisation de firebase pour la lecture des codes barre
                  // onBarCodeRead: null, 
                  // googleVisionBarcodeType :RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeType.CODABAR,
                  // onGoogleVisionBarcodesDetected: (event) => { 
                  //   // __DEV__ && console.tron.log({...event, _barcodeDetected: event.barcodes.length})
                  //   if(event.barcodes && event.barcodes.length) {
                  //     __DEV__ && console.tron.log({ data: event.barcodes[0].data, event })
                  //     this.postEmpruntLivre(event.barcodes[0].data)
                  //   }
                  // }
                }}
                // markerStyle={{ height: 75 }}
                cameraStyle={{ height: 200, overflow: 'hidden' }} //https://github.com/moaazsidat/react-native-qrcode-scanner/issues/173#issuecomment-506326477
                // showMarker={true} 
                reactivate={false}
                reactivateTimeout={2000}

                />
              
            )}
              {/* 
              <RNCamera
                captureAudio={false}
                ref={(node) => { this.scanner = node }}
                style={{ height: 350, overflow: 'hidden' }}
                googleVisionBarcodeType={RNCamera.Constants.GoogleVisionBarcodeDetection.BarcodeType.CODABAR}
                onGoogleVisionBarcodesDetected={(event) => event.barcodes.length && __DEV__ && console.tron.log({data: event.barcodes[0].data, event})}></RNCamera>  */}
            <View>
              <Text style={[styles.label, styles.padded, styles.align_center]}>
                {translate('SCREEN.EMPRUNT_LIVRE.input_code')}
              </Text>
              <TextInput
              style={[styles.input, styles.padded, styles.textInput]}
              placeholder="ex: 0523489"
              //  keyboardType='number-pad'
              placeholderTextColor = {Colors.slightlyTransparentText}
              enablesReturnKeyAutomatically={false}
              onChangeText={(value)=>this.setState({'barcode':value})}
              onBlur={()=>this.postEmpruntLivre(this.state.barcode)}
              returnKeyType='search'
              />
            </View>
          </BetterKeyboardAvoidingView>
          <View>
            <Text style={[styles.label, styles.padded, styles.align_center]}>
              {translate('SCREEN.EMPRUNT_LIVRE.livres_empruntes')}
            </Text>
            <ListeLivres key={this.props.last_book} backgroundColor={this.state.color} />
          </View>
        </ScrollView>
        <Modal
          isVisible={this.state.modal.success}
          onCancel={()=>this.hideModal('success')}
          title={translate('MODAL.EMPRUNT.SUCCESS.title')}
          buttons={this.SUCCESS_BUTTONS}>
          <View>
            <Text style={[ Fonts.style.h3, styles.centerText ]} >
              {!!this.props.last_book && this.props.last_book.book_title}
            </Text>
            <Text style={[ Fonts.style.h4, styles.centerText ]} >
              {!!this.props.last_book && this.props.last_book.book_authors}
            </Text>
            <Text style={[ Fonts.style.h5, styles.centerText, {paddingTop:30, paddingBottom:30} ]}>
              {!!this.props.last_book && `${translate('MODAL.EMPRUNT.SUCCESS.body')} ${this.props.last_book.book_return_date}`}
            </Text>
          </View>
        </Modal>
        {/* / MODAL ERREUR */}
        <Modal
          isVisible={this.state.modal.error}
          onCancel={()=>this.hideModal('error')}
          title={this.props.error ? translate(`ERROR_MAP.${this.props.error.error_code}.title`) : translate('ERROR.erreur')}
          buttons={this.props.error && this.props.error.error_code == "err_carte_non_reconnue" ? this.ERROR_CARD_BUTTONS : this.ERROR_BUTTONS}>
          <View>
            <Text style={[ Fonts.style.h5, styles.centerText, {paddingTop:30, paddingBottom:30} ]}>
              {this.props.error ? translate(`ERROR_MAP.${this.props.error.error_code}.body`) : translate('ERROR.indisponible')}
            </Text>
          </View>
        </Modal>
      </View>
    )
  }
}

EmpruntLivreScreen.defaultProps = {
  sending : {},
  error : {},
  last_book : {},
}
const mapStateToProps = (state) => {
  return {
    screens: state.screen.list,
    sending : state.emprunt_livre.sending,
    error : state.emprunt_livre.error,
    last_book : state.emprunt_livre.last_book,
    utilisateur : state.utilisateur,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    utilisateurShowLogin: ()=>{dispatch(UtilisateurActions.utilisateurShowLogin())},
    postEmpruntLivre : data => {dispatch(EmpruntLivreActions.empruntLivreRequest(data))},
    resetEmpruntLivre : () => {dispatch(EmpruntLivreActions.empruntLivreReset())},
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EmpruntLivreScreen)
