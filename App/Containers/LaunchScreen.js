//@flow
import React, { Component } from 'react'
import { Image, View, TouchableHighlight } from 'react-native'
import { Pages } from 'react-native-pages';
import PageControl from 'react-native-page-control'
import { Images } from '../Themes'
import { connect } from 'react-redux'

import UtilisateurActions from '../Redux/UtilisateurRedux'


// Styles
import styles from './Styles/LaunchScreenStyles'

//Internationalisation
import i18n from 'i18n-js'

class LaunchScreen extends Component {

  constructor(props)
  {
    super(props)

    let pager = null
    this.state = {
      current_page: 0,
      //Utilisation de la locale actuelle pour moment
      pages: Images.tutoriel[i18n.locale],
    }

  }

  displayPageControl = () => {
    return (
      <PageControl
        style={styles.pagecontrol_container}
        numberOfPages={this.state.pages.length}
        currentPage={this.state.current_page}
        hidesForSinglePage
        pageIndicatorTintColor='white'
        currentPageIndicatorTintColor='rgb(155,202,65)'
        indicatorStyle={{borderRadius: 0}}
        currentIndicatorStyle={{borderRadius: 0}}
        indicatorSize={{width:8, height:8}}
      />
    )
  }

  changePage = (new_page) => {
    if (this.state.current_page !== new_page){
      if(new_page !== this.state.pages.length){
        this.setState({current_page: new_page});
        this.pager.scrollToPage(new_page)
      }else
        this.goToNextScreen()
    }
  }

  goToNextScreen = () => {
    this.props.setTutorielVu(true);//pour ne plus afficher le tutoriel par la suite
    this.props.navigation.goBack();
  }

  render () {
    return (
      <View style={[styles.main_container, styles.modalMarginIphone]}>
        {this.displayPageControl()}

        <Pages
          style={styles.viewpager_container}
          indicatorColor='#000'
          indicatorPosition='none'
          ref={ref => { this.pager = ref; }}
          onScrollEnd={(index) => {this.changePage(index)}}>
          {
            this.state.pages.map((img, i) =>
              <TouchableHighlight
                    style={{ flex: 1, backgroundColor: 'white'}}
                    onPress={()=> this.changePage(i+1)}>
                <Image source={img} style={styles.didactImg} />
              </TouchableHighlight>
            )
          }
        </Pages>
      </View>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    tutoriel_est_vu:  state.utilisateur.tutoriel_est_vu
  }
}

const mapDispatchToProps = (dispatch, vu) => {
  return {
    setTutorielVu : (vu)=> {dispatch(UtilisateurActions.setTutorielVu(vu))}
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LaunchScreen)
