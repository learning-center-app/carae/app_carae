//Modules
import moment from 'moment'
import memoizeOne from 'memoize-one'

export default class Item {
  reservations = []

  constructor(item, reservations) {
    for(let property in item){
      if (item.hasOwnProperty(property) && !this.hasOwnProperty(property))
        this[property] = item[property];
    }
    //S'il n'y a pas de résa, on y met un tableau vide
    this.reservations = typeof reservations != "undefined" ? reservations : [];
  }

  //Check si l'item est disponible au moment présent + 60 minutes
  //Prend l'item en argument, parce que j'arrive pas à binder this correctement…
  getAvailability = (utilisateur, start_time = moment(), end_time = moment().add(60, 'minutes'), not_mine = false) => {
    //Cas spécifique où la ressource est déclarée comme non empruntable
    if(this.statut_room == 0)
      return "unavailable";

    // __DEV__ && console.tron.log({'getAvailability': this.room_name})
    start_time = moment.isMoment(start_time) ? start_time : moment.unix(start_time)
    end_time = moment.isMoment(end_time) ? end_time : moment.unix(end_time)
    //Check si la réservation est dans les heures de résa permises (s'il y en a)
    if(this.max_hour && this.min_hour) {
      let min_hour = start_time.clone().set('hours', this.min_hour).set('minutes', 0);
      let max_hour = end_time.clone().set('hours', this.max_hour).set('minutes', 0);
      if (!start_time.isBetween(min_hour, max_hour, 'minutes', '[)') 
        || !end_time.isBetween(min_hour, max_hour, 'minutes', '(]')){
        return 'unavailable';
      }
    }
    //Check s'il y a une résa dans les XX minutes
    //Test avec l'heure de début de la réunion
    var current_reservation = this.reservations.find((item) => {
      //Utilisation d'unix() pour créer un moment depuis un timestamp en seconde (issu de mysql)
      const start = moment.unix(item.start_time),
              end = moment.unix(item.end_time);
      //Docs : If you want to limit the granularity to a unit other than milliseconds, pass the units as the third parameter.
      //https://momentjs.com/docs/#/query/is-between/
      //Version 2.13.0 introduces inclusivity. A [ indicates inclusion of a value. A ( indicates exclusion. If the inclusivity parameter is used, both indicators must be passed.
      return (start_time.isBetween(start, end, 'minute', '[)') //La date de départ demandée est entre le début et la fin d'une des résa
          || start.isBetween(start_time, end_time, 'minute', '[)')) //Une des résa commence au milieu des dates demandées de début et de fin
          && (not_mine ? !(item.beneficiaire == utilisateur.userId) : true);
    })

    // Pour diagnostic de certains items
    __DEV__ && [72].indexOf(this.room_id) > -1 && console.tron.log({
      item : this,
      reservations: this.reservations,
      start_time,
      end_time,
      current_reservation: current_reservation
    })
    //Si c'est occupé
    if (current_reservation) {
      // __DEV__ && console.tron.log({
      //   utilisateur: utilisateur,
      //   current_reservation: current_reservation
      // })
      //Mais que c'est l'utilisateur en cours qui l'a réservée
      if(utilisateur && utilisateur.userId &&
        (current_reservation.beneficiaire == utilisateur.userId))
        return "reserved";
      else
        return "unavailable";
    }
    else {
      //Check si la fin de réunion va tomber au milieu d'une réunion réservée.
      //-> Va falloir réduire la durée de la réunion
      var should_end_sooner = this.reservations.some((item) => {
        const start = moment.unix(item.start_time),
        end = moment.unix(item.end_time);

        return end_time.isBetween(start, end, 'minute', '(]')
               && (not_mine ? !(item.beneficiaire == utilisateur.userId) : true);
      })
      if (should_end_sooner)
        return "conflict";
      else
        return "available";
    }

  }
}
